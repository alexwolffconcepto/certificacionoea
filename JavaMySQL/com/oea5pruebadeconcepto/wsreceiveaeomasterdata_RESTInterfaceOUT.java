package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.fasterxml.jackson.annotation.*;
import java.util.*;

@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.NONE)
@javax.xml.bind.annotation.XmlType(name = "wsreceiveaeomasterdata_RESTInterfaceOUT", namespace ="http://tempuri.org/")
@JsonPropertyOrder(alphabetic=true)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.NONE, getterVisibility=JsonAutoDetect.Visibility.NONE, isGetterVisibility=JsonAutoDetect.Visibility.NONE)
public final  class wsreceiveaeomasterdata_RESTInterfaceOUT
{
   String AV13ErrorDescription;
   @JsonProperty("ErrorDescription")
   public String getErrorDescription( )
   {
      return AV13ErrorDescription ;
   }

   @JsonProperty("ErrorDescription")
   public void setErrorDescription(  String Value )
   {
      AV13ErrorDescription= Value;
   }


}

