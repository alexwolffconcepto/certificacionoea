package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(urlPatterns = {"/servlet/com.oea5pruebadeconcepto.countryaeomasterdatawc", "/com.oea5pruebadeconcepto.countryaeomasterdatawc"})
public final  class countryaeomasterdatawc extends GXWebObjectStub
{
   public countryaeomasterdatawc( )
   {
   }

   public countryaeomasterdatawc( int remoteHandle )
   {
      super(remoteHandle, new ModelContext( countryaeomasterdatawc.class ));
   }

   public countryaeomasterdatawc( int remoteHandle ,
                                  ModelContext context )
   {
      super(remoteHandle, context);
   }

   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new countryaeomasterdatawc_impl(context).doExecute();
   }

   protected void init( com.genexus.internet.HttpContext context )
   {
      new countryaeomasterdatawc_impl(context).cleanup();
   }

   public String getServletInfo( )
   {
      return "Country AEOMaster Data WC";
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

   protected String IntegratedSecurityPermissionPrefix( )
   {
      return "";
   }

   protected String EncryptURLParameters( )
   {
      return "NO";
   }

}

