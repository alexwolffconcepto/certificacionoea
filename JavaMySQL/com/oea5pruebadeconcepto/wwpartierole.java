package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(urlPatterns = {"/servlet/com.oea5pruebadeconcepto.wwpartierole", "/com.oea5pruebadeconcepto.wwpartierole"})
public final  class wwpartierole extends GXWebObjectStub
{
   public wwpartierole( )
   {
   }

   public wwpartierole( int remoteHandle )
   {
      super(remoteHandle, new ModelContext( wwpartierole.class ));
   }

   public wwpartierole( int remoteHandle ,
                        ModelContext context )
   {
      super(remoteHandle, context);
   }

   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new wwpartierole_impl(context).doExecute();
   }

   protected void init( com.genexus.internet.HttpContext context )
   {
      new wwpartierole_impl(context).cleanup();
   }

   public String getServletInfo( )
   {
      return "Partie Roles";
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

   protected String IntegratedSecurityPermissionPrefix( )
   {
      return "";
   }

   protected String EncryptURLParameters( )
   {
      return "NO";
   }

}

