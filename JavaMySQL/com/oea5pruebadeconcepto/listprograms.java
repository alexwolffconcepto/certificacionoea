package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.search.*;

public final  class listprograms extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(com.oea5pruebadeconcepto.GXcfg.class);
      listprograms pgm = new listprograms (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
      GXRuntime.exit( );
   }

   public void executeCmdLine( String args[] )
   {
      @SuppressWarnings("unchecked")
      GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>[] aP0 = new GXBaseCollection[] {new GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>()};

      try
      {
      }
      catch ( ArrayIndexOutOfBoundsException e )
      {
      }

      execute(aP0);
   }

   public listprograms( )
   {
      super( -1 , new ModelContext( listprograms.class ), "" );
      Application.init(com.oea5pruebadeconcepto.GXcfg.class);
   }

   public listprograms( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( listprograms.class ), "" );
   }

   public listprograms( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   @SuppressWarnings("unchecked")
   public GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName> executeUdp( )
   {
      GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>[] aP0 = new GXBaseCollection[] {new GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>()};
      execute_int(aP0);
      return aP0[0];
   }

   public void execute( GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>[] aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>[] aP0 )
   {
      listprograms.this.aP0 = aP0;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
   }

   protected void cleanup( )
   {
      this.aP0[0] = listprograms.this.AV2ProgramNames;
      CloseOpenCursors();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV2ProgramNames = new GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>(com.oea5pruebadeconcepto.SdtProgramNames_ProgramName.class, "ProgramName", "OEA5pruebaDeConcepto", remoteHandle);
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>[] aP0 ;
   private GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName> AV2ProgramNames ;
}

