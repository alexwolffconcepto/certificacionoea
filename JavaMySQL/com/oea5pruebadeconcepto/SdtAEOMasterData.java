package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.xml.*;
import com.genexus.search.*;
import com.genexus.webpanels.*;
import java.util.*;

public final  class SdtAEOMasterData extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtAEOMasterData( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtAEOMasterData.class));
   }

   public SdtAEOMasterData( int remoteHandle ,
                            ModelContext context )
   {
      super( remoteHandle, context, "SdtAEOMasterData");
      initialize( remoteHandle) ;
   }

   public SdtAEOMasterData( int remoteHandle ,
                            StructSdtAEOMasterData struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   private static java.util.HashMap mapper = new java.util.HashMap();
   static
   {
   }

   public String getJsonMap( String value )
   {
      return (String) mapper.get(value);
   }

   public void Load( String AV1IdentificationIssuingCountryCode ,
                     String AV5PartieID )
   {
      IGxSilentTrn obj;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV1IdentificationIssuingCountryCode,AV5PartieID});
   }

   public Object[][] GetBCKey( )
   {
      return (Object[][])(new Object[][]{new Object[]{"IdentificationIssuingCountryCode", String.class}, new Object[]{"PartieID", String.class}}) ;
   }

   public com.genexus.util.GXProperties getMetadata( )
   {
      com.genexus.util.GXProperties metadata = new com.genexus.util.GXProperties();
      metadata.set("Name", "AEOMasterData");
      metadata.set("BT", "AEOMasterData");
      metadata.set("PK", "[ \"IdentificationIssuingCountryCode\",\"PartieID\" ]");
      metadata.set("PKAssigned", "[ \"IdentificationIssuingCountryCode\" ]");
      metadata.set("FKList", "[ { \"FK\":[ \"CountryCode\" ],\"FKMap\":[ \"IdentificationIssuingCountryCode-CountryCode\" ] },{ \"FK\":[ \"PartieRoleCode\" ],\"FKMap\":[  ] } ]");
      metadata.set("AllowInsert", "True");
      metadata.set("AllowUpdate", "True");
      metadata.set("AllowDelete", "True");
      return metadata ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError = 1;
      formatError = false ;
      sTagName = oReader.getName() ;
      if ( oReader.getIsSimple() == 0 )
      {
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(0) ;
         while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
         {
            readOk = (short)(0) ;
            readElement = false ;
            if ( GXutil.strcmp2( oReader.getLocalName(), "IdentificationIssuingCountryCode") )
            {
               gxTv_SdtAEOMasterData_Identificationissuingcountrycode = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "IdentificationIssuingCountryName") )
            {
               gxTv_SdtAEOMasterData_Identificationissuingcountryname = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "PartieID") )
            {
               gxTv_SdtAEOMasterData_Partieid = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "PartieName") )
            {
               gxTv_SdtAEOMasterData_Partiename = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "PartieShortName") )
            {
               gxTv_SdtAEOMasterData_Partieshortname = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "PartieRoleCode") )
            {
               gxTv_SdtAEOMasterData_Partierolecode = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "Mode") )
            {
               gxTv_SdtAEOMasterData_Mode = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "Initialized") )
            {
               gxTv_SdtAEOMasterData_Initialized = (short)(getnumericvalue(oReader.getValue())) ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "IdentificationIssuingCountryCode_Z") )
            {
               gxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "IdentificationIssuingCountryName_Z") )
            {
               gxTv_SdtAEOMasterData_Identificationissuingcountryname_Z = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "PartieID_Z") )
            {
               gxTv_SdtAEOMasterData_Partieid_Z = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "PartieName_Z") )
            {
               gxTv_SdtAEOMasterData_Partiename_Z = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "PartieShortName_Z") )
            {
               gxTv_SdtAEOMasterData_Partieshortname_Z = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "PartieRoleCode_Z") )
            {
               gxTv_SdtAEOMasterData_Partierolecode_Z = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( ! readElement )
            {
               readOk = (short)(1) ;
               GXSoapError = oReader.read() ;
            }
            nOutParmCount = (short)(nOutParmCount+1) ;
            if ( ( readOk == 0 ) || formatError )
            {
               context.globals.sSOAPErrMsg += "Error reading " + sTagName + GXutil.newLine( ) ;
               context.globals.sSOAPErrMsg += "Message: " + oReader.readRawXML() ;
               GXSoapError = (short)(nOutParmCount*-1) ;
            }
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      writexml(oWriter, sName, sNameSpace, true);
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace ,
                         boolean sIncludeState )
   {
      if ( (GXutil.strcmp("", sName)==0) )
      {
         sName = "AEOMasterData" ;
      }
      if ( (GXutil.strcmp("", sNameSpace)==0) )
      {
         sNameSpace = "OEA5pruebaDeConcepto" ;
      }
      oWriter.writeStartElement(sName);
      if ( GXutil.strcmp(GXutil.left( sNameSpace, 10), "[*:nosend]") != 0 )
      {
         oWriter.writeAttribute("xmlns", sNameSpace);
      }
      else
      {
         sNameSpace = GXutil.right( sNameSpace, GXutil.len( sNameSpace)-10) ;
      }
      oWriter.writeElement("IdentificationIssuingCountryCode", GXutil.rtrim( gxTv_SdtAEOMasterData_Identificationissuingcountrycode));
      if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
      {
         oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
      }
      oWriter.writeElement("IdentificationIssuingCountryName", GXutil.rtrim( gxTv_SdtAEOMasterData_Identificationissuingcountryname));
      if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
      {
         oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
      }
      oWriter.writeElement("PartieID", GXutil.rtrim( gxTv_SdtAEOMasterData_Partieid));
      if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
      {
         oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
      }
      oWriter.writeElement("PartieName", GXutil.rtrim( gxTv_SdtAEOMasterData_Partiename));
      if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
      {
         oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
      }
      oWriter.writeElement("PartieShortName", GXutil.rtrim( gxTv_SdtAEOMasterData_Partieshortname));
      if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
      {
         oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
      }
      oWriter.writeElement("PartieRoleCode", GXutil.rtrim( gxTv_SdtAEOMasterData_Partierolecode));
      if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
      {
         oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
      }
      if ( sIncludeState )
      {
         oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtAEOMasterData_Mode));
         if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
         {
            oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
         }
         oWriter.writeElement("Initialized", GXutil.trim( GXutil.str( gxTv_SdtAEOMasterData_Initialized, 4, 0)));
         if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
         {
            oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
         }
         oWriter.writeElement("IdentificationIssuingCountryCode_Z", GXutil.rtrim( gxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z));
         if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
         {
            oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
         }
         oWriter.writeElement("IdentificationIssuingCountryName_Z", GXutil.rtrim( gxTv_SdtAEOMasterData_Identificationissuingcountryname_Z));
         if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
         {
            oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
         }
         oWriter.writeElement("PartieID_Z", GXutil.rtrim( gxTv_SdtAEOMasterData_Partieid_Z));
         if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
         {
            oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
         }
         oWriter.writeElement("PartieName_Z", GXutil.rtrim( gxTv_SdtAEOMasterData_Partiename_Z));
         if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
         {
            oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
         }
         oWriter.writeElement("PartieShortName_Z", GXutil.rtrim( gxTv_SdtAEOMasterData_Partieshortname_Z));
         if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
         {
            oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
         }
         oWriter.writeElement("PartieRoleCode_Z", GXutil.rtrim( gxTv_SdtAEOMasterData_Partierolecode_Z));
         if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
         {
            oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
         }
      }
      oWriter.writeEndElement();
   }

   public long getnumericvalue( String value )
   {
      if ( GXutil.notNumeric( value) )
      {
         formatError = true ;
      }
      return GXutil.lval( value) ;
   }

   public void tojson( )
   {
      tojson( true) ;
   }

   public void tojson( boolean includeState )
   {
      tojson( includeState, true) ;
   }

   public void tojson( boolean includeState ,
                       boolean includeNonInitialized )
   {
      AddObjectProperty("IdentificationIssuingCountryCode", gxTv_SdtAEOMasterData_Identificationissuingcountrycode, false, includeNonInitialized);
      AddObjectProperty("IdentificationIssuingCountryName", gxTv_SdtAEOMasterData_Identificationissuingcountryname, false, includeNonInitialized);
      AddObjectProperty("PartieID", gxTv_SdtAEOMasterData_Partieid, false, includeNonInitialized);
      AddObjectProperty("PartieName", gxTv_SdtAEOMasterData_Partiename, false, includeNonInitialized);
      AddObjectProperty("PartieShortName", gxTv_SdtAEOMasterData_Partieshortname, false, includeNonInitialized);
      AddObjectProperty("PartieRoleCode", gxTv_SdtAEOMasterData_Partierolecode, false, includeNonInitialized);
      if ( includeState )
      {
         AddObjectProperty("Mode", gxTv_SdtAEOMasterData_Mode, false, includeNonInitialized);
         AddObjectProperty("Initialized", gxTv_SdtAEOMasterData_Initialized, false, includeNonInitialized);
         AddObjectProperty("IdentificationIssuingCountryCode_Z", gxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z, false, includeNonInitialized);
         AddObjectProperty("IdentificationIssuingCountryName_Z", gxTv_SdtAEOMasterData_Identificationissuingcountryname_Z, false, includeNonInitialized);
         AddObjectProperty("PartieID_Z", gxTv_SdtAEOMasterData_Partieid_Z, false, includeNonInitialized);
         AddObjectProperty("PartieName_Z", gxTv_SdtAEOMasterData_Partiename_Z, false, includeNonInitialized);
         AddObjectProperty("PartieShortName_Z", gxTv_SdtAEOMasterData_Partieshortname_Z, false, includeNonInitialized);
         AddObjectProperty("PartieRoleCode_Z", gxTv_SdtAEOMasterData_Partierolecode_Z, false, includeNonInitialized);
      }
   }

   public void updateDirties( com.oea5pruebadeconcepto.SdtAEOMasterData sdt )
   {
      if ( sdt.IsDirty("IdentificationIssuingCountryCode") )
      {
         gxTv_SdtAEOMasterData_N = (byte)(0) ;
         gxTv_SdtAEOMasterData_Identificationissuingcountrycode = sdt.getgxTv_SdtAEOMasterData_Identificationissuingcountrycode() ;
      }
      if ( sdt.IsDirty("IdentificationIssuingCountryName") )
      {
         gxTv_SdtAEOMasterData_N = (byte)(0) ;
         gxTv_SdtAEOMasterData_Identificationissuingcountryname = sdt.getgxTv_SdtAEOMasterData_Identificationissuingcountryname() ;
      }
      if ( sdt.IsDirty("PartieID") )
      {
         gxTv_SdtAEOMasterData_N = (byte)(0) ;
         gxTv_SdtAEOMasterData_Partieid = sdt.getgxTv_SdtAEOMasterData_Partieid() ;
      }
      if ( sdt.IsDirty("PartieName") )
      {
         gxTv_SdtAEOMasterData_N = (byte)(0) ;
         gxTv_SdtAEOMasterData_Partiename = sdt.getgxTv_SdtAEOMasterData_Partiename() ;
      }
      if ( sdt.IsDirty("PartieShortName") )
      {
         gxTv_SdtAEOMasterData_N = (byte)(0) ;
         gxTv_SdtAEOMasterData_Partieshortname = sdt.getgxTv_SdtAEOMasterData_Partieshortname() ;
      }
      if ( sdt.IsDirty("PartieRoleCode") )
      {
         gxTv_SdtAEOMasterData_N = (byte)(0) ;
         gxTv_SdtAEOMasterData_Partierolecode = sdt.getgxTv_SdtAEOMasterData_Partierolecode() ;
      }
   }

   public String getgxTv_SdtAEOMasterData_Identificationissuingcountrycode( )
   {
      return gxTv_SdtAEOMasterData_Identificationissuingcountrycode ;
   }

   public void setgxTv_SdtAEOMasterData_Identificationissuingcountrycode( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      if ( GXutil.strcmp(gxTv_SdtAEOMasterData_Identificationissuingcountrycode, value) != 0 )
      {
         gxTv_SdtAEOMasterData_Mode = "INS" ;
         this.setgxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z_SetNull( );
         this.setgxTv_SdtAEOMasterData_Identificationissuingcountryname_Z_SetNull( );
         this.setgxTv_SdtAEOMasterData_Partieid_Z_SetNull( );
         this.setgxTv_SdtAEOMasterData_Partiename_Z_SetNull( );
         this.setgxTv_SdtAEOMasterData_Partieshortname_Z_SetNull( );
         this.setgxTv_SdtAEOMasterData_Partierolecode_Z_SetNull( );
      }
      SetDirty("Identificationissuingcountrycode");
      gxTv_SdtAEOMasterData_Identificationissuingcountrycode = value ;
   }

   public String getgxTv_SdtAEOMasterData_Identificationissuingcountryname( )
   {
      return gxTv_SdtAEOMasterData_Identificationissuingcountryname ;
   }

   public void setgxTv_SdtAEOMasterData_Identificationissuingcountryname( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      SetDirty("Identificationissuingcountryname");
      gxTv_SdtAEOMasterData_Identificationissuingcountryname = value ;
   }

   public String getgxTv_SdtAEOMasterData_Partieid( )
   {
      return gxTv_SdtAEOMasterData_Partieid ;
   }

   public void setgxTv_SdtAEOMasterData_Partieid( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      if ( GXutil.strcmp(gxTv_SdtAEOMasterData_Partieid, value) != 0 )
      {
         gxTv_SdtAEOMasterData_Mode = "INS" ;
         this.setgxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z_SetNull( );
         this.setgxTv_SdtAEOMasterData_Identificationissuingcountryname_Z_SetNull( );
         this.setgxTv_SdtAEOMasterData_Partieid_Z_SetNull( );
         this.setgxTv_SdtAEOMasterData_Partiename_Z_SetNull( );
         this.setgxTv_SdtAEOMasterData_Partieshortname_Z_SetNull( );
         this.setgxTv_SdtAEOMasterData_Partierolecode_Z_SetNull( );
      }
      SetDirty("Partieid");
      gxTv_SdtAEOMasterData_Partieid = value ;
   }

   public String getgxTv_SdtAEOMasterData_Partiename( )
   {
      return gxTv_SdtAEOMasterData_Partiename ;
   }

   public void setgxTv_SdtAEOMasterData_Partiename( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      SetDirty("Partiename");
      gxTv_SdtAEOMasterData_Partiename = value ;
   }

   public String getgxTv_SdtAEOMasterData_Partieshortname( )
   {
      return gxTv_SdtAEOMasterData_Partieshortname ;
   }

   public void setgxTv_SdtAEOMasterData_Partieshortname( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      SetDirty("Partieshortname");
      gxTv_SdtAEOMasterData_Partieshortname = value ;
   }

   public String getgxTv_SdtAEOMasterData_Partierolecode( )
   {
      return gxTv_SdtAEOMasterData_Partierolecode ;
   }

   public void setgxTv_SdtAEOMasterData_Partierolecode( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      SetDirty("Partierolecode");
      gxTv_SdtAEOMasterData_Partierolecode = value ;
   }

   public String getgxTv_SdtAEOMasterData_Mode( )
   {
      return gxTv_SdtAEOMasterData_Mode ;
   }

   public void setgxTv_SdtAEOMasterData_Mode( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      SetDirty("Mode");
      gxTv_SdtAEOMasterData_Mode = value ;
   }

   public void setgxTv_SdtAEOMasterData_Mode_SetNull( )
   {
      gxTv_SdtAEOMasterData_Mode = "" ;
   }

   public boolean getgxTv_SdtAEOMasterData_Mode_IsNull( )
   {
      return false ;
   }

   public short getgxTv_SdtAEOMasterData_Initialized( )
   {
      return gxTv_SdtAEOMasterData_Initialized ;
   }

   public void setgxTv_SdtAEOMasterData_Initialized( short value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      SetDirty("Initialized");
      gxTv_SdtAEOMasterData_Initialized = value ;
   }

   public void setgxTv_SdtAEOMasterData_Initialized_SetNull( )
   {
      gxTv_SdtAEOMasterData_Initialized = (short)(0) ;
   }

   public boolean getgxTv_SdtAEOMasterData_Initialized_IsNull( )
   {
      return false ;
   }

   public String getgxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z( )
   {
      return gxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z ;
   }

   public void setgxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      SetDirty("Identificationissuingcountrycode_Z");
      gxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z = value ;
   }

   public void setgxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z_SetNull( )
   {
      gxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z = "" ;
   }

   public boolean getgxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z_IsNull( )
   {
      return false ;
   }

   public String getgxTv_SdtAEOMasterData_Identificationissuingcountryname_Z( )
   {
      return gxTv_SdtAEOMasterData_Identificationissuingcountryname_Z ;
   }

   public void setgxTv_SdtAEOMasterData_Identificationissuingcountryname_Z( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      SetDirty("Identificationissuingcountryname_Z");
      gxTv_SdtAEOMasterData_Identificationissuingcountryname_Z = value ;
   }

   public void setgxTv_SdtAEOMasterData_Identificationissuingcountryname_Z_SetNull( )
   {
      gxTv_SdtAEOMasterData_Identificationissuingcountryname_Z = "" ;
   }

   public boolean getgxTv_SdtAEOMasterData_Identificationissuingcountryname_Z_IsNull( )
   {
      return false ;
   }

   public String getgxTv_SdtAEOMasterData_Partieid_Z( )
   {
      return gxTv_SdtAEOMasterData_Partieid_Z ;
   }

   public void setgxTv_SdtAEOMasterData_Partieid_Z( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      SetDirty("Partieid_Z");
      gxTv_SdtAEOMasterData_Partieid_Z = value ;
   }

   public void setgxTv_SdtAEOMasterData_Partieid_Z_SetNull( )
   {
      gxTv_SdtAEOMasterData_Partieid_Z = "" ;
   }

   public boolean getgxTv_SdtAEOMasterData_Partieid_Z_IsNull( )
   {
      return false ;
   }

   public String getgxTv_SdtAEOMasterData_Partiename_Z( )
   {
      return gxTv_SdtAEOMasterData_Partiename_Z ;
   }

   public void setgxTv_SdtAEOMasterData_Partiename_Z( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      SetDirty("Partiename_Z");
      gxTv_SdtAEOMasterData_Partiename_Z = value ;
   }

   public void setgxTv_SdtAEOMasterData_Partiename_Z_SetNull( )
   {
      gxTv_SdtAEOMasterData_Partiename_Z = "" ;
   }

   public boolean getgxTv_SdtAEOMasterData_Partiename_Z_IsNull( )
   {
      return false ;
   }

   public String getgxTv_SdtAEOMasterData_Partieshortname_Z( )
   {
      return gxTv_SdtAEOMasterData_Partieshortname_Z ;
   }

   public void setgxTv_SdtAEOMasterData_Partieshortname_Z( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      SetDirty("Partieshortname_Z");
      gxTv_SdtAEOMasterData_Partieshortname_Z = value ;
   }

   public void setgxTv_SdtAEOMasterData_Partieshortname_Z_SetNull( )
   {
      gxTv_SdtAEOMasterData_Partieshortname_Z = "" ;
   }

   public boolean getgxTv_SdtAEOMasterData_Partieshortname_Z_IsNull( )
   {
      return false ;
   }

   public String getgxTv_SdtAEOMasterData_Partierolecode_Z( )
   {
      return gxTv_SdtAEOMasterData_Partierolecode_Z ;
   }

   public void setgxTv_SdtAEOMasterData_Partierolecode_Z( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      SetDirty("Partierolecode_Z");
      gxTv_SdtAEOMasterData_Partierolecode_Z = value ;
   }

   public void setgxTv_SdtAEOMasterData_Partierolecode_Z_SetNull( )
   {
      gxTv_SdtAEOMasterData_Partierolecode_Z = "" ;
   }

   public boolean getgxTv_SdtAEOMasterData_Partierolecode_Z_IsNull( )
   {
      return false ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      com.oea5pruebadeconcepto.aeomasterdata_bc obj;
      obj = new com.oea5pruebadeconcepto.aeomasterdata_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
   }

   public void initialize( )
   {
      gxTv_SdtAEOMasterData_Identificationissuingcountrycode = "" ;
      gxTv_SdtAEOMasterData_N = (byte)(1) ;
      gxTv_SdtAEOMasterData_Identificationissuingcountryname = "" ;
      gxTv_SdtAEOMasterData_Partieid = "" ;
      gxTv_SdtAEOMasterData_Partiename = "" ;
      gxTv_SdtAEOMasterData_Partieshortname = "" ;
      gxTv_SdtAEOMasterData_Partierolecode = "" ;
      gxTv_SdtAEOMasterData_Mode = "" ;
      gxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z = "" ;
      gxTv_SdtAEOMasterData_Identificationissuingcountryname_Z = "" ;
      gxTv_SdtAEOMasterData_Partieid_Z = "" ;
      gxTv_SdtAEOMasterData_Partiename_Z = "" ;
      gxTv_SdtAEOMasterData_Partieshortname_Z = "" ;
      gxTv_SdtAEOMasterData_Partierolecode_Z = "" ;
      sTagName = "" ;
   }

   public byte isNull( )
   {
      return gxTv_SdtAEOMasterData_N ;
   }

   public com.oea5pruebadeconcepto.SdtAEOMasterData Clone( )
   {
      com.oea5pruebadeconcepto.SdtAEOMasterData sdt;
      com.oea5pruebadeconcepto.aeomasterdata_bc obj;
      sdt = (com.oea5pruebadeconcepto.SdtAEOMasterData)(clone()) ;
      obj = (com.oea5pruebadeconcepto.aeomasterdata_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( com.oea5pruebadeconcepto.StructSdtAEOMasterData struct )
   {
      setgxTv_SdtAEOMasterData_Identificationissuingcountrycode(struct.getIdentificationissuingcountrycode());
      setgxTv_SdtAEOMasterData_Identificationissuingcountryname(struct.getIdentificationissuingcountryname());
      setgxTv_SdtAEOMasterData_Partieid(struct.getPartieid());
      setgxTv_SdtAEOMasterData_Partiename(struct.getPartiename());
      setgxTv_SdtAEOMasterData_Partieshortname(struct.getPartieshortname());
      setgxTv_SdtAEOMasterData_Partierolecode(struct.getPartierolecode());
      setgxTv_SdtAEOMasterData_Mode(struct.getMode());
      setgxTv_SdtAEOMasterData_Initialized(struct.getInitialized());
      setgxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z(struct.getIdentificationissuingcountrycode_Z());
      setgxTv_SdtAEOMasterData_Identificationissuingcountryname_Z(struct.getIdentificationissuingcountryname_Z());
      setgxTv_SdtAEOMasterData_Partieid_Z(struct.getPartieid_Z());
      setgxTv_SdtAEOMasterData_Partiename_Z(struct.getPartiename_Z());
      setgxTv_SdtAEOMasterData_Partieshortname_Z(struct.getPartieshortname_Z());
      setgxTv_SdtAEOMasterData_Partierolecode_Z(struct.getPartierolecode_Z());
   }

   @SuppressWarnings("unchecked")
   public com.oea5pruebadeconcepto.StructSdtAEOMasterData getStruct( )
   {
      com.oea5pruebadeconcepto.StructSdtAEOMasterData struct = new com.oea5pruebadeconcepto.StructSdtAEOMasterData ();
      struct.setIdentificationissuingcountrycode(getgxTv_SdtAEOMasterData_Identificationissuingcountrycode());
      struct.setIdentificationissuingcountryname(getgxTv_SdtAEOMasterData_Identificationissuingcountryname());
      struct.setPartieid(getgxTv_SdtAEOMasterData_Partieid());
      struct.setPartiename(getgxTv_SdtAEOMasterData_Partiename());
      struct.setPartieshortname(getgxTv_SdtAEOMasterData_Partieshortname());
      struct.setPartierolecode(getgxTv_SdtAEOMasterData_Partierolecode());
      struct.setMode(getgxTv_SdtAEOMasterData_Mode());
      struct.setInitialized(getgxTv_SdtAEOMasterData_Initialized());
      struct.setIdentificationissuingcountrycode_Z(getgxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z());
      struct.setIdentificationissuingcountryname_Z(getgxTv_SdtAEOMasterData_Identificationissuingcountryname_Z());
      struct.setPartieid_Z(getgxTv_SdtAEOMasterData_Partieid_Z());
      struct.setPartiename_Z(getgxTv_SdtAEOMasterData_Partiename_Z());
      struct.setPartieshortname_Z(getgxTv_SdtAEOMasterData_Partieshortname_Z());
      struct.setPartierolecode_Z(getgxTv_SdtAEOMasterData_Partierolecode_Z());
      return struct ;
   }

   private byte gxTv_SdtAEOMasterData_N ;
   private short gxTv_SdtAEOMasterData_Initialized ;
   private short readOk ;
   private short nOutParmCount ;
   private String gxTv_SdtAEOMasterData_Identificationissuingcountrycode ;
   private String gxTv_SdtAEOMasterData_Partieid ;
   private String gxTv_SdtAEOMasterData_Partierolecode ;
   private String gxTv_SdtAEOMasterData_Mode ;
   private String gxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z ;
   private String gxTv_SdtAEOMasterData_Partieid_Z ;
   private String gxTv_SdtAEOMasterData_Partierolecode_Z ;
   private String sTagName ;
   private boolean readElement ;
   private boolean formatError ;
   private String gxTv_SdtAEOMasterData_Identificationissuingcountryname ;
   private String gxTv_SdtAEOMasterData_Partiename ;
   private String gxTv_SdtAEOMasterData_Partieshortname ;
   private String gxTv_SdtAEOMasterData_Identificationissuingcountryname_Z ;
   private String gxTv_SdtAEOMasterData_Partiename_Z ;
   private String gxTv_SdtAEOMasterData_Partieshortname_Z ;
}

