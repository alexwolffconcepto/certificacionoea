package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(urlPatterns = {"/servlet/com.oea5pruebadeconcepto.aeomasterdatageneral", "/com.oea5pruebadeconcepto.aeomasterdatageneral"})
public final  class aeomasterdatageneral extends GXWebObjectStub
{
   public aeomasterdatageneral( )
   {
   }

   public aeomasterdatageneral( int remoteHandle )
   {
      super(remoteHandle, new ModelContext( aeomasterdatageneral.class ));
   }

   public aeomasterdatageneral( int remoteHandle ,
                                ModelContext context )
   {
      super(remoteHandle, context);
   }

   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new aeomasterdatageneral_impl(context).doExecute();
   }

   protected void init( com.genexus.internet.HttpContext context )
   {
      new aeomasterdatageneral_impl(context).cleanup();
   }

   public String getServletInfo( )
   {
      return "AEOMaster Data General";
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

   protected String IntegratedSecurityPermissionPrefix( )
   {
      return "";
   }

   protected String EncryptURLParameters( )
   {
      return "NO";
   }

}

