package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.util.*;

public final  class reorg extends GXProcedure
{
   public reorg( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( reorg.class ), "" );
   }

   public reorg( int remoteHandle ,
                 ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      initialize();
      if ( previousCheck() )
      {
         executeReorganization( ) ;
      }
   }

   private void FirstActions( )
   {
      /* Load data into tables. */
   }

   public void ReorganizePartieRole( ) throws SQLException
   {
      String cmdBufferCreate = "";
      String cmdBuffer = "";
      /* Indices for table PartieRole */
      try
      {
         cmdBufferCreate = " CREATE TABLE `PartieRole` (`PartieRoleCode` national char(2) NOT NULL , `PartieRoleName` national varchar(30) NOT NULL , PRIMARY KEY(`PartieRoleCode`))  ENGINE=InnoDB ";
         cmdBufferCreate += "  ";
         ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBufferCreate) ;
      }
      catch(SQLException ex)
      {
         try
         {
            sSchemaVar = GXutil.databaseName( context, remoteHandle, "DEFAULT") ;
            DropTableConstraints( "PartieRole", sSchemaVar) ;
            cmdBuffer = " DROP TABLE `PartieRole` ";
            ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
         }
         catch(SQLException sqlex1)
         {
            try
            {
               sSchemaVar = GXutil.databaseName( context, remoteHandle, "DEFAULT") ;
               DropTableConstraints( "PartieRole", sSchemaVar) ;
               cmdBuffer = " DROP VIEW `PartieRole` ";
               ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
            }
            catch(SQLException sqlex2)
            {
               try
               {
                  sSchemaVar = GXutil.databaseName( context, remoteHandle, "DEFAULT") ;
                  DropTableConstraints( "PartieRole", sSchemaVar) ;
                  cmdBuffer = " DROP FUNCTION `PartieRole` ";
                  ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
               }
               catch(SQLException sqlex3)
               {
               }
            }
         }
         ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBufferCreate) ;
      }
      cmdBuffer = " INSERT IGNORE INTO `PartieRole` (`PartieRoleCode`, `PartieRoleName`) (SELECT `PartieRoleCode`, '' FROM `AEOMasterData`) ";
      ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
   }

   public void ReorganizeAEOMasterData( ) throws SQLException
   {
      String cmdBufferCreate = "";
      String cmdBuffer = "";
      /* Indices for table AEOMasterData */
      try
      {
         cmdBuffer = " CREATE INDEX `IAEOMASTERDATA2` ON `AEOMasterData` (`PartieRoleCode` ) ";
         ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
      }
      catch(SQLException ex)
      {
         cmdBuffer = " DROP INDEX `IAEOMASTERDATA2` ON `AEOMasterData` ";
         ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
         cmdBuffer = " CREATE INDEX `IAEOMASTERDATA2` ON `AEOMasterData` (`PartieRoleCode` ) ";
         ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
      }
   }

   public void RIAEOMasterDataPartieRole( ) throws SQLException
   {
      String cmdBuffer;
      try
      {
         cmdBuffer = " ALTER TABLE `AEOMasterData` ADD CONSTRAINT `IAEOMASTERDATA2` FOREIGN KEY (`PartieRoleCode`) REFERENCES `PartieRole` (`PartieRoleCode`) ";
         ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
      }
      catch(SQLException ex)
      {
         try
         {
            cmdBuffer = " ALTER TABLE `AEOMasterData` DROP FOREIGN KEY `IAEOMASTERDATA2` ";
            ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
         }
         catch(SQLException sqlex1)
         {
         }
         cmdBuffer = " ALTER TABLE `AEOMasterData` ADD CONSTRAINT `IAEOMASTERDATA2` FOREIGN KEY (`PartieRoleCode`) REFERENCES `PartieRole` (`PartieRoleCode`) ";
         ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
      }
   }

   private void tablesCount( )
   {
      if ( ! GXReorganization.isResumeMode( ) )
      {
         /* Using cursor P00012 */
         pr_default.execute(0);
         AEOMasterDataCount = P00012_AAEOMasterDataCount[0] ;
         pr_default.close(0);
         GXReorganization.printRecordCount ( "AEOMasterData" ,  AEOMasterDataCount );
      }
   }

   private boolean previousCheck( )
   {
      if ( ! GXReorganization.isResumeMode( ) )
      {
         if ( GXutil.dbmsVersion( context, remoteHandle, "DEFAULT") < 5 )
         {
            GXReorganization.setCheckError ( localUtil.getMessages().getMessage("GXM_bad_DBMS_version", new Object[] {"5"}) ) ;
            return false ;
         }
      }
      if ( ! GXReorganization.mustRunCheck( ) )
      {
         return true ;
      }
      sSchemaVar = GXutil.databaseName( context, remoteHandle, "DEFAULT") ;
      if ( tableexist("PartieRole",sSchemaVar) )
      {
         GXReorganization.setCheckError ( localUtil.getMessages().getMessage("GXM_table_exist", new Object[] {"PartieRole"}) ) ;
         return false ;
      }
      return true ;
   }

   private boolean tableexist( String sTableName ,
                               String sMySchemaName )
   {
      boolean result;
      result = false ;
      /* Using cursor P00023 */
      pr_default.execute(1, new Object[] {sTableName, sMySchemaName});
      while ( (pr_default.getStatus(1) != 101) )
      {
         tablename = P00023_Atablename[0] ;
         ntablename = P00023_ntablename[0] ;
         schemaname = P00023_Aschemaname[0] ;
         nschemaname = P00023_nschemaname[0] ;
         result = true ;
         pr_default.readNext(1);
      }
      pr_default.close(1);
      return result ;
   }

   private void executeOnlyTablesReorganization( )
   {
      callSubmit( "ReorganizePartieRole" ,  localUtil.getMessages().getMessage("GXM_fileupdate", new Object[] {"PartieRole",""}) ,  1 , new Object[]{ });
      callSubmit( "ReorganizeAEOMasterData" ,  localUtil.getMessages().getMessage("GXM_fileupdate", new Object[] {"AEOMasterData",""}) ,  2 , new Object[]{ });
   }

   private void executeOnlyRisReorganization( )
   {
      callSubmit( "RIAEOMasterDataPartieRole" ,  localUtil.getMessages().getMessage("GXM_refintcrea", new Object[] {"`IAEOMASTERDATA2`"}) ,  3 , new Object[]{ });
   }

   private void executeTablesReorganization( )
   {
      executeOnlyTablesReorganization( ) ;
      executeOnlyRisReorganization( ) ;
      ReorgSubmitThreadPool.startProcess();
   }

   private void setPrecedence( )
   {
      setPrecedencetables( ) ;
      setPrecedenceris( ) ;
   }

   private void setPrecedencetables( )
   {
      GXReorganization.addMsg( 1 ,  localUtil.getMessages().getMessage("GXM_fileupdate", new Object[] {"PartieRole",""}) );
      ReorgSubmitThreadPool.addBlock( "ReorganizePartieRole" );
      GXReorganization.addMsg( 2 ,  localUtil.getMessages().getMessage("GXM_fileupdate", new Object[] {"AEOMasterData",""}) );
      ReorgSubmitThreadPool.addBlock( "ReorganizeAEOMasterData" );
      ReorgSubmitThreadPool.addPrecedence( "ReorganizeAEOMasterData" ,  "ReorganizePartieRole" );
   }

   private void setPrecedenceris( )
   {
      GXReorganization.addMsg( 3 ,  localUtil.getMessages().getMessage("GXM_refintcrea", new Object[] {"`IAEOMASTERDATA2`"}) );
      ReorgSubmitThreadPool.addBlock( "RIAEOMasterDataPartieRole" );
      ReorgSubmitThreadPool.addPrecedence( "RIAEOMasterDataPartieRole" ,  "ReorganizeAEOMasterData" );
      ReorgSubmitThreadPool.addPrecedence( "RIAEOMasterDataPartieRole" ,  "ReorganizePartieRole" );
   }

   private void executeReorganization( )
   {
      if ( ErrCode == 0 )
      {
         tablesCount( ) ;
         if ( ! GXReorganization.getRecordCount( ) )
         {
            FirstActions( ) ;
            setPrecedence( ) ;
            executeTablesReorganization( ) ;
         }
      }
   }

   public synchronized void DropTableConstraints( String sTableName ,
                                                  String sMySchemaName ) throws SQLException
   {
      String cmdBuffer;
      /* Using cursor P00034 */
      pr_default.execute(2, new Object[] {sTableName, sMySchemaName});
      while ( (pr_default.getStatus(2) != 101) )
      {
         tablename = P00034_Atablename[0] ;
         ntablename = P00034_ntablename[0] ;
         referencedtablename = P00034_Areferencedtablename[0] ;
         nreferencedtablename = P00034_nreferencedtablename[0] ;
         constid = P00034_Aconstid[0] ;
         nconstid = P00034_nconstid[0] ;
         schemaname = P00034_Aschemaname[0] ;
         nschemaname = P00034_nschemaname[0] ;
         cmdBuffer = "ALTER TABLE " + "" + tablename + "" + " DROP FOREIGN KEY " + "" + constid + "" ;
         ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
         pr_default.readNext(2);
      }
      pr_default.close(2);
   }

   public void UtilsCleanup( )
   {
      cleanup();
   }

   protected void cleanup( )
   {
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void submitReorg( int submitId ,
                            Object [] submitParms ) throws SQLException
   {
      UserInformation submitUI = Application.getConnectionManager().createUserInformation(Namespace.getNamespace(context.getNAME_SPACE()));
      switch ( submitId )
      {
            case 1 :
               GXReorganization.replaceMsg( 1 ,  localUtil.getMessages().getMessage("GXM_fileupdate", new Object[] {"PartieRole",""})+" STARTED" );
               ReorganizePartieRole( ) ;
               GXReorganization.replaceMsg( 1 ,  localUtil.getMessages().getMessage("GXM_fileupdate", new Object[] {"PartieRole",""})+" ENDED" );
               try { submitUI.disconnect(); } catch(Exception submitExc) { ; }
               break;
            case 2 :
               GXReorganization.replaceMsg( 2 ,  localUtil.getMessages().getMessage("GXM_fileupdate", new Object[] {"AEOMasterData",""})+" STARTED" );
               ReorganizeAEOMasterData( ) ;
               GXReorganization.replaceMsg( 2 ,  localUtil.getMessages().getMessage("GXM_fileupdate", new Object[] {"AEOMasterData",""})+" ENDED" );
               try { submitUI.disconnect(); } catch(Exception submitExc) { ; }
               break;
            case 3 :
               GXReorganization.replaceMsg( 3 ,  localUtil.getMessages().getMessage("GXM_refintcrea", new Object[] {"`IAEOMASTERDATA2`"})+" STARTED" );
               RIAEOMasterDataPartieRole( ) ;
               GXReorganization.replaceMsg( 3 ,  localUtil.getMessages().getMessage("GXM_refintcrea", new Object[] {"`IAEOMASTERDATA2`"})+" ENDED" );
               try { submitUI.disconnect(); } catch(Exception submitExc) { ; }
               break;
      }
   }

   public void initialize( )
   {
      sSchemaVar = "" ;
      scmdbuf = "" ;
      P00012_AAEOMasterDataCount = new int[1] ;
      sTableName = "" ;
      sMySchemaName = "" ;
      tablename = "" ;
      ntablename = false ;
      schemaname = "" ;
      nschemaname = false ;
      P00023_Atablename = new String[] {""} ;
      P00023_ntablename = new boolean[] {false} ;
      P00023_Aschemaname = new String[] {""} ;
      P00023_nschemaname = new boolean[] {false} ;
      referencedtablename = "" ;
      nreferencedtablename = false ;
      constid = "" ;
      nconstid = false ;
      P00034_Atablename = new String[] {""} ;
      P00034_ntablename = new boolean[] {false} ;
      P00034_Areferencedtablename = new String[] {""} ;
      P00034_nreferencedtablename = new boolean[] {false} ;
      P00034_Aconstid = new String[] {""} ;
      P00034_nconstid = new boolean[] {false} ;
      P00034_Aschemaname = new String[] {""} ;
      P00034_nschemaname = new boolean[] {false} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new com.oea5pruebadeconcepto.reorg__default(),
         new Object[] {
             new Object[] {
            P00012_AAEOMasterDataCount
            }
            , new Object[] {
            P00023_Atablename, P00023_Aschemaname
            }
            , new Object[] {
            P00034_Atablename, P00034_Areferencedtablename, P00034_Aconstid, P00034_Aschemaname
            }
         }
      );
      /* GeneXus formulas. */
   }

   protected short ErrCode ;
   protected int AEOMasterDataCount ;
   protected String sSchemaVar ;
   protected String scmdbuf ;
   protected String sTableName ;
   protected String sMySchemaName ;
   protected boolean ntablename ;
   protected boolean nschemaname ;
   protected boolean nreferencedtablename ;
   protected boolean nconstid ;
   protected String tablename ;
   protected String schemaname ;
   protected String referencedtablename ;
   protected String constid ;
   protected IDataStoreProvider pr_default ;
   protected int[] P00012_AAEOMasterDataCount ;
   protected String[] P00023_Atablename ;
   protected boolean[] P00023_ntablename ;
   protected String[] P00023_Aschemaname ;
   protected boolean[] P00023_nschemaname ;
   protected String[] P00034_Atablename ;
   protected boolean[] P00034_ntablename ;
   protected String[] P00034_Areferencedtablename ;
   protected boolean[] P00034_nreferencedtablename ;
   protected String[] P00034_Aconstid ;
   protected boolean[] P00034_nconstid ;
   protected String[] P00034_Aschemaname ;
   protected boolean[] P00034_nschemaname ;
}

final  class reorg__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P00012", "SELECT COUNT(*) FROM `AEOMasterData` ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,100, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("P00023", "SELECT table_name, table_schema FROM INFORMATION_SCHEMA.TABLES WHERE (table_name = ?) AND (table_schema = ?) ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,100, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("P00034", "SELECT DISTINCT TABLE_NAME, REFERENCED_TABLE_NAME, CONSTRAINT_NAME, TABLE_SCHEMA FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE (REFERENCED_TABLE_NAME = ?) AND (TABLE_SCHEMA = ?) ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,100, GxCacheFrequency.OFF,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((int[]) buf[0])[0] = rslt.getInt(1);
               return;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               ((String[]) buf[2])[0] = rslt.getVarchar(3);
               ((String[]) buf[3])[0] = rslt.getVarchar(4);
               return;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               stmt.setString(1, (String)parms[0], 255);
               stmt.setString(2, (String)parms[1], 255);
               return;
            case 2 :
               stmt.setString(1, (String)parms[0], 255);
               stmt.setString(2, (String)parms[1], 255);
               return;
      }
   }

}

