package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class aeomasterdata_bc extends GXWebPanel implements IGxSilentTrn
{
   public aeomasterdata_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public aeomasterdata_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( aeomasterdata_bc.class ));
   }

   public aeomasterdata_bc( int remoteHandle ,
                            ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public GXBCCollection<com.oea5pruebadeconcepto.SdtAEOMasterData> GetAll( int Start ,
                                                                            int Count )
   {
      GXPagingFrom3 = Start ;
      GXPagingTo3 = ((Count>0) ? Count : 100000000) ;
      /* Using cursor BC00016 */
      pr_default.execute(4, new Object[] {Integer.valueOf(GXPagingFrom3), Integer.valueOf(GXPagingTo3)});
      RcdFound3 = (short)(0) ;
      if ( (pr_default.getStatus(4) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A5PartieID = BC00016_A5PartieID[0] ;
         A4IdentificationIssuingCountryNa = BC00016_A4IdentificationIssuingCountryNa[0] ;
         A6PartieName = BC00016_A6PartieName[0] ;
         A7PartieShortName = BC00016_A7PartieShortName[0] ;
         A8PartieRoleCode = BC00016_A8PartieRoleCode[0] ;
         A1IdentificationIssuingCountryCo = BC00016_A1IdentificationIssuingCountryCo[0] ;
      }
      bcAEOMasterData = new com.oea5pruebadeconcepto.SdtAEOMasterData(remoteHandle);
      gx_restcollection.clear();
      while ( RcdFound3 != 0 )
      {
         onLoadActions013( ) ;
         addRow013( ) ;
         gx_sdt_item = bcAEOMasterData.Clone() ;
         gx_restcollection.add(gx_sdt_item, 0);
         pr_default.readNext(4);
         RcdFound3 = (short)(0) ;
         sMode3 = Gx_mode ;
         Gx_mode = "DSP" ;
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound3 = (short)(1) ;
            A5PartieID = BC00016_A5PartieID[0] ;
            A4IdentificationIssuingCountryNa = BC00016_A4IdentificationIssuingCountryNa[0] ;
            A6PartieName = BC00016_A6PartieName[0] ;
            A7PartieShortName = BC00016_A7PartieShortName[0] ;
            A8PartieRoleCode = BC00016_A8PartieRoleCode[0] ;
            A1IdentificationIssuingCountryCo = BC00016_A1IdentificationIssuingCountryCo[0] ;
         }
         Gx_mode = sMode3 ;
      }
      pr_default.close(4);
      /* Load Subordinate Levels */
      return gx_restcollection ;
   }

   public void setseudovars( )
   {
      zm013( 0) ;
   }

   public void getInsDefault( )
   {
      readRow013( ) ;
      standaloneNotModal( ) ;
      initializeNonKey013( ) ;
      standaloneModal( ) ;
      addRow013( ) ;
      Gx_mode = "INS" ;
   }

   public void afterTrn( )
   {
      if ( trnEnded == 1 )
      {
         if ( ! (GXutil.strcmp("", endTrnMsgTxt)==0) )
         {
            httpContext.GX_msglist.addItem(endTrnMsgTxt, endTrnMsgCod, 0, "", true);
         }
         /* Execute user event: After Trn */
         e11012 ();
         trnEnded = 0 ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( isIns( )  )
         {
            Z1IdentificationIssuingCountryCo = A1IdentificationIssuingCountryCo ;
            Z5PartieID = A5PartieID ;
            SetMode( "UPD") ;
         }
      }
      endTrnMsgTxt = "" ;
   }

   public String toString( )
   {
      return "" ;
   }

   public GXContentInfo getContentInfo( )
   {
      return (GXContentInfo)(null) ;
   }

   public boolean Reindex( )
   {
      return true ;
   }

   public void confirm_010( )
   {
      beforeValidate013( ) ;
      if ( AnyError == 0 )
      {
         if ( isDlt( ) )
         {
            onDeleteControls013( ) ;
         }
         else
         {
            checkExtendedTable013( ) ;
            if ( AnyError == 0 )
            {
               zm013( 2) ;
               zm013( 3) ;
            }
            closeExtendedTableCursors013( ) ;
         }
      }
      if ( AnyError == 0 )
      {
         IsConfirmed = (short)(1) ;
      }
   }

   public void e12012( )
   {
      /* Start Routine */
      returnInSub = false ;
   }

   public void e11012( )
   {
      /* After Trn Routine */
      returnInSub = false ;
   }

   public void zm013( int GX_JID )
   {
      if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
      {
         Z6PartieName = A6PartieName ;
         Z7PartieShortName = A7PartieShortName ;
         Z8PartieRoleCode = A8PartieRoleCode ;
      }
      if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
      {
      }
      if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
      {
         Z4IdentificationIssuingCountryNa = A4IdentificationIssuingCountryNa ;
      }
      if ( GX_JID == -1 )
      {
         Z5PartieID = A5PartieID ;
         Z6PartieName = A6PartieName ;
         Z7PartieShortName = A7PartieShortName ;
         Z8PartieRoleCode = A8PartieRoleCode ;
         Z1IdentificationIssuingCountryCo = A1IdentificationIssuingCountryCo ;
         Z4IdentificationIssuingCountryNa = A4IdentificationIssuingCountryNa ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
   }

   public void load013( )
   {
      /* Using cursor BC00017 */
      pr_default.execute(5, new Object[] {A1IdentificationIssuingCountryCo, A5PartieID});
      if ( (pr_default.getStatus(5) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A4IdentificationIssuingCountryNa = BC00017_A4IdentificationIssuingCountryNa[0] ;
         A6PartieName = BC00017_A6PartieName[0] ;
         A7PartieShortName = BC00017_A7PartieShortName[0] ;
         A8PartieRoleCode = BC00017_A8PartieRoleCode[0] ;
         zm013( -1) ;
      }
      pr_default.close(5);
      onLoadActions013( ) ;
   }

   public void onLoadActions013( )
   {
   }

   public void checkExtendedTable013( )
   {
      nIsDirty_3 = (short)(0) ;
      standaloneModal( ) ;
      /* Using cursor BC00018 */
      pr_default.execute(6, new Object[] {A1IdentificationIssuingCountryCo});
      if ( (pr_default.getStatus(6) == 101) )
      {
         httpContext.GX_msglist.addItem("No existe 'AEOMaster Data Country'.", "ForeignKeyNotFound", 1, "IDENTIFICATIONISSUINGCOUNTRYCO");
         AnyError = (short)(1) ;
      }
      A4IdentificationIssuingCountryNa = BC00018_A4IdentificationIssuingCountryNa[0] ;
      pr_default.close(6);
      /* Using cursor BC00019 */
      pr_default.execute(7, new Object[] {A8PartieRoleCode});
      if ( (pr_default.getStatus(7) == 101) )
      {
         httpContext.GX_msglist.addItem("No existe 'Partie Role'.", "ForeignKeyNotFound", 1, "PARTIEROLECODE");
         AnyError = (short)(1) ;
      }
      pr_default.close(7);
   }

   public void closeExtendedTableCursors013( )
   {
      pr_default.close(6);
      pr_default.close(7);
   }

   public void enableDisable( )
   {
   }

   public void getKey013( )
   {
      /* Using cursor BC000110 */
      pr_default.execute(8, new Object[] {A1IdentificationIssuingCountryCo, A5PartieID});
      if ( (pr_default.getStatus(8) != 101) )
      {
         RcdFound3 = (short)(1) ;
      }
      else
      {
         RcdFound3 = (short)(0) ;
      }
      pr_default.close(8);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC000111 */
      pr_default.execute(9, new Object[] {A1IdentificationIssuingCountryCo, A5PartieID});
      if ( (pr_default.getStatus(9) != 101) )
      {
         zm013( 1) ;
         RcdFound3 = (short)(1) ;
         A5PartieID = BC000111_A5PartieID[0] ;
         A6PartieName = BC000111_A6PartieName[0] ;
         A7PartieShortName = BC000111_A7PartieShortName[0] ;
         A8PartieRoleCode = BC000111_A8PartieRoleCode[0] ;
         A1IdentificationIssuingCountryCo = BC000111_A1IdentificationIssuingCountryCo[0] ;
         Z1IdentificationIssuingCountryCo = A1IdentificationIssuingCountryCo ;
         Z5PartieID = A5PartieID ;
         sMode3 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load013( ) ;
         if ( AnyError == 1 )
         {
            RcdFound3 = (short)(0) ;
            initializeNonKey013( ) ;
         }
         Gx_mode = sMode3 ;
      }
      else
      {
         RcdFound3 = (short)(0) ;
         initializeNonKey013( ) ;
         sMode3 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode3 ;
      }
      pr_default.close(9);
   }

   public void getEqualNoModal( )
   {
      getKey013( ) ;
      if ( RcdFound3 == 0 )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_010( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency013( )
   {
      if ( ! isIns( ) )
      {
         /* Using cursor BC000112 */
         pr_default.execute(10, new Object[] {A1IdentificationIssuingCountryCo, A5PartieID});
         if ( (pr_default.getStatus(10) == 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"AEOMasterData"}), "RecordIsLocked", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(10) == 101) || ( GXutil.strcmp(Z6PartieName, BC000112_A6PartieName[0]) != 0 ) || ( GXutil.strcmp(Z7PartieShortName, BC000112_A7PartieShortName[0]) != 0 ) || ( GXutil.strcmp(Z8PartieRoleCode, BC000112_A8PartieRoleCode[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_waschg", new Object[] {"AEOMasterData"}), "RecordWasChanged", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert013( )
   {
      beforeValidate013( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable013( ) ;
      }
      if ( AnyError == 0 )
      {
         zm013( 0) ;
         checkOptimisticConcurrency013( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm013( ) ;
            if ( AnyError == 0 )
            {
               beforeInsert013( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor BC000113 */
                  pr_default.execute(11, new Object[] {A5PartieID, A6PartieName, A7PartieShortName, A8PartieRoleCode, A1IdentificationIssuingCountryCo});
                  Application.getSmartCacheProvider(remoteHandle).setUpdated("AEOMasterData");
                  if ( (pr_default.getStatus(11) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "");
                     AnyError = (short)(1) ;
                  }
                  if ( AnyError == 0 )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( AnyError == 0 )
                     {
                        /* Save values for previous() function. */
                        endTrnMsgTxt = localUtil.getMessages().getMessage("GXM_sucadded") ;
                        endTrnMsgCod = "SuccessfullyAdded" ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load013( ) ;
         }
         endLevel013( ) ;
      }
      closeExtendedTableCursors013( ) ;
   }

   public void update013( )
   {
      beforeValidate013( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable013( ) ;
      }
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency013( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm013( ) ;
            if ( AnyError == 0 )
            {
               beforeUpdate013( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor BC000114 */
                  pr_default.execute(12, new Object[] {A6PartieName, A7PartieShortName, A8PartieRoleCode, A1IdentificationIssuingCountryCo, A5PartieID});
                  Application.getSmartCacheProvider(remoteHandle).setUpdated("AEOMasterData");
                  if ( (pr_default.getStatus(12) == 103) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"AEOMasterData"}), "RecordIsLocked", 1, "");
                     AnyError = (short)(1) ;
                  }
                  deferredUpdate013( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( AnyError == 0 )
                     {
                        getByPrimaryKey( ) ;
                        endTrnMsgTxt = localUtil.getMessages().getMessage("GXM_sucupdated") ;
                        endTrnMsgCod = "SuccessfullyUpdated" ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel013( ) ;
      }
      closeExtendedTableCursors013( ) ;
   }

   public void deferredUpdate013( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate013( ) ;
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency013( ) ;
      }
      if ( AnyError == 0 )
      {
         onDeleteControls013( ) ;
         afterConfirm013( ) ;
         if ( AnyError == 0 )
         {
            beforeDelete013( ) ;
            if ( AnyError == 0 )
            {
               /* No cascading delete specified. */
               /* Using cursor BC000115 */
               pr_default.execute(13, new Object[] {A1IdentificationIssuingCountryCo, A5PartieID});
               Application.getSmartCacheProvider(remoteHandle).setUpdated("AEOMasterData");
               if ( AnyError == 0 )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( AnyError == 0 )
                  {
                     endTrnMsgTxt = localUtil.getMessages().getMessage("GXM_sucdeleted") ;
                     endTrnMsgCod = "SuccessfullyDeleted" ;
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode3 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel013( ) ;
      Gx_mode = sMode3 ;
   }

   public void onDeleteControls013( )
   {
      standaloneModal( ) ;
      if ( AnyError == 0 )
      {
         /* Delete mode formulas */
         /* Using cursor BC000116 */
         pr_default.execute(14, new Object[] {A1IdentificationIssuingCountryCo});
         A4IdentificationIssuingCountryNa = BC000116_A4IdentificationIssuingCountryNa[0] ;
         pr_default.close(14);
      }
   }

   public void endLevel013( )
   {
      if ( ! isIns( ) )
      {
         pr_default.close(10);
      }
      if ( AnyError == 0 )
      {
         beforeComplete013( ) ;
      }
      if ( AnyError == 0 )
      {
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( AnyError != 0 )
      {
         httpContext.wjLoc = "" ;
         httpContext.nUserReturn = (byte)(0) ;
      }
   }

   public void scanKeyStart013( )
   {
      /* Scan By routine */
      /* Using cursor BC000117 */
      pr_default.execute(15, new Object[] {A1IdentificationIssuingCountryCo, A5PartieID});
      RcdFound3 = (short)(0) ;
      if ( (pr_default.getStatus(15) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A5PartieID = BC000117_A5PartieID[0] ;
         A4IdentificationIssuingCountryNa = BC000117_A4IdentificationIssuingCountryNa[0] ;
         A6PartieName = BC000117_A6PartieName[0] ;
         A7PartieShortName = BC000117_A7PartieShortName[0] ;
         A8PartieRoleCode = BC000117_A8PartieRoleCode[0] ;
         A1IdentificationIssuingCountryCo = BC000117_A1IdentificationIssuingCountryCo[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanKeyNext013( )
   {
      /* Scan next routine */
      pr_default.readNext(15);
      RcdFound3 = (short)(0) ;
      scanKeyLoad013( ) ;
   }

   public void scanKeyLoad013( )
   {
      sMode3 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(15) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A5PartieID = BC000117_A5PartieID[0] ;
         A4IdentificationIssuingCountryNa = BC000117_A4IdentificationIssuingCountryNa[0] ;
         A6PartieName = BC000117_A6PartieName[0] ;
         A7PartieShortName = BC000117_A7PartieShortName[0] ;
         A8PartieRoleCode = BC000117_A8PartieRoleCode[0] ;
         A1IdentificationIssuingCountryCo = BC000117_A1IdentificationIssuingCountryCo[0] ;
      }
      Gx_mode = sMode3 ;
   }

   public void scanKeyEnd013( )
   {
      pr_default.close(15);
   }

   public void afterConfirm013( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert013( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate013( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete013( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete013( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate013( )
   {
      /* Before Validate Rules */
   }

   public void disableAttributes013( )
   {
   }

   public void send_integrity_lvl_hashes013( )
   {
   }

   public void addRow013( )
   {
      VarsToRow3( bcAEOMasterData) ;
   }

   public void readRow013( )
   {
      RowToVars3( bcAEOMasterData, 1) ;
   }

   public void initializeNonKey013( )
   {
      A4IdentificationIssuingCountryNa = "" ;
      A6PartieName = "" ;
      A7PartieShortName = "" ;
      A8PartieRoleCode = "" ;
      Z6PartieName = "" ;
      Z7PartieShortName = "" ;
      Z8PartieRoleCode = "" ;
   }

   public void initAll013( )
   {
      A1IdentificationIssuingCountryCo = "" ;
      A5PartieID = "" ;
      initializeNonKey013( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public boolean isIns( )
   {
      return ((GXutil.strcmp(Gx_mode, "INS")==0) ? true : false) ;
   }

   public boolean isDlt( )
   {
      return ((GXutil.strcmp(Gx_mode, "DLT")==0) ? true : false) ;
   }

   public boolean isUpd( )
   {
      return ((GXutil.strcmp(Gx_mode, "UPD")==0) ? true : false) ;
   }

   public boolean isDsp( )
   {
      return ((GXutil.strcmp(Gx_mode, "DSP")==0) ? true : false) ;
   }

   public void VarsToRow3( com.oea5pruebadeconcepto.SdtAEOMasterData obj3 )
   {
      obj3.setgxTv_SdtAEOMasterData_Mode( Gx_mode );
      obj3.setgxTv_SdtAEOMasterData_Identificationissuingcountryname( A4IdentificationIssuingCountryNa );
      obj3.setgxTv_SdtAEOMasterData_Partiename( A6PartieName );
      obj3.setgxTv_SdtAEOMasterData_Partieshortname( A7PartieShortName );
      obj3.setgxTv_SdtAEOMasterData_Partierolecode( A8PartieRoleCode );
      obj3.setgxTv_SdtAEOMasterData_Identificationissuingcountrycode( A1IdentificationIssuingCountryCo );
      obj3.setgxTv_SdtAEOMasterData_Partieid( A5PartieID );
      obj3.setgxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z( Z1IdentificationIssuingCountryCo );
      obj3.setgxTv_SdtAEOMasterData_Identificationissuingcountryname_Z( Z4IdentificationIssuingCountryNa );
      obj3.setgxTv_SdtAEOMasterData_Partieid_Z( Z5PartieID );
      obj3.setgxTv_SdtAEOMasterData_Partiename_Z( Z6PartieName );
      obj3.setgxTv_SdtAEOMasterData_Partieshortname_Z( Z7PartieShortName );
      obj3.setgxTv_SdtAEOMasterData_Partierolecode_Z( Z8PartieRoleCode );
      obj3.setgxTv_SdtAEOMasterData_Mode( Gx_mode );
   }

   public void KeyVarsToRow3( com.oea5pruebadeconcepto.SdtAEOMasterData obj3 )
   {
      obj3.setgxTv_SdtAEOMasterData_Identificationissuingcountrycode( A1IdentificationIssuingCountryCo );
      obj3.setgxTv_SdtAEOMasterData_Partieid( A5PartieID );
   }

   public void RowToVars3( com.oea5pruebadeconcepto.SdtAEOMasterData obj3 ,
                           int forceLoad )
   {
      Gx_mode = obj3.getgxTv_SdtAEOMasterData_Mode() ;
      A4IdentificationIssuingCountryNa = obj3.getgxTv_SdtAEOMasterData_Identificationissuingcountryname() ;
      A6PartieName = obj3.getgxTv_SdtAEOMasterData_Partiename() ;
      A7PartieShortName = obj3.getgxTv_SdtAEOMasterData_Partieshortname() ;
      A8PartieRoleCode = obj3.getgxTv_SdtAEOMasterData_Partierolecode() ;
      A1IdentificationIssuingCountryCo = obj3.getgxTv_SdtAEOMasterData_Identificationissuingcountrycode() ;
      A5PartieID = obj3.getgxTv_SdtAEOMasterData_Partieid() ;
      Z1IdentificationIssuingCountryCo = obj3.getgxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z() ;
      Z4IdentificationIssuingCountryNa = obj3.getgxTv_SdtAEOMasterData_Identificationissuingcountryname_Z() ;
      Z5PartieID = obj3.getgxTv_SdtAEOMasterData_Partieid_Z() ;
      Z6PartieName = obj3.getgxTv_SdtAEOMasterData_Partiename_Z() ;
      Z7PartieShortName = obj3.getgxTv_SdtAEOMasterData_Partieshortname_Z() ;
      Z8PartieRoleCode = obj3.getgxTv_SdtAEOMasterData_Partierolecode_Z() ;
      Gx_mode = obj3.getgxTv_SdtAEOMasterData_Mode() ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A1IdentificationIssuingCountryCo = (String)getParm(obj,0) ;
      A5PartieID = (String)getParm(obj,1) ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey013( ) ;
      scanKeyStart013( ) ;
      if ( RcdFound3 == 0 )
      {
         Gx_mode = "INS" ;
         /* Using cursor BC000118 */
         pr_default.execute(16, new Object[] {A1IdentificationIssuingCountryCo});
         if ( (pr_default.getStatus(16) == 101) )
         {
            httpContext.GX_msglist.addItem("No existe 'AEOMaster Data Country'.", "ForeignKeyNotFound", 1, "IDENTIFICATIONISSUINGCOUNTRYCO");
            AnyError = (short)(1) ;
         }
         A4IdentificationIssuingCountryNa = BC000118_A4IdentificationIssuingCountryNa[0] ;
         pr_default.close(16);
      }
      else
      {
         Gx_mode = "UPD" ;
         Z1IdentificationIssuingCountryCo = A1IdentificationIssuingCountryCo ;
         Z5PartieID = A5PartieID ;
      }
      zm013( -1) ;
      onLoadActions013( ) ;
      addRow013( ) ;
      scanKeyEnd013( ) ;
      if ( RcdFound3 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_keynfound"), "PrimaryKeyNotFound", 1, "");
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Load( )
   {
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      RowToVars3( bcAEOMasterData, 0) ;
      scanKeyStart013( ) ;
      if ( RcdFound3 == 0 )
      {
         Gx_mode = "INS" ;
         /* Using cursor BC000119 */
         pr_default.execute(17, new Object[] {A1IdentificationIssuingCountryCo});
         if ( (pr_default.getStatus(17) == 101) )
         {
            httpContext.GX_msglist.addItem("No existe 'AEOMaster Data Country'.", "ForeignKeyNotFound", 1, "IDENTIFICATIONISSUINGCOUNTRYCO");
            AnyError = (short)(1) ;
         }
         A4IdentificationIssuingCountryNa = BC000119_A4IdentificationIssuingCountryNa[0] ;
         pr_default.close(17);
      }
      else
      {
         Gx_mode = "UPD" ;
         Z1IdentificationIssuingCountryCo = A1IdentificationIssuingCountryCo ;
         Z5PartieID = A5PartieID ;
      }
      zm013( -1) ;
      onLoadActions013( ) ;
      addRow013( ) ;
      scanKeyEnd013( ) ;
      if ( RcdFound3 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_keynfound"), "PrimaryKeyNotFound", 1, "");
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void saveImpl( )
   {
      nKeyPressed = (byte)(1) ;
      getKey013( ) ;
      if ( isIns( ) )
      {
         /* Insert record */
         insert013( ) ;
      }
      else
      {
         if ( RcdFound3 == 1 )
         {
            if ( ( GXutil.strcmp(A1IdentificationIssuingCountryCo, Z1IdentificationIssuingCountryCo) != 0 ) || ( GXutil.strcmp(A5PartieID, Z5PartieID) != 0 ) )
            {
               A1IdentificationIssuingCountryCo = Z1IdentificationIssuingCountryCo ;
               A5PartieID = Z5PartieID ;
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "CandidateKeyNotFound", 1, "");
               AnyError = (short)(1) ;
            }
            else if ( isDlt( ) )
            {
               delete( ) ;
               afterTrn( ) ;
            }
            else
            {
               Gx_mode = "UPD" ;
               /* Update record */
               update013( ) ;
            }
         }
         else
         {
            if ( isDlt( ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "CandidateKeyNotFound", 1, "");
               AnyError = (short)(1) ;
            }
            else
            {
               if ( ( GXutil.strcmp(A1IdentificationIssuingCountryCo, Z1IdentificationIssuingCountryCo) != 0 ) || ( GXutil.strcmp(A5PartieID, Z5PartieID) != 0 ) )
               {
                  if ( isUpd( ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "DuplicatePrimaryKey", 1, "");
                     AnyError = (short)(1) ;
                  }
                  else
                  {
                     Gx_mode = "INS" ;
                     /* Insert record */
                     insert013( ) ;
                  }
               }
               else
               {
                  if ( GXutil.strcmp(Gx_mode, "UPD") == 0 )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_recdeleted"), 1, "");
                     AnyError = (short)(1) ;
                  }
                  else
                  {
                     Gx_mode = "INS" ;
                     /* Insert record */
                     insert013( ) ;
                  }
               }
            }
         }
      }
      afterTrn( ) ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars3( bcAEOMasterData, 1) ;
      saveImpl( ) ;
      VarsToRow3( bcAEOMasterData) ;
      httpContext.GX_msglist = BackMsgLst ;
   }

   public boolean Insert( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars3( bcAEOMasterData, 1) ;
      Gx_mode = "INS" ;
      /* Insert record */
      insert013( ) ;
      afterTrn( ) ;
      VarsToRow3( bcAEOMasterData) ;
      httpContext.GX_msglist = BackMsgLst ;
      return (AnyError==0) ;
   }

   public void updateImpl( )
   {
      if ( isUpd( ) )
      {
         saveImpl( ) ;
      }
      else
      {
         com.oea5pruebadeconcepto.SdtAEOMasterData auxBC = new com.oea5pruebadeconcepto.SdtAEOMasterData( remoteHandle, context);
         IGxSilentTrn auxTrn = auxBC.getTransaction();
         auxBC.Load(A1IdentificationIssuingCountryCo, A5PartieID);
         if ( auxTrn.Errors() == 0 )
         {
            auxBC.updateDirties(bcAEOMasterData);
            auxBC.Save();
         }
         LclMsgLst = auxTrn.GetMessages() ;
         AnyError = (short)(auxTrn.Errors()) ;
         httpContext.GX_msglist = LclMsgLst ;
         if ( auxTrn.Errors() == 0 )
         {
            Gx_mode = auxTrn.GetMode() ;
            afterTrn( ) ;
         }
      }
   }

   public boolean Update( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars3( bcAEOMasterData, 1) ;
      updateImpl( ) ;
      VarsToRow3( bcAEOMasterData) ;
      httpContext.GX_msglist = BackMsgLst ;
      return (AnyError==0) ;
   }

   public boolean InsertOrUpdate( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars3( bcAEOMasterData, 1) ;
      Gx_mode = "INS" ;
      /* Insert record */
      insert013( ) ;
      if ( AnyError == 1 )
      {
         if ( GXutil.strcmp(httpContext.GX_msglist.getItemValue((short)(1)), "DuplicatePrimaryKey") == 0 )
         {
            AnyError = (short)(0) ;
            httpContext.GX_msglist.removeAllItems();
            updateImpl( ) ;
         }
      }
      else
      {
         afterTrn( ) ;
      }
      VarsToRow3( bcAEOMasterData) ;
      httpContext.GX_msglist = BackMsgLst ;
      return (AnyError==0) ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars3( bcAEOMasterData, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey013( ) ;
      if ( RcdFound3 == 1 )
      {
         if ( isIns( ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "");
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A1IdentificationIssuingCountryCo, Z1IdentificationIssuingCountryCo) != 0 ) || ( GXutil.strcmp(A5PartieID, Z5PartieID) != 0 ) )
         {
            A1IdentificationIssuingCountryCo = Z1IdentificationIssuingCountryCo ;
            A5PartieID = Z5PartieID ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "DuplicatePrimaryKey", 1, "");
            AnyError = (short)(1) ;
         }
         else if ( isDlt( ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A1IdentificationIssuingCountryCo, Z1IdentificationIssuingCountryCo) != 0 ) || ( GXutil.strcmp(A5PartieID, Z5PartieID) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( isUpd( ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_recdeleted"), 1, "");
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      Application.rollbackDataStores(context, remoteHandle, pr_default, "aeomasterdata_bc");
      VarsToRow3( bcAEOMasterData) ;
      httpContext.GX_msglist = BackMsgLst ;
   }

   public int Errors( )
   {
      if ( AnyError == 0 )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcAEOMasterData.getgxTv_SdtAEOMasterData_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcAEOMasterData.setgxTv_SdtAEOMasterData_Mode( Gx_mode );
   }

   public void SetSDT( com.oea5pruebadeconcepto.SdtAEOMasterData sdt ,
                       byte sdtToBc )
   {
      if ( sdt != bcAEOMasterData )
      {
         bcAEOMasterData = sdt ;
         if ( GXutil.strcmp(bcAEOMasterData.getgxTv_SdtAEOMasterData_Mode(), "") == 0 )
         {
            bcAEOMasterData.setgxTv_SdtAEOMasterData_Mode( "INS" );
         }
         if ( sdtToBc == 1 )
         {
            VarsToRow3( bcAEOMasterData) ;
         }
         else
         {
            RowToVars3( bcAEOMasterData, 1) ;
         }
      }
      else
      {
         if ( GXutil.strcmp(bcAEOMasterData.getgxTv_SdtAEOMasterData_Mode(), "") == 0 )
         {
            bcAEOMasterData.setgxTv_SdtAEOMasterData_Mode( "INS" );
         }
      }
   }

   public void ReloadFromSDT( )
   {
      RowToVars3( bcAEOMasterData, 1) ;
   }

   public void ForceCommitOnExit( )
   {
      mustCommit = true ;
   }

   public SdtAEOMasterData getAEOMasterData_BC( )
   {
      return bcAEOMasterData ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      BC00016_A5PartieID = new String[] {""} ;
      BC00016_A4IdentificationIssuingCountryNa = new String[] {""} ;
      BC00016_A6PartieName = new String[] {""} ;
      BC00016_A7PartieShortName = new String[] {""} ;
      BC00016_A8PartieRoleCode = new String[] {""} ;
      BC00016_A1IdentificationIssuingCountryCo = new String[] {""} ;
      A5PartieID = "" ;
      A4IdentificationIssuingCountryNa = "" ;
      A6PartieName = "" ;
      A7PartieShortName = "" ;
      A8PartieRoleCode = "" ;
      A1IdentificationIssuingCountryCo = "" ;
      gx_restcollection = new GXBCCollection<com.oea5pruebadeconcepto.SdtAEOMasterData>(com.oea5pruebadeconcepto.SdtAEOMasterData.class, "AEOMasterData", "OEA5pruebaDeConcepto", remoteHandle);
      sMode3 = "" ;
      Gx_mode = "" ;
      endTrnMsgTxt = "" ;
      endTrnMsgCod = "" ;
      Z1IdentificationIssuingCountryCo = "" ;
      Z5PartieID = "" ;
      Z6PartieName = "" ;
      Z7PartieShortName = "" ;
      Z8PartieRoleCode = "" ;
      Z4IdentificationIssuingCountryNa = "" ;
      BC00017_A5PartieID = new String[] {""} ;
      BC00017_A4IdentificationIssuingCountryNa = new String[] {""} ;
      BC00017_A6PartieName = new String[] {""} ;
      BC00017_A7PartieShortName = new String[] {""} ;
      BC00017_A8PartieRoleCode = new String[] {""} ;
      BC00017_A1IdentificationIssuingCountryCo = new String[] {""} ;
      BC00018_A4IdentificationIssuingCountryNa = new String[] {""} ;
      BC00019_A8PartieRoleCode = new String[] {""} ;
      BC000110_A1IdentificationIssuingCountryCo = new String[] {""} ;
      BC000110_A5PartieID = new String[] {""} ;
      BC000111_A5PartieID = new String[] {""} ;
      BC000111_A6PartieName = new String[] {""} ;
      BC000111_A7PartieShortName = new String[] {""} ;
      BC000111_A8PartieRoleCode = new String[] {""} ;
      BC000111_A1IdentificationIssuingCountryCo = new String[] {""} ;
      BC000112_A5PartieID = new String[] {""} ;
      BC000112_A6PartieName = new String[] {""} ;
      BC000112_A7PartieShortName = new String[] {""} ;
      BC000112_A8PartieRoleCode = new String[] {""} ;
      BC000112_A1IdentificationIssuingCountryCo = new String[] {""} ;
      BC000116_A4IdentificationIssuingCountryNa = new String[] {""} ;
      BC000117_A5PartieID = new String[] {""} ;
      BC000117_A4IdentificationIssuingCountryNa = new String[] {""} ;
      BC000117_A6PartieName = new String[] {""} ;
      BC000117_A7PartieShortName = new String[] {""} ;
      BC000117_A8PartieRoleCode = new String[] {""} ;
      BC000117_A1IdentificationIssuingCountryCo = new String[] {""} ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      BC000118_A4IdentificationIssuingCountryNa = new String[] {""} ;
      BC000119_A4IdentificationIssuingCountryNa = new String[] {""} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new com.oea5pruebadeconcepto.aeomasterdata_bc__default(),
         new Object[] {
             new Object[] {
            BC00012_A5PartieID, BC00012_A6PartieName, BC00012_A7PartieShortName, BC00012_A8PartieRoleCode, BC00012_A1IdentificationIssuingCountryCo
            }
            , new Object[] {
            BC00013_A5PartieID, BC00013_A6PartieName, BC00013_A7PartieShortName, BC00013_A8PartieRoleCode, BC00013_A1IdentificationIssuingCountryCo
            }
            , new Object[] {
            BC00014_A8PartieRoleCode
            }
            , new Object[] {
            BC00015_A4IdentificationIssuingCountryNa
            }
            , new Object[] {
            BC00016_A5PartieID, BC00016_A4IdentificationIssuingCountryNa, BC00016_A6PartieName, BC00016_A7PartieShortName, BC00016_A8PartieRoleCode, BC00016_A1IdentificationIssuingCountryCo
            }
            , new Object[] {
            BC00017_A5PartieID, BC00017_A4IdentificationIssuingCountryNa, BC00017_A6PartieName, BC00017_A7PartieShortName, BC00017_A8PartieRoleCode, BC00017_A1IdentificationIssuingCountryCo
            }
            , new Object[] {
            BC00018_A4IdentificationIssuingCountryNa
            }
            , new Object[] {
            BC00019_A8PartieRoleCode
            }
            , new Object[] {
            BC000110_A1IdentificationIssuingCountryCo, BC000110_A5PartieID
            }
            , new Object[] {
            BC000111_A5PartieID, BC000111_A6PartieName, BC000111_A7PartieShortName, BC000111_A8PartieRoleCode, BC000111_A1IdentificationIssuingCountryCo
            }
            , new Object[] {
            BC000112_A5PartieID, BC000112_A6PartieName, BC000112_A7PartieShortName, BC000112_A8PartieRoleCode, BC000112_A1IdentificationIssuingCountryCo
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC000116_A4IdentificationIssuingCountryNa
            }
            , new Object[] {
            BC000117_A5PartieID, BC000117_A4IdentificationIssuingCountryNa, BC000117_A6PartieName, BC000117_A7PartieShortName, BC000117_A8PartieRoleCode, BC000117_A1IdentificationIssuingCountryCo
            }
            , new Object[] {
            BC000118_A4IdentificationIssuingCountryNa
            }
            , new Object[] {
            BC000119_A4IdentificationIssuingCountryNa
            }
         }
      );
      /* Execute Start event if defined. */
      /* Execute user event: Start */
      e12012 ();
      standaloneNotModal( ) ;
   }

   private byte nKeyPressed ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short RcdFound3 ;
   private short nIsDirty_3 ;
   private int trnEnded ;
   private int Start ;
   private int Count ;
   private int GXPagingFrom3 ;
   private int GXPagingTo3 ;
   private int GX_JID ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String A5PartieID ;
   private String A8PartieRoleCode ;
   private String A1IdentificationIssuingCountryCo ;
   private String sMode3 ;
   private String Gx_mode ;
   private String endTrnMsgTxt ;
   private String endTrnMsgCod ;
   private String Z1IdentificationIssuingCountryCo ;
   private String Z5PartieID ;
   private String Z8PartieRoleCode ;
   private boolean returnInSub ;
   private boolean mustCommit ;
   private String A4IdentificationIssuingCountryNa ;
   private String A6PartieName ;
   private String A7PartieShortName ;
   private String Z6PartieName ;
   private String Z7PartieShortName ;
   private String Z4IdentificationIssuingCountryNa ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private GXBCCollection<com.oea5pruebadeconcepto.SdtAEOMasterData> gx_restcollection ;
   private com.oea5pruebadeconcepto.SdtAEOMasterData bcAEOMasterData ;
   private com.oea5pruebadeconcepto.SdtAEOMasterData gx_sdt_item ;
   private IDataStoreProvider pr_default ;
   private String[] BC00016_A5PartieID ;
   private String[] BC00016_A4IdentificationIssuingCountryNa ;
   private String[] BC00016_A6PartieName ;
   private String[] BC00016_A7PartieShortName ;
   private String[] BC00016_A8PartieRoleCode ;
   private String[] BC00016_A1IdentificationIssuingCountryCo ;
   private String[] BC00017_A5PartieID ;
   private String[] BC00017_A4IdentificationIssuingCountryNa ;
   private String[] BC00017_A6PartieName ;
   private String[] BC00017_A7PartieShortName ;
   private String[] BC00017_A8PartieRoleCode ;
   private String[] BC00017_A1IdentificationIssuingCountryCo ;
   private String[] BC00018_A4IdentificationIssuingCountryNa ;
   private String[] BC00019_A8PartieRoleCode ;
   private String[] BC000110_A1IdentificationIssuingCountryCo ;
   private String[] BC000110_A5PartieID ;
   private String[] BC000111_A5PartieID ;
   private String[] BC000111_A6PartieName ;
   private String[] BC000111_A7PartieShortName ;
   private String[] BC000111_A8PartieRoleCode ;
   private String[] BC000111_A1IdentificationIssuingCountryCo ;
   private String[] BC000112_A5PartieID ;
   private String[] BC000112_A6PartieName ;
   private String[] BC000112_A7PartieShortName ;
   private String[] BC000112_A8PartieRoleCode ;
   private String[] BC000112_A1IdentificationIssuingCountryCo ;
   private String[] BC000116_A4IdentificationIssuingCountryNa ;
   private String[] BC000117_A5PartieID ;
   private String[] BC000117_A4IdentificationIssuingCountryNa ;
   private String[] BC000117_A6PartieName ;
   private String[] BC000117_A7PartieShortName ;
   private String[] BC000117_A8PartieRoleCode ;
   private String[] BC000117_A1IdentificationIssuingCountryCo ;
   private String[] BC000118_A4IdentificationIssuingCountryNa ;
   private String[] BC000119_A4IdentificationIssuingCountryNa ;
   private String[] BC00012_A5PartieID ;
   private String[] BC00012_A6PartieName ;
   private String[] BC00012_A7PartieShortName ;
   private String[] BC00012_A8PartieRoleCode ;
   private String[] BC00012_A1IdentificationIssuingCountryCo ;
   private String[] BC00013_A5PartieID ;
   private String[] BC00013_A6PartieName ;
   private String[] BC00013_A7PartieShortName ;
   private String[] BC00013_A8PartieRoleCode ;
   private String[] BC00013_A1IdentificationIssuingCountryCo ;
   private String[] BC00014_A8PartieRoleCode ;
   private String[] BC00015_A4IdentificationIssuingCountryNa ;
}

final  class aeomasterdata_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC00012", "SELECT `PartieID`, `PartieName`, `PartieShortName`, `PartieRoleCode`, `IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo FROM `AEOMasterData` WHERE `IdentificationIssuingCountryCo` = ? AND `PartieID` = ?  FOR UPDATE ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC00013", "SELECT `PartieID`, `PartieName`, `PartieShortName`, `PartieRoleCode`, `IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo FROM `AEOMasterData` WHERE `IdentificationIssuingCountryCo` = ? AND `PartieID` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC00014", "SELECT `PartieRoleCode` FROM `PartieRole` WHERE `PartieRoleCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC00015", "SELECT `CountryName` AS IdentificationIssuingCountryNa FROM `Country` WHERE `CountryCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC00016", "SELECT TM1.`PartieID`, T2.`CountryName` AS IdentificationIssuingCountryNa, TM1.`PartieName`, TM1.`PartieShortName`, TM1.`PartieRoleCode`, TM1.`IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo FROM (`AEOMasterData` TM1 INNER JOIN `Country` T2 ON T2.`CountryCode` = TM1.`IdentificationIssuingCountryCo`) ORDER BY TM1.`IdentificationIssuingCountryCo`, TM1.`PartieID`  LIMIT ?, ?",true, GX_NOMASK, false, this,100, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC00017", "SELECT TM1.`PartieID`, T2.`CountryName` AS IdentificationIssuingCountryNa, TM1.`PartieName`, TM1.`PartieShortName`, TM1.`PartieRoleCode`, TM1.`IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo FROM (`AEOMasterData` TM1 INNER JOIN `Country` T2 ON T2.`CountryCode` = TM1.`IdentificationIssuingCountryCo`) WHERE TM1.`IdentificationIssuingCountryCo` = ? and TM1.`PartieID` = ? ORDER BY TM1.`IdentificationIssuingCountryCo`, TM1.`PartieID` ",true, GX_NOMASK, false, this,100, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC00018", "SELECT `CountryName` AS IdentificationIssuingCountryNa FROM `Country` WHERE `CountryCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC00019", "SELECT `PartieRoleCode` FROM `PartieRole` WHERE `PartieRoleCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC000110", "SELECT `IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo, `PartieID` FROM `AEOMasterData` WHERE `IdentificationIssuingCountryCo` = ? AND `PartieID` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC000111", "SELECT `PartieID`, `PartieName`, `PartieShortName`, `PartieRoleCode`, `IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo FROM `AEOMasterData` WHERE `IdentificationIssuingCountryCo` = ? AND `PartieID` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC000112", "SELECT `PartieID`, `PartieName`, `PartieShortName`, `PartieRoleCode`, `IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo FROM `AEOMasterData` WHERE `IdentificationIssuingCountryCo` = ? AND `PartieID` = ?  FOR UPDATE ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new UpdateCursor("BC000113", "INSERT INTO `AEOMasterData`(`PartieID`, `PartieName`, `PartieShortName`, `PartieRoleCode`, `IdentificationIssuingCountryCo`) VALUES(?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC000114", "UPDATE `AEOMasterData` SET `PartieName`=?, `PartieShortName`=?, `PartieRoleCode`=?  WHERE `IdentificationIssuingCountryCo` = ? AND `PartieID` = ?", GX_NOMASK)
         ,new UpdateCursor("BC000115", "DELETE FROM `AEOMasterData`  WHERE `IdentificationIssuingCountryCo` = ? AND `PartieID` = ?", GX_NOMASK)
         ,new ForEachCursor("BC000116", "SELECT `CountryName` AS IdentificationIssuingCountryNa FROM `Country` WHERE `CountryCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC000117", "SELECT TM1.`PartieID`, T2.`CountryName` AS IdentificationIssuingCountryNa, TM1.`PartieName`, TM1.`PartieShortName`, TM1.`PartieRoleCode`, TM1.`IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo FROM (`AEOMasterData` TM1 INNER JOIN `Country` T2 ON T2.`CountryCode` = TM1.`IdentificationIssuingCountryCo`) WHERE TM1.`IdentificationIssuingCountryCo` = ? and TM1.`PartieID` = ? ORDER BY TM1.`IdentificationIssuingCountryCo`, TM1.`PartieID` ",true, GX_NOMASK, false, this,100, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC000118", "SELECT `CountryName` AS IdentificationIssuingCountryNa FROM `Country` WHERE `CountryCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC000119", "SELECT `CountryName` AS IdentificationIssuingCountryNa FROM `Country` WHERE `CountryCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 30);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               ((String[]) buf[2])[0] = rslt.getVarchar(3);
               ((String[]) buf[3])[0] = rslt.getString(4, 2);
               ((String[]) buf[4])[0] = rslt.getString(5, 2);
               return;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 30);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               ((String[]) buf[2])[0] = rslt.getVarchar(3);
               ((String[]) buf[3])[0] = rslt.getString(4, 2);
               ((String[]) buf[4])[0] = rslt.getString(5, 2);
               return;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               return;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1);
               return;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getString(1, 30);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               ((String[]) buf[2])[0] = rslt.getVarchar(3);
               ((String[]) buf[3])[0] = rslt.getVarchar(4);
               ((String[]) buf[4])[0] = rslt.getString(5, 2);
               ((String[]) buf[5])[0] = rslt.getString(6, 2);
               return;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getString(1, 30);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               ((String[]) buf[2])[0] = rslt.getVarchar(3);
               ((String[]) buf[3])[0] = rslt.getVarchar(4);
               ((String[]) buf[4])[0] = rslt.getString(5, 2);
               ((String[]) buf[5])[0] = rslt.getString(6, 2);
               return;
            case 6 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1);
               return;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               return;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getString(2, 30);
               return;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getString(1, 30);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               ((String[]) buf[2])[0] = rslt.getVarchar(3);
               ((String[]) buf[3])[0] = rslt.getString(4, 2);
               ((String[]) buf[4])[0] = rslt.getString(5, 2);
               return;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getString(1, 30);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               ((String[]) buf[2])[0] = rslt.getVarchar(3);
               ((String[]) buf[3])[0] = rslt.getString(4, 2);
               ((String[]) buf[4])[0] = rslt.getString(5, 2);
               return;
            case 14 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1);
               return;
            case 15 :
               ((String[]) buf[0])[0] = rslt.getString(1, 30);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               ((String[]) buf[2])[0] = rslt.getVarchar(3);
               ((String[]) buf[3])[0] = rslt.getVarchar(4);
               ((String[]) buf[4])[0] = rslt.getString(5, 2);
               ((String[]) buf[5])[0] = rslt.getString(6, 2);
               return;
            case 16 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1);
               return;
            case 17 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1);
               return;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               return;
            case 1 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               return;
            case 2 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 3 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 4 :
               stmt.setInt(1, ((Number) parms[0]).intValue());
               stmt.setInt(2, ((Number) parms[1]).intValue());
               return;
            case 5 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               return;
            case 6 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 7 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 8 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               return;
            case 9 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               return;
            case 10 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               return;
            case 11 :
               stmt.setString(1, (String)parms[0], 30);
               stmt.setVarchar(2, (String)parms[1], 50, false);
               stmt.setVarchar(3, (String)parms[2], 50, false);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 2);
               return;
            case 12 :
               stmt.setVarchar(1, (String)parms[0], 50, false);
               stmt.setVarchar(2, (String)parms[1], 50, false);
               stmt.setString(3, (String)parms[2], 2);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 30);
               return;
            case 13 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               return;
            case 14 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 15 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               return;
            case 16 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 17 :
               stmt.setString(1, (String)parms[0], 2);
               return;
      }
   }

}

