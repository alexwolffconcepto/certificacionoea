package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(urlPatterns = {"/servlet/com.oea5pruebadeconcepto.partieroleaeomasterdatawc", "/com.oea5pruebadeconcepto.partieroleaeomasterdatawc"})
public final  class partieroleaeomasterdatawc extends GXWebObjectStub
{
   public partieroleaeomasterdatawc( )
   {
   }

   public partieroleaeomasterdatawc( int remoteHandle )
   {
      super(remoteHandle, new ModelContext( partieroleaeomasterdatawc.class ));
   }

   public partieroleaeomasterdatawc( int remoteHandle ,
                                     ModelContext context )
   {
      super(remoteHandle, context);
   }

   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new partieroleaeomasterdatawc_impl(context).doExecute();
   }

   protected void init( com.genexus.internet.HttpContext context )
   {
      new partieroleaeomasterdatawc_impl(context).cleanup();
   }

   public String getServletInfo( )
   {
      return "Partie Role AEOMaster Data WC";
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

   protected String IntegratedSecurityPermissionPrefix( )
   {
      return "";
   }

   protected String EncryptURLParameters( )
   {
      return "NO";
   }

}

