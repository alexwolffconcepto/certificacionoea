package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.fasterxml.jackson.annotation.*;
import java.util.*;

@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.NONE)
@javax.xml.bind.annotation.XmlType(name = "listprograms_RESTInterfaceOUT", namespace ="http://tempuri.org/")
@JsonPropertyOrder(alphabetic=true)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.NONE, getterVisibility=JsonAutoDetect.Visibility.NONE, isGetterVisibility=JsonAutoDetect.Visibility.NONE)
public final  class listprograms_RESTInterfaceOUT
{
   Vector<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName_RESTInterface> AV9ProgramNames ;
   @JsonProperty("ProgramNames")
   @JsonInclude(JsonInclude.Include.NON_EMPTY)
   public Vector<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName_RESTInterface> getProgramNames( )
   {
      return AV9ProgramNames ;
   }

   @JsonProperty("ProgramNames")
   public void setProgramNames(  Vector<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName_RESTInterface> Value )
   {
      AV9ProgramNames= Value;
   }


}

