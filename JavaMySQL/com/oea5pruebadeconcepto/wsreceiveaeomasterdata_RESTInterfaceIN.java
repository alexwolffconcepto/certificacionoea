package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.fasterxml.jackson.annotation.*;
import java.util.*;

@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.NONE)
@javax.xml.bind.annotation.XmlType(name = "wsreceiveaeomasterdata_RESTInterfaceIN", namespace ="http://tempuri.org/")
@JsonPropertyOrder(alphabetic=true)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.NONE, getterVisibility=JsonAutoDetect.Visibility.NONE, isGetterVisibility=JsonAutoDetect.Visibility.NONE)
public final  class wsreceiveaeomasterdata_RESTInterfaceIN
{
   String AV8IdentificationIssuingCountryCode;
   @JsonProperty("IdentificationIssuingCountryCode")
   public String getIdentificationIssuingCountryCode( )
   {
      if ( GXutil.strcmp(AV8IdentificationIssuingCountryCode, null) == 0 )
      {
         return "" ;
      }
      else
      {
         return AV8IdentificationIssuingCountryCode ;
      }
   }

   @JsonProperty("IdentificationIssuingCountryCode")
   public void setIdentificationIssuingCountryCode(  String Value )
   {
      if ( Value == null )
      {
         AV8IdentificationIssuingCountryCode = "" ;
      }
      else
      {
         AV8IdentificationIssuingCountryCode= Value;
      }
   }


   String AV9PartieID;
   @JsonProperty("PartieID")
   public String getPartieID( )
   {
      if ( GXutil.strcmp(AV9PartieID, null) == 0 )
      {
         return "" ;
      }
      else
      {
         return AV9PartieID ;
      }
   }

   @JsonProperty("PartieID")
   public void setPartieID(  String Value )
   {
      if ( Value == null )
      {
         AV9PartieID = "" ;
      }
      else
      {
         AV9PartieID= Value;
      }
   }


   String AV10PartieName;
   @JsonProperty("PartieName")
   public String getPartieName( )
   {
      if ( GXutil.strcmp(AV10PartieName, null) == 0 )
      {
         return "" ;
      }
      else
      {
         return AV10PartieName ;
      }
   }

   @JsonProperty("PartieName")
   public void setPartieName(  String Value )
   {
      if ( Value == null )
      {
         AV10PartieName = "" ;
      }
      else
      {
         AV10PartieName= Value;
      }
   }


   String AV11PartieShortName;
   @JsonProperty("PartieShortName")
   public String getPartieShortName( )
   {
      if ( GXutil.strcmp(AV11PartieShortName, null) == 0 )
      {
         return "" ;
      }
      else
      {
         return AV11PartieShortName ;
      }
   }

   @JsonProperty("PartieShortName")
   public void setPartieShortName(  String Value )
   {
      if ( Value == null )
      {
         AV11PartieShortName = "" ;
      }
      else
      {
         AV11PartieShortName= Value;
      }
   }


   String AV12PartieRoleCode;
   @JsonProperty("PartieRoleCode")
   public String getPartieRoleCode( )
   {
      if ( GXutil.strcmp(AV12PartieRoleCode, null) == 0 )
      {
         return "" ;
      }
      else
      {
         return AV12PartieRoleCode ;
      }
   }

   @JsonProperty("PartieRoleCode")
   public void setPartieRoleCode(  String Value )
   {
      if ( Value == null )
      {
         AV12PartieRoleCode = "" ;
      }
      else
      {
         AV12PartieRoleCode= Value;
      }
   }


}

