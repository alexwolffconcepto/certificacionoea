package com.oea5pruebadeconcepto ;
import com.genexus.*;

@javax.xml.bind.annotation.XmlType(name = "ColAEOMasterData", namespace ="OEA5pruebaDeConcepto")
public final  class StructSdtColAEOMasterData implements Cloneable, java.io.Serializable
{
   public StructSdtColAEOMasterData( )
   {
      this( -1, new ModelContext( StructSdtColAEOMasterData.class ));
   }

   public StructSdtColAEOMasterData( int remoteHandle ,
                                     ModelContext context )
   {
   }

   public  StructSdtColAEOMasterData( java.util.Vector<StructSdtAEOMasterData> value )
   {
      item = value;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   @javax.xml.bind.annotation.XmlElement(name="AEOMasterData",namespace="OEA5pruebaDeConcepto")
   public java.util.Vector<StructSdtAEOMasterData> getItem( )
   {
      return item;
   }

   public void setItem( java.util.Vector<StructSdtAEOMasterData> value )
   {
      item = value;
   }

   protected  java.util.Vector<StructSdtAEOMasterData> item = new java.util.Vector<>();
}

