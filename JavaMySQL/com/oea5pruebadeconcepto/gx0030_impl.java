package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class gx0030_impl extends GXDataArea
{
   public gx0030_impl( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public gx0030_impl( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( gx0030_impl.class ));
   }

   public gx0030_impl( int remoteHandle ,
                       ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected void createObjects( )
   {
      dynIdentificationIssuingCountryCo = new HTMLChoice();
   }

   public void initweb( )
   {
      initialize_properties( ) ;
      if ( nGotPars == 0 )
      {
         entryPointCalled = false ;
         gxfirstwebparm = httpContext.GetFirstPar( "pIdentificationIssuingCountryCode") ;
         gxfirstwebparm_bkp = gxfirstwebparm ;
         gxfirstwebparm = httpContext.DecryptAjaxCall( gxfirstwebparm) ;
         toggleJsOutput = httpContext.isJsOutputEnabled( ) ;
         if ( httpContext.isSpaRequest( ) )
         {
            httpContext.disableJsOutput();
         }
         if ( GXutil.strcmp(gxfirstwebparm, "dyncall") == 0 )
         {
            httpContext.setAjaxCallMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            dyncall( httpContext.GetNextPar( )) ;
            return  ;
         }
         else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            httpContext.setAjaxEventMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxfirstwebparm = httpContext.GetFirstPar( "pIdentificationIssuingCountryCode") ;
         }
         else if ( GXutil.strcmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxfirstwebparm = httpContext.GetFirstPar( "pIdentificationIssuingCountryCode") ;
         }
         else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
         {
            nRC_GXsfl_64 = (int)(GXutil.lval( httpContext.GetPar( "nRC_GXsfl_64"))) ;
            nGXsfl_64_idx = (int)(GXutil.lval( httpContext.GetPar( "nGXsfl_64_idx"))) ;
            sGXsfl_64_idx = httpContext.GetPar( "sGXsfl_64_idx") ;
            httpContext.setAjaxCallMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxnrgrid1_newrow( ) ;
            return  ;
         }
         else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
         {
            subGrid1_Rows = (int)(GXutil.lval( httpContext.GetPar( "subGrid1_Rows"))) ;
            AV6cIdentificationIssuingCountryName = httpContext.GetPar( "cIdentificationIssuingCountryName") ;
            AV7cPartieID = httpContext.GetPar( "cPartieID") ;
            AV8cPartieName = httpContext.GetPar( "cPartieName") ;
            AV9cPartieShortName = httpContext.GetPar( "cPartieShortName") ;
            AV14cPartieRoleName = httpContext.GetPar( "cPartieRoleName") ;
            httpContext.setAjaxCallMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxgrgrid1_refresh( subGrid1_Rows, AV6cIdentificationIssuingCountryName, AV7cPartieID, AV8cPartieName, AV9cPartieShortName, AV14cPartieRoleName) ;
            com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "ADVANCEDCONTAINER_Class", GXutil.rtrim( divAdvancedcontainer_Class));
            com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "BTNTOGGLE_Class", GXutil.rtrim( bttBtntoggle_Class));
            com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "IDENTIFICATIONISSUINGCOUNTRYNAMEFILTERCONTAINER_Class", GXutil.rtrim( divIdentificationissuingcountrynamefiltercontainer_Class));
            com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "PARTIEIDFILTERCONTAINER_Class", GXutil.rtrim( divPartieidfiltercontainer_Class));
            com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "PARTIENAMEFILTERCONTAINER_Class", GXutil.rtrim( divPartienamefiltercontainer_Class));
            com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "PARTIESHORTNAMEFILTERCONTAINER_Class", GXutil.rtrim( divPartieshortnamefiltercontainer_Class));
            com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "PARTIEROLENAMEFILTERCONTAINER_Class", GXutil.rtrim( divPartierolenamefiltercontainer_Class));
            addString( httpContext.getJSONResponse( )) ;
            return  ;
         }
         else
         {
            if ( ! httpContext.IsValidAjaxCall( false) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp ;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            AV11pIdentificationIssuingCountryCode = gxfirstwebparm ;
            httpContext.ajax_rsp_assign_attri("", false, "AV11pIdentificationIssuingCountryCode", AV11pIdentificationIssuingCountryCode);
            if ( GXutil.strcmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV12pPartieID = httpContext.GetPar( "pPartieID") ;
               httpContext.ajax_rsp_assign_attri("", false, "AV12pPartieID", AV12pPartieID);
            }
         }
         if ( toggleJsOutput )
         {
            if ( httpContext.isSpaRequest( ) )
            {
               httpContext.enableJsOutput();
            }
         }
      }
      if ( ! httpContext.isLocalStorageSupported( ) )
      {
         httpContext.pushCurrentUrl();
      }
   }

   public void webExecute( )
   {
      initweb( ) ;
      if ( ! isAjaxCallMode( ) )
      {
         MasterPageObj= createMasterPage(remoteHandle, "com.oea5pruebadeconcepto.rwdpromptmasterpage");
         MasterPageObj.setDataArea(this,true);
         validateSpaRequest();
         MasterPageObj.webExecute();
         if ( ( GxWebError == 0 ) && httpContext.isAjaxRequest( ) )
         {
            httpContext.enableOutput();
            if ( ! httpContext.isAjaxRequest( ) )
            {
               httpContext.GX_webresponse.addHeader("Cache-Control", "no-store");
            }
            if ( ! httpContext.willRedirect( ) )
            {
               addString( httpContext.getJSONResponse( )) ;
            }
            else
            {
               if ( httpContext.isAjaxRequest( ) )
               {
                  httpContext.disableOutput();
               }
               renderHtmlHeaders( ) ;
               httpContext.redirect( httpContext.wjLoc );
               httpContext.dispatchAjaxCommands();
            }
         }
      }
      if ( isAjaxCallMode( ) )
      {
         cleanup();
      }
   }

   public byte executeStartEvent( )
   {
      pa082( ) ;
      gxajaxcallmode = (byte)((isAjaxCallMode( ) ? 1 : 0)) ;
      if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
      {
         start082( ) ;
      }
      return gxajaxcallmode ;
   }

   public void renderHtmlHeaders( )
   {
      com.oea5pruebadeconcepto.GxWebStd.gx_html_headers( httpContext, 0, "", "", Form.getMeta(), Form.getMetaequiv(), true);
   }

   public void renderHtmlOpenForm( )
   {
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.enableOutput();
      }
      httpContext.writeText( "<title>") ;
      httpContext.writeValue( Form.getCaption()) ;
      httpContext.writeTextNL( "</title>") ;
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( GXutil.len( sDynURL) > 0 )
      {
         httpContext.writeText( "<BASE href=\""+sDynURL+"\" />") ;
      }
      define_styles( ) ;
      if ( nGXWrapped != 1 )
      {
         MasterPageObj.master_styles();
      }
      if ( ( ( httpContext.getBrowserType( ) == 1 ) || ( httpContext.getBrowserType( ) == 5 ) ) && ( GXutil.strcmp(httpContext.getBrowserVersion( ), "7.0") == 0 ) )
      {
         httpContext.AddJavascriptSource("json2.js", "?"+httpContext.getBuildNumber( 2240200), false, true);
      }
      httpContext.AddJavascriptSource("jquery.js", "?"+httpContext.getBuildNumber( 2240200), false, true);
      httpContext.AddJavascriptSource("gxgral.js", "?"+httpContext.getBuildNumber( 2240200), false, true);
      httpContext.AddJavascriptSource("gxcfg.js", "?202232411201432", false, true);
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.enableOutput();
      }
      httpContext.writeText( Form.getHeaderrawhtml()) ;
      httpContext.closeHtmlHeader();
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.disableOutput();
      }
      FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"" ;
      httpContext.writeText( "<body ") ;
      bodyStyle = "" + "background-color:" + WebUtils.getHTMLColor( Form.getIBackground()) + ";color:" + WebUtils.getHTMLColor( Form.getTextcolor()) + ";" ;
      if ( nGXWrapped == 0 )
      {
         bodyStyle += "-moz-opacity:0;opacity:0;" ;
      }
      if ( ! ( (GXutil.strcmp("", Form.getBackground())==0) ) )
      {
         bodyStyle += " background-image:url(" + httpContext.convertURL( Form.getBackground()) + ")" ;
      }
      httpContext.writeText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
      httpContext.writeText( FormProcess+">") ;
      httpContext.skipLines( 1 );
      httpContext.writeTextNL( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("com.oea5pruebadeconcepto.gx0030", new String[] {GXutil.URLEncode(GXutil.rtrim(AV11pIdentificationIssuingCountryCode)),GXutil.URLEncode(GXutil.rtrim(AV12pPartieID))}, new String[] {"pIdentificationIssuingCountryCode","pPartieID"}) +"\">") ;
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "_EventName", "");
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "_EventGridId", "");
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "_EventRowId", "");
      httpContext.writeText( "<input type=\"submit\" title=\"submit\" style=\"display:block;height:0;border:0;padding:0\" disabled>") ;
      httpContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
      toggleJsOutput = httpContext.isJsOutputEnabled( ) ;
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.disableJsOutput();
      }
   }

   public void send_integrity_footer_hashes( )
   {
      GXKey = httpContext.decrypt64( httpContext.getCookie( "GX_SESSION_ID"), context.getServerKey( )) ;
   }

   public void sendCloseFormHiddens( )
   {
      /* Send hidden variables. */
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GXH_vCIDENTIFICATIONISSUINGCOUNTRYNAME", AV6cIdentificationIssuingCountryName);
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GXH_vCPARTIEID", GXutil.rtrim( AV7cPartieID));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GXH_vCPARTIENAME", AV8cPartieName);
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GXH_vCPARTIESHORTNAME", AV9cPartieShortName);
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GXH_vCPARTIEROLENAME", AV14cPartieRoleName);
      /* Send saved values. */
      send_integrity_footer_hashes( ) ;
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "nRC_GXsfl_64", GXutil.ltrim( localUtil.ntoc( nRC_GXsfl_64, (byte)(8), (byte)(0), ",", "")));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "vPIDENTIFICATIONISSUINGCOUNTRYCODE", GXutil.rtrim( AV11pIdentificationIssuingCountryCode));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "vPPARTIEID", GXutil.rtrim( AV12pPartieID));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GRID1_nFirstRecordOnPage", GXutil.ltrim( localUtil.ntoc( GRID1_nFirstRecordOnPage, (byte)(15), (byte)(0), ",", "")));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GRID1_nEOF", GXutil.ltrim( localUtil.ntoc( GRID1_nEOF, (byte)(1), (byte)(0), ",", "")));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "ADVANCEDCONTAINER_Class", GXutil.rtrim( divAdvancedcontainer_Class));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "BTNTOGGLE_Class", GXutil.rtrim( bttBtntoggle_Class));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "IDENTIFICATIONISSUINGCOUNTRYNAMEFILTERCONTAINER_Class", GXutil.rtrim( divIdentificationissuingcountrynamefiltercontainer_Class));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "PARTIEIDFILTERCONTAINER_Class", GXutil.rtrim( divPartieidfiltercontainer_Class));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "PARTIENAMEFILTERCONTAINER_Class", GXutil.rtrim( divPartienamefiltercontainer_Class));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "PARTIESHORTNAMEFILTERCONTAINER_Class", GXutil.rtrim( divPartieshortnamefiltercontainer_Class));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "PARTIEROLENAMEFILTERCONTAINER_Class", GXutil.rtrim( divPartierolenamefiltercontainer_Class));
   }

   public void renderHtmlCloseForm( )
   {
      sendCloseFormHiddens( ) ;
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GX_FocusControl", "notset");
      httpContext.SendAjaxEncryptionKey();
      sendSecurityToken(sPrefix);
      httpContext.SendComponentObjects();
      httpContext.SendServerCommands();
      httpContext.SendState();
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.disableOutput();
      }
      httpContext.writeTextNL( "</form>") ;
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.enableOutput();
      }
      include_jscripts( ) ;
   }

   public void renderHtmlContent( )
   {
      gxajaxcallmode = (byte)((isAjaxCallMode( ) ? 1 : 0)) ;
      if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
      {
         httpContext.writeText( "<div") ;
         com.oea5pruebadeconcepto.GxWebStd.classAttribute( httpContext, "gx-ct-body"+" "+((GXutil.strcmp("", Form.getThemeClass())==0) ? "form-horizontal Form" : Form.getThemeClass())+"-fx");
         httpContext.writeText( ">") ;
         we082( ) ;
         httpContext.writeText( "</div>") ;
      }
   }

   public void dispatchEvents( )
   {
      evt082( ) ;
   }

   public boolean hasEnterEvent( )
   {
      return true ;
   }

   public com.genexus.webpanels.GXWebForm getForm( )
   {
      return Form ;
   }

   public String getSelfLink( )
   {
      return formatLink("com.oea5pruebadeconcepto.gx0030", new String[] {GXutil.URLEncode(GXutil.rtrim(AV11pIdentificationIssuingCountryCode)),GXutil.URLEncode(GXutil.rtrim(AV12pPartieID))}, new String[] {"pIdentificationIssuingCountryCode","pPartieID"})  ;
   }

   public String getPgmname( )
   {
      return "Gx0030" ;
   }

   public String getPgmdesc( )
   {
      return "Selection List AEO Master Data" ;
   }

   public void wb080( )
   {
      if ( httpContext.isAjaxRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( ! wbLoad )
      {
         if ( nGXWrapped == 1 )
         {
            renderHtmlHeaders( ) ;
            renderHtmlOpenForm( ) ;
         }
         com.oea5pruebadeconcepto.GxWebStd.gx_msg_list( httpContext, "", httpContext.GX_msglist.getDisplaymode(), "", "", "", "false");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, divMain_Internalname, 1, 0, "px", 0, "px", "ContainerFluid PromptContainer", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-3 PromptAdvancedBarCell", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, divAdvancedcontainer_Internalname, 1, 0, "px", 0, "px", divAdvancedcontainer_Class, "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, divIdentificationissuingcountrynamefiltercontainer_Internalname, 1, 0, "px", 0, "px", divIdentificationissuingcountrynamefiltercontainer_Class, "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         com.oea5pruebadeconcepto.GxWebStd.gx_label_ctrl( httpContext, lblLblidentificationissuingcountrynamefilter_Internalname, "Country Name", "", "", lblLblidentificationissuingcountrynamefilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e11081_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 0, (short)(1), "HLP_Gx0030.htm");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
         /* Attribute/Variable Label */
         com.oea5pruebadeconcepto.GxWebStd.gx_label_element( httpContext, edtavCidentificationissuingcountryname_Internalname, "Identification Issuing Country Name", "col-sm-3 AttributeLabel", 0, true, "");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_64_idx + "',0)\"" ;
         com.oea5pruebadeconcepto.GxWebStd.gx_single_line_edit( httpContext, edtavCidentificationissuingcountryname_Internalname, AV6cIdentificationIssuingCountryName, GXutil.rtrim( localUtil.format( AV6cIdentificationIssuingCountryName, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "Identification Issuing Country Name", "", edtavCidentificationissuingcountryname_Jsonclick, 0, "Attribute", "", "", "", "", edtavCidentificationissuingcountryname_Visible, edtavCidentificationissuingcountryname_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), (byte)(-1), true, "", "left", true, "", "HLP_Gx0030.htm");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, divPartieidfiltercontainer_Internalname, 1, 0, "px", 0, "px", divPartieidfiltercontainer_Class, "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         com.oea5pruebadeconcepto.GxWebStd.gx_label_ctrl( httpContext, lblLblpartieidfilter_Internalname, "Partie ID", "", "", lblLblpartieidfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e12081_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 0, (short)(1), "HLP_Gx0030.htm");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
         /* Attribute/Variable Label */
         com.oea5pruebadeconcepto.GxWebStd.gx_label_element( httpContext, edtavCpartieid_Internalname, "Partie ID", "col-sm-3 AttributeLabel", 0, true, "");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_64_idx + "',0)\"" ;
         com.oea5pruebadeconcepto.GxWebStd.gx_single_line_edit( httpContext, edtavCpartieid_Internalname, GXutil.rtrim( AV7cPartieID), GXutil.rtrim( localUtil.format( AV7cPartieID, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCpartieid_Jsonclick, 0, "Attribute", "", "", "", "", edtavCpartieid_Visible, edtavCpartieid_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), (byte)(-1), true, "", "left", true, "", "HLP_Gx0030.htm");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, divPartienamefiltercontainer_Internalname, 1, 0, "px", 0, "px", divPartienamefiltercontainer_Class, "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         com.oea5pruebadeconcepto.GxWebStd.gx_label_ctrl( httpContext, lblLblpartienamefilter_Internalname, "Partie Name", "", "", lblLblpartienamefilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e13081_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 0, (short)(1), "HLP_Gx0030.htm");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
         /* Attribute/Variable Label */
         com.oea5pruebadeconcepto.GxWebStd.gx_label_element( httpContext, edtavCpartiename_Internalname, "Partie Name", "col-sm-3 AttributeLabel", 0, true, "");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_64_idx + "',0)\"" ;
         com.oea5pruebadeconcepto.GxWebStd.gx_single_line_edit( httpContext, edtavCpartiename_Internalname, AV8cPartieName, GXutil.rtrim( localUtil.format( AV8cPartieName, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCpartiename_Jsonclick, 0, "Attribute", "", "", "", "", edtavCpartiename_Visible, edtavCpartiename_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), (byte)(-1), true, "", "left", true, "", "HLP_Gx0030.htm");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, divPartieshortnamefiltercontainer_Internalname, 1, 0, "px", 0, "px", divPartieshortnamefiltercontainer_Class, "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         com.oea5pruebadeconcepto.GxWebStd.gx_label_ctrl( httpContext, lblLblpartieshortnamefilter_Internalname, "Partie Short Name", "", "", lblLblpartieshortnamefilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e14081_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 0, (short)(1), "HLP_Gx0030.htm");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
         /* Attribute/Variable Label */
         com.oea5pruebadeconcepto.GxWebStd.gx_label_element( httpContext, edtavCpartieshortname_Internalname, "Partie Short Name", "col-sm-3 AttributeLabel", 0, true, "");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_64_idx + "',0)\"" ;
         com.oea5pruebadeconcepto.GxWebStd.gx_single_line_edit( httpContext, edtavCpartieshortname_Internalname, AV9cPartieShortName, GXutil.rtrim( localUtil.format( AV9cPartieShortName, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCpartieshortname_Jsonclick, 0, "Attribute", "", "", "", "", edtavCpartieshortname_Visible, edtavCpartieshortname_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), (byte)(-1), true, "", "left", true, "", "HLP_Gx0030.htm");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, divPartierolenamefiltercontainer_Internalname, 1, 0, "px", 0, "px", divPartierolenamefiltercontainer_Class, "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         com.oea5pruebadeconcepto.GxWebStd.gx_label_ctrl( httpContext, lblLblpartierolenamefilter_Internalname, "Partie Role Name", "", "", lblLblpartierolenamefilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e15081_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 0, (short)(1), "HLP_Gx0030.htm");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
         /* Attribute/Variable Label */
         com.oea5pruebadeconcepto.GxWebStd.gx_label_element( httpContext, edtavCpartierolename_Internalname, "Partie Role Name", "col-sm-3 AttributeLabel", 0, true, "");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_64_idx + "',0)\"" ;
         com.oea5pruebadeconcepto.GxWebStd.gx_single_line_edit( httpContext, edtavCpartierolename_Internalname, AV14cPartieRoleName, GXutil.rtrim( localUtil.format( AV14cPartieRoleName, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCpartierolename_Jsonclick, 0, "Attribute", "", "", "", "", edtavCpartierolename_Visible, edtavCpartierolename_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), (byte)(-1), true, "", "left", true, "", "HLP_Gx0030.htm");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 WWGridCell", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, divGridtable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 hidden-sm hidden-md hidden-lg ToggleCell", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"" ;
         ClassString = bttBtntoggle_Class ;
         StyleString = "" ;
         com.oea5pruebadeconcepto.GxWebStd.gx_button_ctrl( httpContext, bttBtntoggle_Internalname, "gx.evt.setGridEvt("+GXutil.str( 64, 2, 0)+","+"null"+");", "|||", bttBtntoggle_Jsonclick, 7, "|||", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e16081_client"+"'", TempTags, "", 2, "HLP_Gx0030.htm");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /*  Grid Control  */
         Grid1Container.SetWrapped(nGXWrapped);
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"64\">") ;
            sStyleString = "" ;
            com.oea5pruebadeconcepto.GxWebStd.gx_table_start( httpContext, subGrid1_Internalname, subGrid1_Internalname, "", "PromptGrid", 0, "", "", 1, 2, sStyleString, "", "", 0);
            /* Subfile titles */
            httpContext.writeText( "<tr") ;
            httpContext.writeTextNL( ">") ;
            if ( subGrid1_Backcolorstyle == 0 )
            {
               subGrid1_Titlebackstyle = (byte)(0) ;
               if ( GXutil.len( subGrid1_Class) > 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Title" ;
               }
            }
            else
            {
               subGrid1_Titlebackstyle = (byte)(1) ;
               if ( subGrid1_Backcolorstyle == 1 )
               {
                  subGrid1_Titlebackcolor = subGrid1_Allbackcolor ;
                  if ( GXutil.len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"UniformTitle" ;
                  }
               }
               else
               {
                  if ( GXutil.len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title" ;
                  }
               }
            }
            httpContext.writeText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"SelectionAttribute"+"\" "+" style=\""+""+""+"\" "+">") ;
            httpContext.writeValue( "") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"DescriptionAttribute"+"\" "+" style=\""+""+""+"\" "+">") ;
            httpContext.writeValue( "Country Name") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
            httpContext.writeValue( "Partie ID") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
            httpContext.writeValue( "Role Name") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
            httpContext.writeValue( "Country") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeTextNL( "</tr>") ;
            Grid1Container.AddObjectProperty("GridName", "Grid1");
         }
         else
         {
            if ( isAjaxCallMode( ) )
            {
               Grid1Container = new com.genexus.webpanels.GXWebGrid(context);
            }
            else
            {
               Grid1Container.Clear();
            }
            Grid1Container.SetWrapped(nGXWrapped);
            Grid1Container.AddObjectProperty("GridName", "Grid1");
            Grid1Container.AddObjectProperty("Header", subGrid1_Header);
            Grid1Container.AddObjectProperty("Class", "PromptGrid");
            Grid1Container.AddObjectProperty("Cellpadding", GXutil.ltrim( localUtil.ntoc( 1, (byte)(4), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("Cellspacing", GXutil.ltrim( localUtil.ntoc( 2, (byte)(4), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("Backcolorstyle", GXutil.ltrim( localUtil.ntoc( subGrid1_Backcolorstyle, (byte)(1), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("CmpContext", "");
            Grid1Container.AddObjectProperty("InMasterPage", "false");
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", httpContext.convertURL( AV5LinkSelection));
            Grid1Column.AddObjectProperty("Link", GXutil.rtrim( edtavLinkselection_Link));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", A4IdentificationIssuingCountryNa);
            Grid1Column.AddObjectProperty("Link", GXutil.rtrim( edtIdentificationIssuingCountryNa_Link));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", GXutil.rtrim( A5PartieID));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", A9PartieRoleName);
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", GXutil.rtrim( A1IdentificationIssuingCountryCo));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Container.AddObjectProperty("Selectedindex", GXutil.ltrim( localUtil.ntoc( subGrid1_Selectedindex, (byte)(4), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("Allowselection", GXutil.ltrim( localUtil.ntoc( subGrid1_Allowselection, (byte)(1), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("Selectioncolor", GXutil.ltrim( localUtil.ntoc( subGrid1_Selectioncolor, (byte)(9), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("Allowhover", GXutil.ltrim( localUtil.ntoc( subGrid1_Allowhovering, (byte)(1), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("Hovercolor", GXutil.ltrim( localUtil.ntoc( subGrid1_Hoveringcolor, (byte)(9), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("Allowcollapsing", GXutil.ltrim( localUtil.ntoc( subGrid1_Allowcollapsing, (byte)(1), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("Collapsed", GXutil.ltrim( localUtil.ntoc( subGrid1_Collapsed, (byte)(1), (byte)(0), ".", "")));
         }
      }
      if ( wbEnd == 64 )
      {
         wbEnd = (short)(0) ;
         nRC_GXsfl_64 = (int)(nGXsfl_64_idx-1) ;
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "</table>") ;
            httpContext.writeText( "</div>") ;
         }
         else
         {
            Grid1Container.AddObjectProperty("GRID1_nEOF", GRID1_nEOF);
            Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
            sStyleString = "" ;
            httpContext.writeText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
            httpContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
            if ( ! httpContext.isAjaxRequest( ) && ! httpContext.isSpaRequest( ) )
            {
               com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
            }
            if ( httpContext.isAjaxRequest( ) || httpContext.isSpaRequest( ) )
            {
               com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
            }
            else
            {
               httpContext.writeText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
            }
         }
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"" ;
         ClassString = "BtnCancel" ;
         StyleString = "" ;
         com.oea5pruebadeconcepto.GxWebStd.gx_button_ctrl( httpContext, bttBtn_cancel_Internalname, "gx.evt.setGridEvt("+GXutil.str( 64, 2, 0)+","+"null"+");", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", httpContext.getButtonType( ), "HLP_Gx0030.htm");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
         com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      }
      if ( wbEnd == 64 )
      {
         wbEnd = (short)(0) ;
         if ( isFullAjaxMode( ) )
         {
            if ( Grid1Container.GetWrapped() == 1 )
            {
               httpContext.writeText( "</table>") ;
               httpContext.writeText( "</div>") ;
            }
            else
            {
               Grid1Container.AddObjectProperty("GRID1_nEOF", GRID1_nEOF);
               Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
               sStyleString = "" ;
               httpContext.writeText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               httpContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! httpContext.isAjaxRequest( ) && ! httpContext.isSpaRequest( ) )
               {
                  com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( httpContext.isAjaxRequest( ) || httpContext.isSpaRequest( ) )
               {
                  com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  httpContext.writeText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
         }
      }
      wbLoad = true ;
   }

   public void start082( )
   {
      wbLoad = false ;
      wbEnd = 0 ;
      wbStart = 0 ;
      if ( ! httpContext.isSpaRequest( ) )
      {
         if ( httpContext.exposeMetadata( ) )
         {
            Form.getMeta().addItem("generator", "GeneXus Java 17_0_8-158023", (short)(0)) ;
         }
         Form.getMeta().addItem("description", "Selection List AEO Master Data", (short)(0)) ;
      }
      httpContext.wjLoc = "" ;
      httpContext.nUserReturn = (byte)(0) ;
      httpContext.wbHandled = (byte)(0) ;
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
      }
      wbErr = false ;
      strup080( ) ;
   }

   public void ws082( )
   {
      start082( ) ;
      evt082( ) ;
   }

   public void evt082( )
   {
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         if ( ! httpContext.willRedirect( ) && ( httpContext.nUserReturn != 1 ) && ! wbErr )
         {
            /* Read Web Panel buttons. */
            sEvt = httpContext.cgiGet( "_EventName") ;
            EvtGridId = httpContext.cgiGet( "_EventGridId") ;
            EvtRowId = httpContext.cgiGet( "_EventRowId") ;
            if ( GXutil.len( sEvt) > 0 )
            {
               sEvtType = GXutil.left( sEvt, 1) ;
               sEvt = GXutil.right( sEvt, GXutil.len( sEvt)-1) ;
               if ( GXutil.strcmp(sEvtType, "M") != 0 )
               {
                  if ( GXutil.strcmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = GXutil.right( sEvt, 1) ;
                     if ( GXutil.strcmp(sEvtType, ".") == 0 )
                     {
                        sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                        if ( GXutil.strcmp(sEvt, "RFR") == 0 )
                        {
                           httpContext.wbHandled = (byte)(1) ;
                           dynload_actions( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( GXutil.strcmp(sEvt, "LSCR") == 0 )
                        {
                           httpContext.wbHandled = (byte)(1) ;
                           dynload_actions( ) ;
                        }
                        else if ( GXutil.strcmp(sEvt, "GRID1PAGING") == 0 )
                        {
                           httpContext.wbHandled = (byte)(1) ;
                           sEvt = httpContext.cgiGet( "GRID1PAGING") ;
                           if ( GXutil.strcmp(sEvt, "FIRST") == 0 )
                           {
                              subgrid1_firstpage( ) ;
                           }
                           else if ( GXutil.strcmp(sEvt, "PREV") == 0 )
                           {
                              subgrid1_previouspage( ) ;
                           }
                           else if ( GXutil.strcmp(sEvt, "NEXT") == 0 )
                           {
                              subgrid1_nextpage( ) ;
                           }
                           else if ( GXutil.strcmp(sEvt, "LAST") == 0 )
                           {
                              subgrid1_lastpage( ) ;
                           }
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = GXutil.right( sEvt, 4) ;
                        sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-4) ;
                        if ( ( GXutil.strcmp(GXutil.left( sEvt, 5), "START") == 0 ) || ( GXutil.strcmp(GXutil.left( sEvt, 4), "LOAD") == 0 ) || ( GXutil.strcmp(GXutil.left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_64_idx = (int)(GXutil.lval( sEvtType)) ;
                           sGXsfl_64_idx = GXutil.padl( GXutil.ltrimstr( DecimalUtil.doubleToDec(nGXsfl_64_idx), 4, 0), (short)(4), "0") ;
                           subsflControlProps_642( ) ;
                           AV5LinkSelection = httpContext.cgiGet( edtavLinkselection_Internalname) ;
                           httpContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "Bitmap", ((GXutil.strcmp("", AV5LinkSelection)==0) ? AV17Linkselection_GXI : httpContext.convertURL( httpContext.getResourceRelative(AV5LinkSelection))), !bGXsfl_64_Refreshing);
                           httpContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "SrcSet", context.getHttpContext().getImageSrcSet( AV5LinkSelection), true);
                           A4IdentificationIssuingCountryNa = httpContext.cgiGet( edtIdentificationIssuingCountryNa_Internalname) ;
                           A5PartieID = httpContext.cgiGet( edtPartieID_Internalname) ;
                           A9PartieRoleName = httpContext.cgiGet( edtPartieRoleName_Internalname) ;
                           dynIdentificationIssuingCountryCo.setName( dynIdentificationIssuingCountryCo.getInternalname() );
                           dynIdentificationIssuingCountryCo.setValue( httpContext.cgiGet( dynIdentificationIssuingCountryCo.getInternalname()) );
                           A1IdentificationIssuingCountryCo = httpContext.cgiGet( dynIdentificationIssuingCountryCo.getInternalname()) ;
                           sEvtType = GXutil.right( sEvt, 1) ;
                           if ( GXutil.strcmp(sEvtType, ".") == 0 )
                           {
                              sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                              if ( GXutil.strcmp(sEvt, "START") == 0 )
                              {
                                 httpContext.wbHandled = (byte)(1) ;
                                 dynload_actions( ) ;
                                 /* Execute user event: Start */
                                 e17082 ();
                              }
                              else if ( GXutil.strcmp(sEvt, "LOAD") == 0 )
                              {
                                 httpContext.wbHandled = (byte)(1) ;
                                 dynload_actions( ) ;
                                 /* Execute user event: Load */
                                 e18082 ();
                              }
                              else if ( GXutil.strcmp(sEvt, "ENTER") == 0 )
                              {
                                 httpContext.wbHandled = (byte)(1) ;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false ;
                                    /* Set Refresh If Cidentificationissuingcountryname Changed */
                                    if ( GXutil.strcmp(httpContext.cgiGet( "GXH_vCIDENTIFICATIONISSUINGCOUNTRYNAME"), AV6cIdentificationIssuingCountryName) != 0 )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cpartieid Changed */
                                    if ( GXutil.strcmp(httpContext.cgiGet( "GXH_vCPARTIEID"), AV7cPartieID) != 0 )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cpartiename Changed */
                                    if ( GXutil.strcmp(httpContext.cgiGet( "GXH_vCPARTIENAME"), AV8cPartieName) != 0 )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cpartieshortname Changed */
                                    if ( GXutil.strcmp(httpContext.cgiGet( "GXH_vCPARTIESHORTNAME"), AV9cPartieShortName) != 0 )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cpartierolename Changed */
                                    if ( GXutil.strcmp(httpContext.cgiGet( "GXH_vCPARTIEROLENAME"), AV14cPartieRoleName) != 0 )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: Enter */
                                       e19082 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( GXutil.strcmp(sEvt, "LSCR") == 0 )
                              {
                                 httpContext.wbHandled = (byte)(1) ;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  httpContext.wbHandled = (byte)(1) ;
               }
            }
         }
      }
   }

   public void we082( )
   {
      if ( ! com.oea5pruebadeconcepto.GxWebStd.gx_redirect( httpContext) )
      {
         Rfr0gs = true ;
         refresh( ) ;
         if ( ! com.oea5pruebadeconcepto.GxWebStd.gx_redirect( httpContext) )
         {
            if ( nGXWrapped == 1 )
            {
               renderHtmlCloseForm( ) ;
            }
         }
      }
   }

   public void pa082( )
   {
      if ( nDonePA == 0 )
      {
         if ( (GXutil.strcmp("", httpContext.getCookie( "GX_SESSION_ID"))==0) )
         {
            gxcookieaux = httpContext.setCookie( "GX_SESSION_ID", httpContext.encrypt64( com.genexus.util.Encryption.getNewKey( ), context.getServerKey( )), "", GXutil.nullDate(), "", (short)(httpContext.getHttpSecure( ))) ;
         }
         GXKey = httpContext.decrypt64( httpContext.getCookie( "GX_SESSION_ID"), context.getServerKey( )) ;
         toggleJsOutput = httpContext.isJsOutputEnabled( ) ;
         if ( httpContext.isSpaRequest( ) )
         {
            httpContext.disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( httpContext.isSpaRequest( ) )
            {
               httpContext.enableJsOutput();
            }
         }
         nDonePA = (byte)(1) ;
      }
   }

   public void dynload_actions( )
   {
      /* End function dynload_actions */
   }

   public void gxnrgrid1_newrow( )
   {
      com.oea5pruebadeconcepto.GxWebStd.set_html_headers( httpContext, 0, "", "");
      subsflControlProps_642( ) ;
      while ( nGXsfl_64_idx <= nRC_GXsfl_64 )
      {
         sendrow_642( ) ;
         nGXsfl_64_idx = ((subGrid1_Islastpage==1)&&(nGXsfl_64_idx+1>subgrid1_fnc_recordsperpage( )) ? 1 : nGXsfl_64_idx+1) ;
         sGXsfl_64_idx = GXutil.padl( GXutil.ltrimstr( DecimalUtil.doubleToDec(nGXsfl_64_idx), 4, 0), (short)(4), "0") ;
         subsflControlProps_642( ) ;
      }
      addString( httpContext.getJSONContainerResponse( Grid1Container)) ;
      /* End function gxnrGrid1_newrow */
   }

   public void gxgrgrid1_refresh( int subGrid1_Rows ,
                                  String AV6cIdentificationIssuingCountryName ,
                                  String AV7cPartieID ,
                                  String AV8cPartieName ,
                                  String AV9cPartieShortName ,
                                  String AV14cPartieRoleName )
   {
      initialize_formulas( ) ;
      com.oea5pruebadeconcepto.GxWebStd.set_html_headers( httpContext, 0, "", "");
      GRID1_nCurrentRecord = 0 ;
      rf082( ) ;
      GXKey = httpContext.decrypt64( httpContext.getCookie( "GX_SESSION_ID"), context.getServerKey( )) ;
      send_integrity_footer_hashes( ) ;
      GXKey = httpContext.decrypt64( httpContext.getCookie( "GX_SESSION_ID"), context.getServerKey( )) ;
      /* End function gxgrGrid1_refresh */
   }

   public void send_integrity_hashes( )
   {
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "gxhash_IDENTIFICATIONISSUINGCOUNTRYCO", getSecureSignedToken( "", GXutil.rtrim( localUtil.format( A1IdentificationIssuingCountryCo, ""))));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "IDENTIFICATIONISSUINGCOUNTRYCO", GXutil.rtrim( A1IdentificationIssuingCountryCo));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "gxhash_PARTIEID", getSecureSignedToken( "", GXutil.rtrim( localUtil.format( A5PartieID, ""))));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "PARTIEID", GXutil.rtrim( A5PartieID));
   }

   public void clear_multi_value_controls( )
   {
      if ( httpContext.isAjaxRequest( ) )
      {
         dynload_actions( ) ;
         before_start_formulas( ) ;
      }
   }

   public void fix_multi_value_controls( )
   {
   }

   public void refresh( )
   {
      send_integrity_hashes( ) ;
      rf082( ) ;
      if ( isFullAjaxMode( ) )
      {
         send_integrity_footer_hashes( ) ;
      }
      /* End function Refresh */
   }

   public void initialize_formulas( )
   {
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   public void rf082( )
   {
      initialize_formulas( ) ;
      clear_multi_value_controls( ) ;
      if ( isAjaxCallMode( ) )
      {
         Grid1Container.ClearRows();
      }
      wbStart = (short)(64) ;
      nGXsfl_64_idx = 1 ;
      sGXsfl_64_idx = GXutil.padl( GXutil.ltrimstr( DecimalUtil.doubleToDec(nGXsfl_64_idx), 4, 0), (short)(4), "0") ;
      subsflControlProps_642( ) ;
      bGXsfl_64_Refreshing = true ;
      Grid1Container.AddObjectProperty("GridName", "Grid1");
      Grid1Container.AddObjectProperty("CmpContext", "");
      Grid1Container.AddObjectProperty("InMasterPage", "false");
      Grid1Container.AddObjectProperty("Class", "PromptGrid");
      Grid1Container.AddObjectProperty("Cellpadding", GXutil.ltrim( localUtil.ntoc( 1, (byte)(4), (byte)(0), ".", "")));
      Grid1Container.AddObjectProperty("Cellspacing", GXutil.ltrim( localUtil.ntoc( 2, (byte)(4), (byte)(0), ".", "")));
      Grid1Container.AddObjectProperty("Backcolorstyle", GXutil.ltrim( localUtil.ntoc( subGrid1_Backcolorstyle, (byte)(1), (byte)(0), ".", "")));
      Grid1Container.setPageSize( subgrid1_fnc_recordsperpage( ) );
      gxdyncontrolsrefreshing = true ;
      fix_multi_value_controls( ) ;
      gxdyncontrolsrefreshing = false ;
      if ( ! httpContext.willRedirect( ) && ( httpContext.nUserReturn != 1 ) )
      {
         subsflControlProps_642( ) ;
         GXPagingFrom2 = (int)(GRID1_nFirstRecordOnPage) ;
         GXPagingTo2 = (int)(subgrid1_fnc_recordsperpage( )+1) ;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV6cIdentificationIssuingCountryName ,
                                              AV8cPartieName ,
                                              AV9cPartieShortName ,
                                              AV14cPartieRoleName ,
                                              A4IdentificationIssuingCountryNa ,
                                              A6PartieName ,
                                              A7PartieShortName ,
                                              A9PartieRoleName ,
                                              A5PartieID ,
                                              AV7cPartieID } ,
                                              new int[]{
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV7cPartieID = GXutil.padr( GXutil.rtrim( AV7cPartieID), 30, "%") ;
         lV6cIdentificationIssuingCountryName = GXutil.concat( GXutil.rtrim( AV6cIdentificationIssuingCountryName), "%", "") ;
         lV8cPartieName = GXutil.concat( GXutil.rtrim( AV8cPartieName), "%", "") ;
         lV9cPartieShortName = GXutil.concat( GXutil.rtrim( AV9cPartieShortName), "%", "") ;
         lV14cPartieRoleName = GXutil.concat( GXutil.rtrim( AV14cPartieRoleName), "%", "") ;
         /* Using cursor H00082 */
         pr_default.execute(0, new Object[] {lV7cPartieID, lV6cIdentificationIssuingCountryName, lV8cPartieName, lV9cPartieShortName, lV14cPartieRoleName, Integer.valueOf(GXPagingFrom2), Integer.valueOf(GXPagingTo2)});
         nGXsfl_64_idx = 1 ;
         sGXsfl_64_idx = GXutil.padl( GXutil.ltrimstr( DecimalUtil.doubleToDec(nGXsfl_64_idx), 4, 0), (short)(4), "0") ;
         subsflControlProps_642( ) ;
         while ( ( (pr_default.getStatus(0) != 101) ) && ( ( GRID1_nCurrentRecord < subgrid1_fnc_recordsperpage( ) ) ) )
         {
            A8PartieRoleCode = H00082_A8PartieRoleCode[0] ;
            A7PartieShortName = H00082_A7PartieShortName[0] ;
            A6PartieName = H00082_A6PartieName[0] ;
            A1IdentificationIssuingCountryCo = H00082_A1IdentificationIssuingCountryCo[0] ;
            A9PartieRoleName = H00082_A9PartieRoleName[0] ;
            A5PartieID = H00082_A5PartieID[0] ;
            A4IdentificationIssuingCountryNa = H00082_A4IdentificationIssuingCountryNa[0] ;
            A9PartieRoleName = H00082_A9PartieRoleName[0] ;
            A4IdentificationIssuingCountryNa = H00082_A4IdentificationIssuingCountryNa[0] ;
            /* Execute user event: Load */
            e18082 ();
            pr_default.readNext(0);
         }
         GRID1_nEOF = (byte)(((pr_default.getStatus(0) == 101) ? 1 : 0)) ;
         com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GRID1_nEOF", GXutil.ltrim( localUtil.ntoc( GRID1_nEOF, (byte)(1), (byte)(0), ".", "")));
         pr_default.close(0);
         wbEnd = (short)(64) ;
         wb080( ) ;
      }
      bGXsfl_64_Refreshing = true ;
   }

   public void send_integrity_lvl_hashes082( )
   {
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "gxhash_IDENTIFICATIONISSUINGCOUNTRYCO"+"_"+sGXsfl_64_idx, getSecureSignedToken( sGXsfl_64_idx, GXutil.rtrim( localUtil.format( A1IdentificationIssuingCountryCo, ""))));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "gxhash_PARTIEID"+"_"+sGXsfl_64_idx, getSecureSignedToken( sGXsfl_64_idx, GXutil.rtrim( localUtil.format( A5PartieID, ""))));
   }

   public int subgrid1_fnc_pagecount( )
   {
      GRID1_nRecordCount = subgrid1_fnc_recordcount( ) ;
      if ( ((int)((GRID1_nRecordCount) % (subgrid1_fnc_recordsperpage( )))) == 0 )
      {
         return (int)(GXutil.Int( GRID1_nRecordCount/ (double) (subgrid1_fnc_recordsperpage( )))) ;
      }
      return (int)(GXutil.Int( GRID1_nRecordCount/ (double) (subgrid1_fnc_recordsperpage( )))+1) ;
   }

   public int subgrid1_fnc_recordcount( )
   {
      pr_default.dynParam(1, new Object[]{ new Object[]{
                                           AV6cIdentificationIssuingCountryName ,
                                           AV8cPartieName ,
                                           AV9cPartieShortName ,
                                           AV14cPartieRoleName ,
                                           A4IdentificationIssuingCountryNa ,
                                           A6PartieName ,
                                           A7PartieShortName ,
                                           A9PartieRoleName ,
                                           A5PartieID ,
                                           AV7cPartieID } ,
                                           new int[]{
                                           TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                           }
      });
      lV7cPartieID = GXutil.padr( GXutil.rtrim( AV7cPartieID), 30, "%") ;
      lV6cIdentificationIssuingCountryName = GXutil.concat( GXutil.rtrim( AV6cIdentificationIssuingCountryName), "%", "") ;
      lV8cPartieName = GXutil.concat( GXutil.rtrim( AV8cPartieName), "%", "") ;
      lV9cPartieShortName = GXutil.concat( GXutil.rtrim( AV9cPartieShortName), "%", "") ;
      lV14cPartieRoleName = GXutil.concat( GXutil.rtrim( AV14cPartieRoleName), "%", "") ;
      /* Using cursor H00083 */
      pr_default.execute(1, new Object[] {lV7cPartieID, lV6cIdentificationIssuingCountryName, lV8cPartieName, lV9cPartieShortName, lV14cPartieRoleName});
      GRID1_nRecordCount = H00083_AGRID1_nRecordCount[0] ;
      pr_default.close(1);
      return (int)(GRID1_nRecordCount) ;
   }

   public int subgrid1_fnc_recordsperpage( )
   {
      return 10*1 ;
   }

   public int subgrid1_fnc_currentpage( )
   {
      return (int)(GXutil.Int( GRID1_nFirstRecordOnPage/ (double) (subgrid1_fnc_recordsperpage( )))+1) ;
   }

   public short subgrid1_firstpage( )
   {
      GRID1_nFirstRecordOnPage = 0 ;
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GRID1_nFirstRecordOnPage", GXutil.ltrim( localUtil.ntoc( GRID1_nFirstRecordOnPage, (byte)(15), (byte)(0), ".", "")));
      if ( isFullAjaxMode( ) )
      {
         gxgrgrid1_refresh( subGrid1_Rows, AV6cIdentificationIssuingCountryName, AV7cPartieID, AV8cPartieName, AV9cPartieShortName, AV14cPartieRoleName) ;
      }
      send_integrity_footer_hashes( ) ;
      return (short)(0) ;
   }

   public short subgrid1_nextpage( )
   {
      GRID1_nRecordCount = subgrid1_fnc_recordcount( ) ;
      if ( ( GRID1_nRecordCount >= subgrid1_fnc_recordsperpage( ) ) && ( GRID1_nEOF == 0 ) )
      {
         GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage+subgrid1_fnc_recordsperpage( )) ;
      }
      else
      {
         return (short)(2) ;
      }
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GRID1_nFirstRecordOnPage", GXutil.ltrim( localUtil.ntoc( GRID1_nFirstRecordOnPage, (byte)(15), (byte)(0), ".", "")));
      Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
      if ( isFullAjaxMode( ) )
      {
         gxgrgrid1_refresh( subGrid1_Rows, AV6cIdentificationIssuingCountryName, AV7cPartieID, AV8cPartieName, AV9cPartieShortName, AV14cPartieRoleName) ;
      }
      send_integrity_footer_hashes( ) ;
      return (short)(((GRID1_nEOF==0) ? 0 : 2)) ;
   }

   public short subgrid1_previouspage( )
   {
      if ( GRID1_nFirstRecordOnPage >= subgrid1_fnc_recordsperpage( ) )
      {
         GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage-subgrid1_fnc_recordsperpage( )) ;
      }
      else
      {
         return (short)(2) ;
      }
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GRID1_nFirstRecordOnPage", GXutil.ltrim( localUtil.ntoc( GRID1_nFirstRecordOnPage, (byte)(15), (byte)(0), ".", "")));
      if ( isFullAjaxMode( ) )
      {
         gxgrgrid1_refresh( subGrid1_Rows, AV6cIdentificationIssuingCountryName, AV7cPartieID, AV8cPartieName, AV9cPartieShortName, AV14cPartieRoleName) ;
      }
      send_integrity_footer_hashes( ) ;
      return (short)(0) ;
   }

   public short subgrid1_lastpage( )
   {
      GRID1_nRecordCount = subgrid1_fnc_recordcount( ) ;
      if ( GRID1_nRecordCount > subgrid1_fnc_recordsperpage( ) )
      {
         if ( ((int)((GRID1_nRecordCount) % (subgrid1_fnc_recordsperpage( )))) == 0 )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-subgrid1_fnc_recordsperpage( )) ;
         }
         else
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-((int)((GRID1_nRecordCount) % (subgrid1_fnc_recordsperpage( ))))) ;
         }
      }
      else
      {
         GRID1_nFirstRecordOnPage = 0 ;
      }
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GRID1_nFirstRecordOnPage", GXutil.ltrim( localUtil.ntoc( GRID1_nFirstRecordOnPage, (byte)(15), (byte)(0), ".", "")));
      if ( isFullAjaxMode( ) )
      {
         gxgrgrid1_refresh( subGrid1_Rows, AV6cIdentificationIssuingCountryName, AV7cPartieID, AV8cPartieName, AV9cPartieShortName, AV14cPartieRoleName) ;
      }
      send_integrity_footer_hashes( ) ;
      return (short)(0) ;
   }

   public int subgrid1_gotopage( int nPageNo )
   {
      if ( nPageNo > 0 )
      {
         GRID1_nFirstRecordOnPage = (long)(subgrid1_fnc_recordsperpage( )*(nPageNo-1)) ;
      }
      else
      {
         GRID1_nFirstRecordOnPage = 0 ;
      }
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GRID1_nFirstRecordOnPage", GXutil.ltrim( localUtil.ntoc( GRID1_nFirstRecordOnPage, (byte)(15), (byte)(0), ".", "")));
      if ( isFullAjaxMode( ) )
      {
         gxgrgrid1_refresh( subGrid1_Rows, AV6cIdentificationIssuingCountryName, AV7cPartieID, AV8cPartieName, AV9cPartieShortName, AV14cPartieRoleName) ;
      }
      send_integrity_footer_hashes( ) ;
      return 0 ;
   }

   public void before_start_formulas( )
   {
      Gx_err = (short)(0) ;
      fix_multi_value_controls( ) ;
   }

   public void strup080( )
   {
      /* Before Start, stand alone formulas. */
      before_start_formulas( ) ;
      /* Execute Start event if defined. */
      httpContext.wbGlbDoneStart = (byte)(0) ;
      /* Execute user event: Start */
      e17082 ();
      httpContext.wbGlbDoneStart = (byte)(1) ;
      /* After Start, stand alone formulas. */
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         /* Read saved SDTs. */
         /* Read saved values. */
         nRC_GXsfl_64 = (int)(localUtil.ctol( httpContext.cgiGet( "nRC_GXsfl_64"), ",", ".")) ;
         GRID1_nFirstRecordOnPage = localUtil.ctol( httpContext.cgiGet( "GRID1_nFirstRecordOnPage"), ",", ".") ;
         GRID1_nEOF = (byte)(localUtil.ctol( httpContext.cgiGet( "GRID1_nEOF"), ",", ".")) ;
         /* Read variables values. */
         AV6cIdentificationIssuingCountryName = httpContext.cgiGet( edtavCidentificationissuingcountryname_Internalname) ;
         httpContext.ajax_rsp_assign_attri("", false, "AV6cIdentificationIssuingCountryName", AV6cIdentificationIssuingCountryName);
         AV7cPartieID = httpContext.cgiGet( edtavCpartieid_Internalname) ;
         httpContext.ajax_rsp_assign_attri("", false, "AV7cPartieID", AV7cPartieID);
         AV8cPartieName = httpContext.cgiGet( edtavCpartiename_Internalname) ;
         httpContext.ajax_rsp_assign_attri("", false, "AV8cPartieName", AV8cPartieName);
         AV9cPartieShortName = httpContext.cgiGet( edtavCpartieshortname_Internalname) ;
         httpContext.ajax_rsp_assign_attri("", false, "AV9cPartieShortName", AV9cPartieShortName);
         AV14cPartieRoleName = httpContext.cgiGet( edtavCpartierolename_Internalname) ;
         httpContext.ajax_rsp_assign_attri("", false, "AV14cPartieRoleName", AV14cPartieRoleName);
         /* Read subfile selected row values. */
         /* Read hidden variables. */
         GXKey = httpContext.decrypt64( httpContext.getCookie( "GX_SESSION_ID"), context.getServerKey( )) ;
         /* Check if conditions changed and reset current page numbers */
         if ( GXutil.strcmp(httpContext.cgiGet( "GXH_vCIDENTIFICATIONISSUINGCOUNTRYNAME"), AV6cIdentificationIssuingCountryName) != 0 )
         {
            GRID1_nFirstRecordOnPage = 0 ;
         }
         if ( GXutil.strcmp(httpContext.cgiGet( "GXH_vCPARTIEID"), AV7cPartieID) != 0 )
         {
            GRID1_nFirstRecordOnPage = 0 ;
         }
         if ( GXutil.strcmp(httpContext.cgiGet( "GXH_vCPARTIENAME"), AV8cPartieName) != 0 )
         {
            GRID1_nFirstRecordOnPage = 0 ;
         }
         if ( GXutil.strcmp(httpContext.cgiGet( "GXH_vCPARTIESHORTNAME"), AV9cPartieShortName) != 0 )
         {
            GRID1_nFirstRecordOnPage = 0 ;
         }
         if ( GXutil.strcmp(httpContext.cgiGet( "GXH_vCPARTIEROLENAME"), AV14cPartieRoleName) != 0 )
         {
            GRID1_nFirstRecordOnPage = 0 ;
         }
      }
      else
      {
         dynload_actions( ) ;
      }
   }

   protected void GXStart( )
   {
      /* Execute user event: Start */
      e17082 ();
      if (returnInSub) return;
   }

   public void e17082( )
   {
      /* Start Routine */
      returnInSub = false ;
      Form.setCaption( GXutil.format( "Lista de Selecci�n %1", "AEO Master Data", "", "", "", "", "", "", "", "") );
      httpContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.getCaption(), true);
      AV13ADVANCED_LABEL_TEMPLATE = "%1 <strong>%2</strong>" ;
   }

   private void e18082( )
   {
      /* Load Routine */
      returnInSub = false ;
      AV5LinkSelection = context.getHttpContext().getImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.getHttpContext().getTheme( )) ;
      httpContext.ajax_rsp_assign_attri("", false, edtavLinkselection_Internalname, AV5LinkSelection);
      AV17Linkselection_GXI = GXDbFile.pathToUrl( context.getHttpContext().getImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.getHttpContext().getTheme( )), context.getHttpContext()) ;
      sendrow_642( ) ;
      GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1) ;
      if ( isFullAjaxMode( ) && ! bGXsfl_64_Refreshing )
      {
         httpContext.doAjaxLoad(64, Grid1Row);
      }
   }

   public void GXEnter( )
   {
      /* Execute user event: Enter */
      e19082 ();
      if (returnInSub) return;
   }

   public void e19082( )
   {
      /* Enter Routine */
      returnInSub = false ;
      AV11pIdentificationIssuingCountryCode = A1IdentificationIssuingCountryCo ;
      httpContext.ajax_rsp_assign_attri("", false, "AV11pIdentificationIssuingCountryCode", AV11pIdentificationIssuingCountryCode);
      AV12pPartieID = A5PartieID ;
      httpContext.ajax_rsp_assign_attri("", false, "AV12pPartieID", AV12pPartieID);
      httpContext.setWebReturnParms(new Object[] {AV11pIdentificationIssuingCountryCode,AV12pPartieID});
      httpContext.setWebReturnParmsMetadata(new Object[] {"AV11pIdentificationIssuingCountryCode","AV12pPartieID"});
      httpContext.wjLocDisableFrm = (byte)(1) ;
      httpContext.nUserReturn = (byte)(1) ;
      returnInSub = true;
      if (true) return;
      /*  Sending Event outputs  */
   }

   @SuppressWarnings("unchecked")
   public void setparameters( Object[] obj )
   {
      AV11pIdentificationIssuingCountryCode = (String)getParm(obj,0) ;
      httpContext.ajax_rsp_assign_attri("", false, "AV11pIdentificationIssuingCountryCode", AV11pIdentificationIssuingCountryCode);
      AV12pPartieID = (String)getParm(obj,1) ;
      httpContext.ajax_rsp_assign_attri("", false, "AV12pPartieID", AV12pPartieID);
   }

   public String getresponse( String sGXDynURL )
   {
      initialize_properties( ) ;
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      sDynURL = sGXDynURL ;
      nGotPars = 1 ;
      nGXWrapped = 1 ;
      httpContext.setWrapped(true);
      pa082( ) ;
      ws082( ) ;
      we082( ) ;
      if ( isAjaxCallMode( ) )
      {
         cleanup();
      }
      httpContext.setWrapped(false);
      httpContext.GX_msglist = BackMsgLst ;
      String response = "";
      try
      {
         response = ((java.io.ByteArrayOutputStream) httpContext.getOutputStream()).toString("UTF8");
      }
      catch (java.io.UnsupportedEncodingException e)
      {
         Application.printWarning(e.getMessage(), e);
      }
      finally
      {
         httpContext.closeOutputStream();
      }
      return response;
   }

   public void responsestatic( String sGXDynURL )
   {
   }

   public void define_styles( )
   {
      httpContext.AddThemeStyleSheetFile("", context.getHttpContext().getTheme( )+".css", "?"+httpContext.getCacheInvalidationToken( ));
      boolean outputEnabled = httpContext.isOutputEnabled( );
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.enableOutput();
      }
      idxLst = 1 ;
      while ( idxLst <= Form.getJscriptsrc().getCount() )
      {
         httpContext.AddJavascriptSource(GXutil.rtrim( Form.getJscriptsrc().item(idxLst)), "?202232411201559", true, true);
         idxLst = (int)(idxLst+1) ;
      }
      if ( ! outputEnabled )
      {
         if ( httpContext.isSpaRequest( ) )
         {
            httpContext.disableOutput();
         }
      }
      /* End function define_styles */
   }

   public void include_jscripts( )
   {
      httpContext.AddJavascriptSource("messages.spa.js", "?"+httpContext.getCacheInvalidationToken( ), false, true);
      httpContext.AddJavascriptSource("gx0030.js", "?202232411201559", false, true);
      /* End function include_jscripts */
   }

   public void subsflControlProps_642( )
   {
      edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_64_idx ;
      edtIdentificationIssuingCountryNa_Internalname = "IDENTIFICATIONISSUINGCOUNTRYNA_"+sGXsfl_64_idx ;
      edtPartieID_Internalname = "PARTIEID_"+sGXsfl_64_idx ;
      edtPartieRoleName_Internalname = "PARTIEROLENAME_"+sGXsfl_64_idx ;
      dynIdentificationIssuingCountryCo.setInternalname( "IDENTIFICATIONISSUINGCOUNTRYCO_"+sGXsfl_64_idx );
   }

   public void subsflControlProps_fel_642( )
   {
      edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_64_fel_idx ;
      edtIdentificationIssuingCountryNa_Internalname = "IDENTIFICATIONISSUINGCOUNTRYNA_"+sGXsfl_64_fel_idx ;
      edtPartieID_Internalname = "PARTIEID_"+sGXsfl_64_fel_idx ;
      edtPartieRoleName_Internalname = "PARTIEROLENAME_"+sGXsfl_64_fel_idx ;
      dynIdentificationIssuingCountryCo.setInternalname( "IDENTIFICATIONISSUINGCOUNTRYCO_"+sGXsfl_64_fel_idx );
   }

   public void sendrow_642( )
   {
      subsflControlProps_642( ) ;
      wb080( ) ;
      if ( ( 10 * 1 == 0 ) || ( nGXsfl_64_idx <= subgrid1_fnc_recordsperpage( ) * 1 ) )
      {
         Grid1Row = GXWebRow.GetNew(context,Grid1Container) ;
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = (byte)(0) ;
            if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd" ;
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = (byte)(0) ;
            subGrid1_Backcolor = subGrid1_Allbackcolor ;
            if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform" ;
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = (byte)(1) ;
            if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd" ;
            }
            subGrid1_Backcolor = (int)(0x0) ;
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = (byte)(1) ;
            if ( ((int)((nGXsfl_64_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0x0) ;
               if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even" ;
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0x0) ;
               if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd" ;
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<tr ") ;
            httpContext.writeText( " class=\""+"PromptGrid"+"\" style=\""+""+"\"") ;
            httpContext.writeText( " gxrow=\""+sGXsfl_64_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Static Bitmap Variable */
         edtavLinkselection_Link = "javascript:gx.popup.gxReturn(["+"'"+PrivateUtilities.encodeJSConstant( GXutil.rtrim( A1IdentificationIssuingCountryCo))+"'"+", "+"'"+PrivateUtilities.encodeJSConstant( GXutil.rtrim( A5PartieID))+"'"+"]);" ;
         httpContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "Link", edtavLinkselection_Link, !bGXsfl_64_Refreshing);
         ClassString = "SelectionAttribute" ;
         StyleString = "" ;
         AV5LinkSelection_IsBlob = (boolean)(((GXutil.strcmp("", AV5LinkSelection)==0)&&(GXutil.strcmp("", AV17Linkselection_GXI)==0))||!(GXutil.strcmp("", AV5LinkSelection)==0)) ;
         sImgUrl = ((GXutil.strcmp("", AV5LinkSelection)==0) ? AV17Linkselection_GXI : httpContext.getResourceRelative(AV5LinkSelection)) ;
         Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {edtavLinkselection_Internalname,sImgUrl,edtavLinkselection_Link,"","",context.getHttpContext().getTheme( ),Integer.valueOf(-1),Integer.valueOf(1),"","",Integer.valueOf(1),Integer.valueOf(-1),Integer.valueOf(0),"px",Integer.valueOf(0),"px",Integer.valueOf(0),Integer.valueOf(0),Integer.valueOf(0),"","",StyleString,ClassString,"WWActionColumn","","","","","","",Integer.valueOf(1),Boolean.valueOf(AV5LinkSelection_IsBlob),Boolean.valueOf(false),context.getHttpContext().getImageSrcSet( sImgUrl)});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "DescriptionAttribute" ;
         edtIdentificationIssuingCountryNa_Link = "javascript:gx.popup.gxReturn(["+"'"+PrivateUtilities.encodeJSConstant( GXutil.rtrim( A1IdentificationIssuingCountryCo))+"'"+", "+"'"+PrivateUtilities.encodeJSConstant( GXutil.rtrim( A5PartieID))+"'"+"]);" ;
         httpContext.ajax_rsp_assign_prop("", false, edtIdentificationIssuingCountryNa_Internalname, "Link", edtIdentificationIssuingCountryNa_Link, !bGXsfl_64_Refreshing);
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {edtIdentificationIssuingCountryNa_Internalname,A4IdentificationIssuingCountryNa,"","","'"+""+"'"+",false,"+"'"+""+"'",edtIdentificationIssuingCountryNa_Link,"","Identification Issuing Country Name","",edtIdentificationIssuingCountryNa_Jsonclick,Integer.valueOf(0),"DescriptionAttribute","",ROClassString,"WWColumn","",Integer.valueOf(-1),Integer.valueOf(0),Integer.valueOf(0),"text","",Integer.valueOf(0),"px",Integer.valueOf(17),"px",Integer.valueOf(50),Integer.valueOf(0),Integer.valueOf(0),Integer.valueOf(64),Integer.valueOf(1),Integer.valueOf(-1),Integer.valueOf(-1),Boolean.valueOf(true),"DMPaisNombre","left",Boolean.valueOf(true),""});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute" ;
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {edtPartieID_Internalname,GXutil.rtrim( A5PartieID),"","","'"+""+"'"+",false,"+"'"+""+"'","","","","",edtPartieID_Jsonclick,Integer.valueOf(0),"Attribute","",ROClassString,"WWColumn","",Integer.valueOf(-1),Integer.valueOf(0),Integer.valueOf(0),"text","",Integer.valueOf(0),"px",Integer.valueOf(17),"px",Integer.valueOf(30),Integer.valueOf(0),Integer.valueOf(0),Integer.valueOf(64),Integer.valueOf(1),Integer.valueOf(-1),Integer.valueOf(-1),Boolean.valueOf(true),"AEOPartieID","left",Boolean.valueOf(true),""});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute" ;
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {edtPartieRoleName_Internalname,A9PartieRoleName,"","","'"+""+"'"+",false,"+"'"+""+"'","","","","",edtPartieRoleName_Jsonclick,Integer.valueOf(0),"Attribute","",ROClassString,"WWColumn OptionalColumn","",Integer.valueOf(-1),Integer.valueOf(0),Integer.valueOf(0),"text","",Integer.valueOf(0),"px",Integer.valueOf(17),"px",Integer.valueOf(30),Integer.valueOf(0),Integer.valueOf(0),Integer.valueOf(64),Integer.valueOf(1),Integer.valueOf(-1),Integer.valueOf(-1),Boolean.valueOf(true),"AEOPartieRoleName","left",Boolean.valueOf(true),""});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         if ( ( dynIdentificationIssuingCountryCo.getItemCount() == 0 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "IDENTIFICATIONISSUINGCOUNTRYCO_" + sGXsfl_64_idx ;
            dynIdentificationIssuingCountryCo.setName( GXCCtl );
            dynIdentificationIssuingCountryCo.setWebtags( "" );
            dynIdentificationIssuingCountryCo.removeAllItems();
            /* Using cursor H00084 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               dynIdentificationIssuingCountryCo.addItem(H00084_A2CountryCode[0], H00084_A3CountryName[0], (short)(0));
               pr_default.readNext(2);
            }
            pr_default.close(2);
            if ( dynIdentificationIssuingCountryCo.getItemCount() > 0 )
            {
               A1IdentificationIssuingCountryCo = dynIdentificationIssuingCountryCo.getValidValue(A1IdentificationIssuingCountryCo) ;
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {dynIdentificationIssuingCountryCo,dynIdentificationIssuingCountryCo.getInternalname(),GXutil.rtrim( A1IdentificationIssuingCountryCo),Integer.valueOf(1),dynIdentificationIssuingCountryCo.getJsonclick(),Integer.valueOf(0),"'"+""+"'"+",false,"+"'"+""+"'","char","Identification Issuing Country Code",Integer.valueOf(0),Integer.valueOf(0),Integer.valueOf(0),Integer.valueOf(0),Integer.valueOf(0),"px",Integer.valueOf(0),"px","","Attribute","","","","",Boolean.valueOf(true),Integer.valueOf(1)});
         dynIdentificationIssuingCountryCo.setValue( GXutil.rtrim( A1IdentificationIssuingCountryCo) );
         httpContext.ajax_rsp_assign_prop("", false, dynIdentificationIssuingCountryCo.getInternalname(), "Values", dynIdentificationIssuingCountryCo.ToJavascriptSource(), !bGXsfl_64_Refreshing);
         send_integrity_lvl_hashes082( ) ;
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_64_idx = ((subGrid1_Islastpage==1)&&(nGXsfl_64_idx+1>subgrid1_fnc_recordsperpage( )) ? 1 : nGXsfl_64_idx+1) ;
         sGXsfl_64_idx = GXutil.padl( GXutil.ltrimstr( DecimalUtil.doubleToDec(nGXsfl_64_idx), 4, 0), (short)(4), "0") ;
         subsflControlProps_642( ) ;
      }
      /* End function sendrow_642 */
   }

   public void init_default_properties( )
   {
      lblLblidentificationissuingcountrynamefilter_Internalname = "LBLIDENTIFICATIONISSUINGCOUNTRYNAMEFILTER" ;
      edtavCidentificationissuingcountryname_Internalname = "vCIDENTIFICATIONISSUINGCOUNTRYNAME" ;
      divIdentificationissuingcountrynamefiltercontainer_Internalname = "IDENTIFICATIONISSUINGCOUNTRYNAMEFILTERCONTAINER" ;
      lblLblpartieidfilter_Internalname = "LBLPARTIEIDFILTER" ;
      edtavCpartieid_Internalname = "vCPARTIEID" ;
      divPartieidfiltercontainer_Internalname = "PARTIEIDFILTERCONTAINER" ;
      lblLblpartienamefilter_Internalname = "LBLPARTIENAMEFILTER" ;
      edtavCpartiename_Internalname = "vCPARTIENAME" ;
      divPartienamefiltercontainer_Internalname = "PARTIENAMEFILTERCONTAINER" ;
      lblLblpartieshortnamefilter_Internalname = "LBLPARTIESHORTNAMEFILTER" ;
      edtavCpartieshortname_Internalname = "vCPARTIESHORTNAME" ;
      divPartieshortnamefiltercontainer_Internalname = "PARTIESHORTNAMEFILTERCONTAINER" ;
      lblLblpartierolenamefilter_Internalname = "LBLPARTIEROLENAMEFILTER" ;
      edtavCpartierolename_Internalname = "vCPARTIEROLENAME" ;
      divPartierolenamefiltercontainer_Internalname = "PARTIEROLENAMEFILTERCONTAINER" ;
      divAdvancedcontainer_Internalname = "ADVANCEDCONTAINER" ;
      bttBtntoggle_Internalname = "BTNTOGGLE" ;
      edtavLinkselection_Internalname = "vLINKSELECTION" ;
      edtIdentificationIssuingCountryNa_Internalname = "IDENTIFICATIONISSUINGCOUNTRYNA" ;
      edtPartieID_Internalname = "PARTIEID" ;
      edtPartieRoleName_Internalname = "PARTIEROLENAME" ;
      dynIdentificationIssuingCountryCo.setInternalname( "IDENTIFICATIONISSUINGCOUNTRYCO" );
      bttBtn_cancel_Internalname = "BTN_CANCEL" ;
      divGridtable_Internalname = "GRIDTABLE" ;
      divMain_Internalname = "MAIN" ;
      Form.setInternalname( "FORM" );
      subGrid1_Internalname = "GRID1" ;
   }

   public void initialize_properties( )
   {
      httpContext.setAjaxOnSessionTimeout(ajaxOnSessionTimeout());
      httpContext.setDefaultTheme("Carmine");
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.disableJsOutput();
      }
      init_default_properties( ) ;
      dynIdentificationIssuingCountryCo.setJsonclick( "" );
      edtPartieRoleName_Jsonclick = "" ;
      edtPartieID_Jsonclick = "" ;
      edtIdentificationIssuingCountryNa_Jsonclick = "" ;
      subGrid1_Allowcollapsing = (byte)(0) ;
      subGrid1_Allowselection = (byte)(0) ;
      edtIdentificationIssuingCountryNa_Link = "" ;
      edtavLinkselection_Link = "" ;
      subGrid1_Header = "" ;
      subGrid1_Class = "PromptGrid" ;
      subGrid1_Backcolorstyle = (byte)(0) ;
      edtavCpartierolename_Jsonclick = "" ;
      edtavCpartierolename_Enabled = 1 ;
      edtavCpartierolename_Visible = 1 ;
      edtavCpartieshortname_Jsonclick = "" ;
      edtavCpartieshortname_Enabled = 1 ;
      edtavCpartieshortname_Visible = 1 ;
      edtavCpartiename_Jsonclick = "" ;
      edtavCpartiename_Enabled = 1 ;
      edtavCpartiename_Visible = 1 ;
      edtavCpartieid_Jsonclick = "" ;
      edtavCpartieid_Enabled = 1 ;
      edtavCpartieid_Visible = 1 ;
      edtavCidentificationissuingcountryname_Jsonclick = "" ;
      edtavCidentificationissuingcountryname_Enabled = 1 ;
      edtavCidentificationissuingcountryname_Visible = 1 ;
      Form.setHeaderrawhtml( "" );
      Form.setBackground( "" );
      Form.setTextcolor( 0 );
      Form.setIBackground( (int)(0xFFFFFF) );
      Form.setCaption( "Selection List AEO Master Data" );
      divPartierolenamefiltercontainer_Class = "AdvancedContainerItem" ;
      divPartieshortnamefiltercontainer_Class = "AdvancedContainerItem" ;
      divPartienamefiltercontainer_Class = "AdvancedContainerItem" ;
      divPartieidfiltercontainer_Class = "AdvancedContainerItem" ;
      divIdentificationissuingcountrynamefiltercontainer_Class = "AdvancedContainerItem" ;
      bttBtntoggle_Class = "BtnToggle" ;
      divAdvancedcontainer_Class = "AdvancedContainerVisible" ;
      subGrid1_Rows = 10 ;
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.enableJsOutput();
      }
   }

   public void init_web_controls( )
   {
      GXCCtl = "IDENTIFICATIONISSUINGCOUNTRYCO_" + sGXsfl_64_idx ;
      dynIdentificationIssuingCountryCo.setName( GXCCtl );
      dynIdentificationIssuingCountryCo.setWebtags( "" );
      dynIdentificationIssuingCountryCo.removeAllItems();
      /* Using cursor H00085 */
      pr_default.execute(3);
      while ( (pr_default.getStatus(3) != 101) )
      {
         dynIdentificationIssuingCountryCo.addItem(H00085_A2CountryCode[0], H00085_A3CountryName[0], (short)(0));
         pr_default.readNext(3);
      }
      pr_default.close(3);
      if ( dynIdentificationIssuingCountryCo.getItemCount() > 0 )
      {
         A1IdentificationIssuingCountryCo = dynIdentificationIssuingCountryCo.getValidValue(A1IdentificationIssuingCountryCo) ;
      }
      /* End function init_web_controls */
   }

   public boolean supportAjaxEvent( )
   {
      return true ;
   }

   public void initializeDynEvents( )
   {
      setEventMetadata("REFRESH","{handler:'refresh',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cIdentificationIssuingCountryName',fld:'vCIDENTIFICATIONISSUINGCOUNTRYNAME',pic:''},{av:'AV7cPartieID',fld:'vCPARTIEID',pic:''},{av:'AV8cPartieName',fld:'vCPARTIENAME',pic:''},{av:'AV9cPartieShortName',fld:'vCPARTIESHORTNAME',pic:''},{av:'AV14cPartieRoleName',fld:'vCPARTIEROLENAME',pic:''}]");
      setEventMetadata("REFRESH",",oparms:[]}");
      setEventMetadata("'TOGGLE'","{handler:'e16081',iparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]");
      setEventMetadata("'TOGGLE'",",oparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]}");
      setEventMetadata("LBLIDENTIFICATIONISSUINGCOUNTRYNAMEFILTER.CLICK","{handler:'e11081',iparms:[{av:'divIdentificationissuingcountrynamefiltercontainer_Class',ctrl:'IDENTIFICATIONISSUINGCOUNTRYNAMEFILTERCONTAINER',prop:'Class'}]");
      setEventMetadata("LBLIDENTIFICATIONISSUINGCOUNTRYNAMEFILTER.CLICK",",oparms:[{av:'divIdentificationissuingcountrynamefiltercontainer_Class',ctrl:'IDENTIFICATIONISSUINGCOUNTRYNAMEFILTERCONTAINER',prop:'Class'},{av:'edtavCidentificationissuingcountryname_Visible',ctrl:'vCIDENTIFICATIONISSUINGCOUNTRYNAME',prop:'Visible'}]}");
      setEventMetadata("LBLPARTIEIDFILTER.CLICK","{handler:'e12081',iparms:[{av:'divPartieidfiltercontainer_Class',ctrl:'PARTIEIDFILTERCONTAINER',prop:'Class'}]");
      setEventMetadata("LBLPARTIEIDFILTER.CLICK",",oparms:[{av:'divPartieidfiltercontainer_Class',ctrl:'PARTIEIDFILTERCONTAINER',prop:'Class'},{av:'edtavCpartieid_Visible',ctrl:'vCPARTIEID',prop:'Visible'}]}");
      setEventMetadata("LBLPARTIENAMEFILTER.CLICK","{handler:'e13081',iparms:[{av:'divPartienamefiltercontainer_Class',ctrl:'PARTIENAMEFILTERCONTAINER',prop:'Class'}]");
      setEventMetadata("LBLPARTIENAMEFILTER.CLICK",",oparms:[{av:'divPartienamefiltercontainer_Class',ctrl:'PARTIENAMEFILTERCONTAINER',prop:'Class'},{av:'edtavCpartiename_Visible',ctrl:'vCPARTIENAME',prop:'Visible'}]}");
      setEventMetadata("LBLPARTIESHORTNAMEFILTER.CLICK","{handler:'e14081',iparms:[{av:'divPartieshortnamefiltercontainer_Class',ctrl:'PARTIESHORTNAMEFILTERCONTAINER',prop:'Class'}]");
      setEventMetadata("LBLPARTIESHORTNAMEFILTER.CLICK",",oparms:[{av:'divPartieshortnamefiltercontainer_Class',ctrl:'PARTIESHORTNAMEFILTERCONTAINER',prop:'Class'},{av:'edtavCpartieshortname_Visible',ctrl:'vCPARTIESHORTNAME',prop:'Visible'}]}");
      setEventMetadata("LBLPARTIEROLENAMEFILTER.CLICK","{handler:'e15081',iparms:[{av:'divPartierolenamefiltercontainer_Class',ctrl:'PARTIEROLENAMEFILTERCONTAINER',prop:'Class'}]");
      setEventMetadata("LBLPARTIEROLENAMEFILTER.CLICK",",oparms:[{av:'divPartierolenamefiltercontainer_Class',ctrl:'PARTIEROLENAMEFILTERCONTAINER',prop:'Class'},{av:'edtavCpartierolename_Visible',ctrl:'vCPARTIEROLENAME',prop:'Visible'}]}");
      setEventMetadata("ENTER","{handler:'e19082',iparms:[{av:'dynIdentificationIssuingCountryCo'},{av:'A1IdentificationIssuingCountryCo',fld:'IDENTIFICATIONISSUINGCOUNTRYCO',pic:'',hsh:true},{av:'A5PartieID',fld:'PARTIEID',pic:'',hsh:true}]");
      setEventMetadata("ENTER",",oparms:[{av:'AV11pIdentificationIssuingCountryCode',fld:'vPIDENTIFICATIONISSUINGCOUNTRYCODE',pic:''},{av:'AV12pPartieID',fld:'vPPARTIEID',pic:''}]}");
      setEventMetadata("GRID1_FIRSTPAGE","{handler:'subgrid1_firstpage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cIdentificationIssuingCountryName',fld:'vCIDENTIFICATIONISSUINGCOUNTRYNAME',pic:''},{av:'AV7cPartieID',fld:'vCPARTIEID',pic:''},{av:'AV8cPartieName',fld:'vCPARTIENAME',pic:''},{av:'AV9cPartieShortName',fld:'vCPARTIESHORTNAME',pic:''},{av:'AV14cPartieRoleName',fld:'vCPARTIEROLENAME',pic:''}]");
      setEventMetadata("GRID1_FIRSTPAGE",",oparms:[]}");
      setEventMetadata("GRID1_PREVPAGE","{handler:'subgrid1_previouspage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cIdentificationIssuingCountryName',fld:'vCIDENTIFICATIONISSUINGCOUNTRYNAME',pic:''},{av:'AV7cPartieID',fld:'vCPARTIEID',pic:''},{av:'AV8cPartieName',fld:'vCPARTIENAME',pic:''},{av:'AV9cPartieShortName',fld:'vCPARTIESHORTNAME',pic:''},{av:'AV14cPartieRoleName',fld:'vCPARTIEROLENAME',pic:''}]");
      setEventMetadata("GRID1_PREVPAGE",",oparms:[]}");
      setEventMetadata("GRID1_NEXTPAGE","{handler:'subgrid1_nextpage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cIdentificationIssuingCountryName',fld:'vCIDENTIFICATIONISSUINGCOUNTRYNAME',pic:''},{av:'AV7cPartieID',fld:'vCPARTIEID',pic:''},{av:'AV8cPartieName',fld:'vCPARTIENAME',pic:''},{av:'AV9cPartieShortName',fld:'vCPARTIESHORTNAME',pic:''},{av:'AV14cPartieRoleName',fld:'vCPARTIEROLENAME',pic:''}]");
      setEventMetadata("GRID1_NEXTPAGE",",oparms:[]}");
      setEventMetadata("GRID1_LASTPAGE","{handler:'subgrid1_lastpage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cIdentificationIssuingCountryName',fld:'vCIDENTIFICATIONISSUINGCOUNTRYNAME',pic:''},{av:'AV7cPartieID',fld:'vCPARTIEID',pic:''},{av:'AV8cPartieName',fld:'vCPARTIENAME',pic:''},{av:'AV9cPartieShortName',fld:'vCPARTIESHORTNAME',pic:''},{av:'AV14cPartieRoleName',fld:'vCPARTIEROLENAME',pic:''}]");
      setEventMetadata("GRID1_LASTPAGE",",oparms:[]}");
      setEventMetadata("VALID_IDENTIFICATIONISSUINGCOUNTRYCO","{handler:'valid_Identificationissuingcountryco',iparms:[]");
      setEventMetadata("VALID_IDENTIFICATIONISSUINGCOUNTRYCO",",oparms:[]}");
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

   protected String IntegratedSecurityPermissionPrefix( )
   {
      return "";
   }

   protected String EncryptURLParameters( )
   {
      return "NO";
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      gxfirstwebparm = "" ;
      gxfirstwebparm_bkp = "" ;
      AV6cIdentificationIssuingCountryName = "" ;
      AV7cPartieID = "" ;
      AV8cPartieName = "" ;
      AV9cPartieShortName = "" ;
      AV14cPartieRoleName = "" ;
      AV11pIdentificationIssuingCountryCode = "" ;
      AV12pPartieID = "" ;
      Form = new com.genexus.webpanels.GXWebForm();
      sDynURL = "" ;
      FormProcess = "" ;
      bodyStyle = "" ;
      GXKey = "" ;
      GX_FocusControl = "" ;
      sPrefix = "" ;
      lblLblidentificationissuingcountrynamefilter_Jsonclick = "" ;
      TempTags = "" ;
      lblLblpartieidfilter_Jsonclick = "" ;
      lblLblpartienamefilter_Jsonclick = "" ;
      lblLblpartieshortnamefilter_Jsonclick = "" ;
      lblLblpartierolenamefilter_Jsonclick = "" ;
      ClassString = "" ;
      StyleString = "" ;
      bttBtntoggle_Jsonclick = "" ;
      Grid1Container = new com.genexus.webpanels.GXWebGrid(context);
      sStyleString = "" ;
      subGrid1_Linesclass = "" ;
      Grid1Column = new com.genexus.webpanels.GXWebColumn();
      AV5LinkSelection = "" ;
      A4IdentificationIssuingCountryNa = "" ;
      A5PartieID = "" ;
      A9PartieRoleName = "" ;
      A1IdentificationIssuingCountryCo = "" ;
      bttBtn_cancel_Jsonclick = "" ;
      sEvt = "" ;
      EvtGridId = "" ;
      EvtRowId = "" ;
      sEvtType = "" ;
      AV17Linkselection_GXI = "" ;
      scmdbuf = "" ;
      lV7cPartieID = "" ;
      lV6cIdentificationIssuingCountryName = "" ;
      lV8cPartieName = "" ;
      lV9cPartieShortName = "" ;
      lV14cPartieRoleName = "" ;
      A6PartieName = "" ;
      A7PartieShortName = "" ;
      H00082_A8PartieRoleCode = new String[] {""} ;
      H00082_A7PartieShortName = new String[] {""} ;
      H00082_A6PartieName = new String[] {""} ;
      H00082_A1IdentificationIssuingCountryCo = new String[] {""} ;
      H00082_A9PartieRoleName = new String[] {""} ;
      H00082_A5PartieID = new String[] {""} ;
      H00082_A4IdentificationIssuingCountryNa = new String[] {""} ;
      A8PartieRoleCode = "" ;
      H00083_AGRID1_nRecordCount = new long[1] ;
      AV13ADVANCED_LABEL_TEMPLATE = "" ;
      Grid1Row = new com.genexus.webpanels.GXWebRow();
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      sImgUrl = "" ;
      ROClassString = "" ;
      GXCCtl = "" ;
      H00084_A2CountryCode = new String[] {""} ;
      H00084_A3CountryName = new String[] {""} ;
      H00085_A2CountryCode = new String[] {""} ;
      H00085_A3CountryName = new String[] {""} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new com.oea5pruebadeconcepto.gx0030__default(),
         new Object[] {
             new Object[] {
            H00082_A8PartieRoleCode, H00082_A7PartieShortName, H00082_A6PartieName, H00082_A1IdentificationIssuingCountryCo, H00082_A9PartieRoleName, H00082_A5PartieID, H00082_A4IdentificationIssuingCountryNa
            }
            , new Object[] {
            H00083_AGRID1_nRecordCount
            }
            , new Object[] {
            H00084_A2CountryCode, H00084_A3CountryName
            }
            , new Object[] {
            H00085_A2CountryCode, H00085_A3CountryName
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte GRID1_nEOF ;
   private byte nGotPars ;
   private byte GxWebError ;
   private byte gxajaxcallmode ;
   private byte subGrid1_Backcolorstyle ;
   private byte subGrid1_Titlebackstyle ;
   private byte subGrid1_Allowselection ;
   private byte subGrid1_Allowhovering ;
   private byte subGrid1_Allowcollapsing ;
   private byte subGrid1_Collapsed ;
   private byte nDonePA ;
   private byte nGXWrapped ;
   private byte subGrid1_Backstyle ;
   private short wbEnd ;
   private short wbStart ;
   private short gxcookieaux ;
   private short Gx_err ;
   private int nRC_GXsfl_64 ;
   private int nGXsfl_64_idx=1 ;
   private int subGrid1_Rows ;
   private int edtavCidentificationissuingcountryname_Visible ;
   private int edtavCidentificationissuingcountryname_Enabled ;
   private int edtavCpartieid_Visible ;
   private int edtavCpartieid_Enabled ;
   private int edtavCpartiename_Visible ;
   private int edtavCpartiename_Enabled ;
   private int edtavCpartieshortname_Visible ;
   private int edtavCpartieshortname_Enabled ;
   private int edtavCpartierolename_Visible ;
   private int edtavCpartierolename_Enabled ;
   private int subGrid1_Titlebackcolor ;
   private int subGrid1_Allbackcolor ;
   private int subGrid1_Selectedindex ;
   private int subGrid1_Selectioncolor ;
   private int subGrid1_Hoveringcolor ;
   private int subGrid1_Islastpage ;
   private int GXPagingFrom2 ;
   private int GXPagingTo2 ;
   private int idxLst ;
   private int subGrid1_Backcolor ;
   private long GRID1_nFirstRecordOnPage ;
   private long GRID1_nCurrentRecord ;
   private long GRID1_nRecordCount ;
   private String divAdvancedcontainer_Class ;
   private String bttBtntoggle_Class ;
   private String divIdentificationissuingcountrynamefiltercontainer_Class ;
   private String divPartieidfiltercontainer_Class ;
   private String divPartienamefiltercontainer_Class ;
   private String divPartieshortnamefiltercontainer_Class ;
   private String divPartierolenamefiltercontainer_Class ;
   private String gxfirstwebparm ;
   private String gxfirstwebparm_bkp ;
   private String sGXsfl_64_idx="0001" ;
   private String AV7cPartieID ;
   private String AV11pIdentificationIssuingCountryCode ;
   private String AV12pPartieID ;
   private String sDynURL ;
   private String FormProcess ;
   private String bodyStyle ;
   private String GXKey ;
   private String GX_FocusControl ;
   private String sPrefix ;
   private String divMain_Internalname ;
   private String divAdvancedcontainer_Internalname ;
   private String divIdentificationissuingcountrynamefiltercontainer_Internalname ;
   private String lblLblidentificationissuingcountrynamefilter_Internalname ;
   private String lblLblidentificationissuingcountrynamefilter_Jsonclick ;
   private String edtavCidentificationissuingcountryname_Internalname ;
   private String TempTags ;
   private String edtavCidentificationissuingcountryname_Jsonclick ;
   private String divPartieidfiltercontainer_Internalname ;
   private String lblLblpartieidfilter_Internalname ;
   private String lblLblpartieidfilter_Jsonclick ;
   private String edtavCpartieid_Internalname ;
   private String edtavCpartieid_Jsonclick ;
   private String divPartienamefiltercontainer_Internalname ;
   private String lblLblpartienamefilter_Internalname ;
   private String lblLblpartienamefilter_Jsonclick ;
   private String edtavCpartiename_Internalname ;
   private String edtavCpartiename_Jsonclick ;
   private String divPartieshortnamefiltercontainer_Internalname ;
   private String lblLblpartieshortnamefilter_Internalname ;
   private String lblLblpartieshortnamefilter_Jsonclick ;
   private String edtavCpartieshortname_Internalname ;
   private String edtavCpartieshortname_Jsonclick ;
   private String divPartierolenamefiltercontainer_Internalname ;
   private String lblLblpartierolenamefilter_Internalname ;
   private String lblLblpartierolenamefilter_Jsonclick ;
   private String edtavCpartierolename_Internalname ;
   private String edtavCpartierolename_Jsonclick ;
   private String divGridtable_Internalname ;
   private String ClassString ;
   private String StyleString ;
   private String bttBtntoggle_Internalname ;
   private String bttBtntoggle_Jsonclick ;
   private String sStyleString ;
   private String subGrid1_Internalname ;
   private String subGrid1_Class ;
   private String subGrid1_Linesclass ;
   private String subGrid1_Header ;
   private String edtavLinkselection_Link ;
   private String edtIdentificationIssuingCountryNa_Link ;
   private String A5PartieID ;
   private String A1IdentificationIssuingCountryCo ;
   private String bttBtn_cancel_Internalname ;
   private String bttBtn_cancel_Jsonclick ;
   private String sEvt ;
   private String EvtGridId ;
   private String EvtRowId ;
   private String sEvtType ;
   private String edtavLinkselection_Internalname ;
   private String edtIdentificationIssuingCountryNa_Internalname ;
   private String edtPartieID_Internalname ;
   private String edtPartieRoleName_Internalname ;
   private String scmdbuf ;
   private String lV7cPartieID ;
   private String A8PartieRoleCode ;
   private String AV13ADVANCED_LABEL_TEMPLATE ;
   private String sGXsfl_64_fel_idx="0001" ;
   private String sImgUrl ;
   private String ROClassString ;
   private String edtIdentificationIssuingCountryNa_Jsonclick ;
   private String edtPartieID_Jsonclick ;
   private String edtPartieRoleName_Jsonclick ;
   private String GXCCtl ;
   private boolean entryPointCalled ;
   private boolean toggleJsOutput ;
   private boolean wbLoad ;
   private boolean Rfr0gs ;
   private boolean wbErr ;
   private boolean bGXsfl_64_Refreshing=false ;
   private boolean gxdyncontrolsrefreshing ;
   private boolean returnInSub ;
   private boolean AV5LinkSelection_IsBlob ;
   private String AV6cIdentificationIssuingCountryName ;
   private String AV8cPartieName ;
   private String AV9cPartieShortName ;
   private String AV14cPartieRoleName ;
   private String A4IdentificationIssuingCountryNa ;
   private String A9PartieRoleName ;
   private String AV17Linkselection_GXI ;
   private String lV6cIdentificationIssuingCountryName ;
   private String lV8cPartieName ;
   private String lV9cPartieShortName ;
   private String lV14cPartieRoleName ;
   private String A6PartieName ;
   private String A7PartieShortName ;
   private String AV5LinkSelection ;
   private com.genexus.webpanels.GXWebGrid Grid1Container ;
   private com.genexus.webpanels.GXWebRow Grid1Row ;
   private com.genexus.webpanels.GXWebColumn Grid1Column ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private HTMLChoice dynIdentificationIssuingCountryCo ;
   private IDataStoreProvider pr_default ;
   private String[] H00082_A8PartieRoleCode ;
   private String[] H00082_A7PartieShortName ;
   private String[] H00082_A6PartieName ;
   private String[] H00082_A1IdentificationIssuingCountryCo ;
   private String[] H00082_A9PartieRoleName ;
   private String[] H00082_A5PartieID ;
   private String[] H00082_A4IdentificationIssuingCountryNa ;
   private long[] H00083_AGRID1_nRecordCount ;
   private String[] H00084_A2CountryCode ;
   private String[] H00084_A3CountryName ;
   private String[] H00085_A2CountryCode ;
   private String[] H00085_A3CountryName ;
   private com.genexus.webpanels.GXWebForm Form ;
}

final  class gx0030__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   protected Object[] conditional_H00082( ModelContext context ,
                                          int remoteHandle ,
                                          com.genexus.IHttpContext httpContext ,
                                          String AV6cIdentificationIssuingCountryName ,
                                          String AV8cPartieName ,
                                          String AV9cPartieShortName ,
                                          String AV14cPartieRoleName ,
                                          String A4IdentificationIssuingCountryNa ,
                                          String A6PartieName ,
                                          String A7PartieShortName ,
                                          String A9PartieRoleName ,
                                          String A5PartieID ,
                                          String AV7cPartieID )
   {
      java.lang.StringBuffer sWhereString = new java.lang.StringBuffer();
      String scmdbuf;
      byte[] GXv_int1 = new byte[7];
      Object[] GXv_Object2 = new Object[2];
      String sSelectString;
      String sFromString;
      String sOrderString;
      sSelectString = " T1.`PartieRoleCode`, T1.`PartieShortName`, T1.`PartieName`, T1.`IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo, T2.`PartieRoleName`, T1.`PartieID`," ;
      sSelectString += " T3.`CountryName` AS IdentificationIssuingCountryNa" ;
      sFromString = " FROM ((`AEOMasterData` T1 INNER JOIN `PartieRole` T2 ON T2.`PartieRoleCode` = T1.`PartieRoleCode`) INNER JOIN `Country` T3 ON T3.`CountryCode` = T1.`IdentificationIssuingCountryCo`)" ;
      sOrderString = "" ;
      addWhere(sWhereString, "(T1.`PartieID` like ?)");
      if ( ! (GXutil.strcmp("", AV6cIdentificationIssuingCountryName)==0) )
      {
         addWhere(sWhereString, "(T3.`CountryName` like ?)");
      }
      else
      {
         GXv_int1[1] = (byte)(1) ;
      }
      if ( ! (GXutil.strcmp("", AV8cPartieName)==0) )
      {
         addWhere(sWhereString, "(T1.`PartieName` like ?)");
      }
      else
      {
         GXv_int1[2] = (byte)(1) ;
      }
      if ( ! (GXutil.strcmp("", AV9cPartieShortName)==0) )
      {
         addWhere(sWhereString, "(T1.`PartieShortName` like ?)");
      }
      else
      {
         GXv_int1[3] = (byte)(1) ;
      }
      if ( ! (GXutil.strcmp("", AV14cPartieRoleName)==0) )
      {
         addWhere(sWhereString, "(T2.`PartieRoleName` like ?)");
      }
      else
      {
         GXv_int1[4] = (byte)(1) ;
      }
      sOrderString += " ORDER BY T1.`IdentificationIssuingCountryCo`, T1.`PartieID`" ;
      scmdbuf = "SELECT " + sSelectString + sFromString + sWhereString + sOrderString + "" + " LIMIT " + "?" + ", " + "?" ;
      GXv_Object2[0] = scmdbuf ;
      GXv_Object2[1] = GXv_int1 ;
      return GXv_Object2 ;
   }

   protected Object[] conditional_H00083( ModelContext context ,
                                          int remoteHandle ,
                                          com.genexus.IHttpContext httpContext ,
                                          String AV6cIdentificationIssuingCountryName ,
                                          String AV8cPartieName ,
                                          String AV9cPartieShortName ,
                                          String AV14cPartieRoleName ,
                                          String A4IdentificationIssuingCountryNa ,
                                          String A6PartieName ,
                                          String A7PartieShortName ,
                                          String A9PartieRoleName ,
                                          String A5PartieID ,
                                          String AV7cPartieID )
   {
      java.lang.StringBuffer sWhereString = new java.lang.StringBuffer();
      String scmdbuf;
      byte[] GXv_int3 = new byte[5];
      Object[] GXv_Object4 = new Object[2];
      scmdbuf = "SELECT COUNT(*) FROM ((`AEOMasterData` T1 INNER JOIN `PartieRole` T2 ON T2.`PartieRoleCode` = T1.`PartieRoleCode`) INNER JOIN `Country` T3 ON T3.`CountryCode` =" ;
      scmdbuf += " T1.`IdentificationIssuingCountryCo`)" ;
      addWhere(sWhereString, "(T1.`PartieID` like ?)");
      if ( ! (GXutil.strcmp("", AV6cIdentificationIssuingCountryName)==0) )
      {
         addWhere(sWhereString, "(T3.`CountryName` like ?)");
      }
      else
      {
         GXv_int3[1] = (byte)(1) ;
      }
      if ( ! (GXutil.strcmp("", AV8cPartieName)==0) )
      {
         addWhere(sWhereString, "(T1.`PartieName` like ?)");
      }
      else
      {
         GXv_int3[2] = (byte)(1) ;
      }
      if ( ! (GXutil.strcmp("", AV9cPartieShortName)==0) )
      {
         addWhere(sWhereString, "(T1.`PartieShortName` like ?)");
      }
      else
      {
         GXv_int3[3] = (byte)(1) ;
      }
      if ( ! (GXutil.strcmp("", AV14cPartieRoleName)==0) )
      {
         addWhere(sWhereString, "(T2.`PartieRoleName` like ?)");
      }
      else
      {
         GXv_int3[4] = (byte)(1) ;
      }
      scmdbuf += sWhereString ;
      GXv_Object4[0] = scmdbuf ;
      GXv_Object4[1] = GXv_int3 ;
      return GXv_Object4 ;
   }

   public Object [] getDynamicStatement( int cursor ,
                                         ModelContext context ,
                                         int remoteHandle ,
                                         com.genexus.IHttpContext httpContext ,
                                         Object [] dynConstraints )
   {
      switch ( cursor )
      {
            case 0 :
                  return conditional_H00082(context, remoteHandle, httpContext, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] );
            case 1 :
                  return conditional_H00083(context, remoteHandle, httpContext, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] );
      }
      return super.getDynamicStatement(cursor, context, remoteHandle, httpContext, dynConstraints);
   }

   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("H00082", "scmdbuf",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,11, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("H00083", "scmdbuf",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("H00084", "SELECT `CountryCode`, `CountryName` FROM `Country` ORDER BY `CountryName` ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("H00085", "SELECT `CountryCode`, `CountryName` FROM `Country` ORDER BY `CountryName` ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0, GxCacheFrequency.OFF,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               ((String[]) buf[2])[0] = rslt.getVarchar(3);
               ((String[]) buf[3])[0] = rslt.getString(4, 2);
               ((String[]) buf[4])[0] = rslt.getVarchar(5);
               ((String[]) buf[5])[0] = rslt.getString(6, 30);
               ((String[]) buf[6])[0] = rslt.getVarchar(7);
               return;
            case 1 :
               ((long[]) buf[0])[0] = rslt.getLong(1);
               return;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      short sIdx;
      switch ( cursor )
      {
            case 0 :
               sIdx = (short)(0) ;
               if ( ((Number) parms[0]).byteValue() == 0 )
               {
                  sIdx = (short)(sIdx+1) ;
                  stmt.setString(sIdx, (String)parms[7], 30);
               }
               if ( ((Number) parms[1]).byteValue() == 0 )
               {
                  sIdx = (short)(sIdx+1) ;
                  stmt.setVarchar(sIdx, (String)parms[8], 50);
               }
               if ( ((Number) parms[2]).byteValue() == 0 )
               {
                  sIdx = (short)(sIdx+1) ;
                  stmt.setVarchar(sIdx, (String)parms[9], 50);
               }
               if ( ((Number) parms[3]).byteValue() == 0 )
               {
                  sIdx = (short)(sIdx+1) ;
                  stmt.setVarchar(sIdx, (String)parms[10], 50);
               }
               if ( ((Number) parms[4]).byteValue() == 0 )
               {
                  sIdx = (short)(sIdx+1) ;
                  stmt.setVarchar(sIdx, (String)parms[11], 30);
               }
               if ( ((Number) parms[5]).byteValue() == 0 )
               {
                  sIdx = (short)(sIdx+1) ;
                  stmt.setInt(sIdx, ((Number) parms[12]).intValue());
               }
               if ( ((Number) parms[6]).byteValue() == 0 )
               {
                  sIdx = (short)(sIdx+1) ;
                  stmt.setInt(sIdx, ((Number) parms[13]).intValue());
               }
               return;
            case 1 :
               sIdx = (short)(0) ;
               if ( ((Number) parms[0]).byteValue() == 0 )
               {
                  sIdx = (short)(sIdx+1) ;
                  stmt.setString(sIdx, (String)parms[5], 30);
               }
               if ( ((Number) parms[1]).byteValue() == 0 )
               {
                  sIdx = (short)(sIdx+1) ;
                  stmt.setVarchar(sIdx, (String)parms[6], 50);
               }
               if ( ((Number) parms[2]).byteValue() == 0 )
               {
                  sIdx = (short)(sIdx+1) ;
                  stmt.setVarchar(sIdx, (String)parms[7], 50);
               }
               if ( ((Number) parms[3]).byteValue() == 0 )
               {
                  sIdx = (short)(sIdx+1) ;
                  stmt.setVarchar(sIdx, (String)parms[8], 50);
               }
               if ( ((Number) parms[4]).byteValue() == 0 )
               {
                  sIdx = (short)(sIdx+1) ;
                  stmt.setVarchar(sIdx, (String)parms[9], 30);
               }
               return;
      }
   }

}

