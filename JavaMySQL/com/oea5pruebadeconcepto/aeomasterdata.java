package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(urlPatterns = {"/servlet/com.oea5pruebadeconcepto.aeomasterdata", "/com.oea5pruebadeconcepto.aeomasterdata"})
public final  class aeomasterdata extends GXWebObjectStub
{
   public aeomasterdata( )
   {
   }

   public aeomasterdata( int remoteHandle )
   {
      super(remoteHandle, new ModelContext( aeomasterdata.class ));
   }

   public aeomasterdata( int remoteHandle ,
                         ModelContext context )
   {
      super(remoteHandle, context);
   }

   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new aeomasterdata_impl(context).doExecute();
   }

   protected void init( com.genexus.internet.HttpContext context )
   {
      new aeomasterdata_impl(context).cleanup();
   }

   public String getServletInfo( )
   {
      return "AEO Master Data";
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

   protected String IntegratedSecurityPermissionPrefix( )
   {
      return "";
   }

   protected String EncryptURLParameters( )
   {
      return "NO";
   }

}

