package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(urlPatterns = {"/servlet/com.oea5pruebadeconcepto.gx0020", "/com.oea5pruebadeconcepto.gx0020"})
public final  class gx0020 extends GXWebObjectStub
{
   public gx0020( )
   {
   }

   public gx0020( int remoteHandle )
   {
      super(remoteHandle, new ModelContext( gx0020.class ));
   }

   public gx0020( int remoteHandle ,
                  ModelContext context )
   {
      super(remoteHandle, context);
   }

   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new gx0020_impl(context).doExecute();
   }

   protected void init( com.genexus.internet.HttpContext context )
   {
      new gx0020_impl(context).cleanup();
   }

   public String getServletInfo( )
   {
      return "Selection List Country";
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

   protected String IntegratedSecurityPermissionPrefix( )
   {
      return "";
   }

   protected String EncryptURLParameters( )
   {
      return "NO";
   }

}

