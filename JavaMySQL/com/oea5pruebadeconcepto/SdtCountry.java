package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.xml.*;
import com.genexus.search.*;
import com.genexus.webpanels.*;
import java.util.*;

public final  class SdtCountry extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtCountry( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtCountry.class));
   }

   public SdtCountry( int remoteHandle ,
                      ModelContext context )
   {
      super( remoteHandle, context, "SdtCountry");
      initialize( remoteHandle) ;
   }

   public SdtCountry( int remoteHandle ,
                      StructSdtCountry struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   private static java.util.HashMap mapper = new java.util.HashMap();
   static
   {
   }

   public String getJsonMap( String value )
   {
      return (String) mapper.get(value);
   }

   public void Load( String AV2CountryCode )
   {
      IGxSilentTrn obj;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV2CountryCode});
   }

   public Object[][] GetBCKey( )
   {
      return (Object[][])(new Object[][]{new Object[]{"CountryCode", String.class}}) ;
   }

   public com.genexus.util.GXProperties getMetadata( )
   {
      com.genexus.util.GXProperties metadata = new com.genexus.util.GXProperties();
      metadata.set("Name", "Country");
      metadata.set("BT", "Country");
      metadata.set("PK", "[ \"CountryCode\" ]");
      metadata.set("AllowInsert", "True");
      metadata.set("AllowUpdate", "True");
      metadata.set("AllowDelete", "True");
      return metadata ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError = 1;
      formatError = false ;
      sTagName = oReader.getName() ;
      if ( oReader.getIsSimple() == 0 )
      {
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(0) ;
         while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
         {
            readOk = (short)(0) ;
            readElement = false ;
            if ( GXutil.strcmp2( oReader.getLocalName(), "CountryCode") )
            {
               gxTv_SdtCountry_Countrycode = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "CountryName") )
            {
               gxTv_SdtCountry_Countryname = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "Mode") )
            {
               gxTv_SdtCountry_Mode = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "Initialized") )
            {
               gxTv_SdtCountry_Initialized = (short)(getnumericvalue(oReader.getValue())) ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "CountryCode_Z") )
            {
               gxTv_SdtCountry_Countrycode_Z = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( GXutil.strcmp2( oReader.getLocalName(), "CountryName_Z") )
            {
               gxTv_SdtCountry_Countryname_Z = oReader.getValue() ;
               readElement = true ;
               if ( GXSoapError > 0 )
               {
                  readOk = (short)(1) ;
               }
               GXSoapError = oReader.read() ;
            }
            if ( ! readElement )
            {
               readOk = (short)(1) ;
               GXSoapError = oReader.read() ;
            }
            nOutParmCount = (short)(nOutParmCount+1) ;
            if ( ( readOk == 0 ) || formatError )
            {
               context.globals.sSOAPErrMsg += "Error reading " + sTagName + GXutil.newLine( ) ;
               context.globals.sSOAPErrMsg += "Message: " + oReader.readRawXML() ;
               GXSoapError = (short)(nOutParmCount*-1) ;
            }
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      writexml(oWriter, sName, sNameSpace, true);
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace ,
                         boolean sIncludeState )
   {
      if ( (GXutil.strcmp("", sName)==0) )
      {
         sName = "Country" ;
      }
      if ( (GXutil.strcmp("", sNameSpace)==0) )
      {
         sNameSpace = "OEA5pruebaDeConcepto" ;
      }
      oWriter.writeStartElement(sName);
      if ( GXutil.strcmp(GXutil.left( sNameSpace, 10), "[*:nosend]") != 0 )
      {
         oWriter.writeAttribute("xmlns", sNameSpace);
      }
      else
      {
         sNameSpace = GXutil.right( sNameSpace, GXutil.len( sNameSpace)-10) ;
      }
      oWriter.writeElement("CountryCode", GXutil.rtrim( gxTv_SdtCountry_Countrycode));
      if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
      {
         oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
      }
      oWriter.writeElement("CountryName", GXutil.rtrim( gxTv_SdtCountry_Countryname));
      if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
      {
         oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
      }
      if ( sIncludeState )
      {
         oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtCountry_Mode));
         if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
         {
            oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
         }
         oWriter.writeElement("Initialized", GXutil.trim( GXutil.str( gxTv_SdtCountry_Initialized, 4, 0)));
         if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
         {
            oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
         }
         oWriter.writeElement("CountryCode_Z", GXutil.rtrim( gxTv_SdtCountry_Countrycode_Z));
         if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
         {
            oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
         }
         oWriter.writeElement("CountryName_Z", GXutil.rtrim( gxTv_SdtCountry_Countryname_Z));
         if ( GXutil.strcmp(sNameSpace, "OEA5pruebaDeConcepto") != 0 )
         {
            oWriter.writeAttribute("xmlns", "OEA5pruebaDeConcepto");
         }
      }
      oWriter.writeEndElement();
   }

   public long getnumericvalue( String value )
   {
      if ( GXutil.notNumeric( value) )
      {
         formatError = true ;
      }
      return GXutil.lval( value) ;
   }

   public void tojson( )
   {
      tojson( true) ;
   }

   public void tojson( boolean includeState )
   {
      tojson( includeState, true) ;
   }

   public void tojson( boolean includeState ,
                       boolean includeNonInitialized )
   {
      AddObjectProperty("CountryCode", gxTv_SdtCountry_Countrycode, false, includeNonInitialized);
      AddObjectProperty("CountryName", gxTv_SdtCountry_Countryname, false, includeNonInitialized);
      if ( includeState )
      {
         AddObjectProperty("Mode", gxTv_SdtCountry_Mode, false, includeNonInitialized);
         AddObjectProperty("Initialized", gxTv_SdtCountry_Initialized, false, includeNonInitialized);
         AddObjectProperty("CountryCode_Z", gxTv_SdtCountry_Countrycode_Z, false, includeNonInitialized);
         AddObjectProperty("CountryName_Z", gxTv_SdtCountry_Countryname_Z, false, includeNonInitialized);
      }
   }

   public void updateDirties( com.oea5pruebadeconcepto.SdtCountry sdt )
   {
      if ( sdt.IsDirty("CountryCode") )
      {
         gxTv_SdtCountry_N = (byte)(0) ;
         gxTv_SdtCountry_Countrycode = sdt.getgxTv_SdtCountry_Countrycode() ;
      }
      if ( sdt.IsDirty("CountryName") )
      {
         gxTv_SdtCountry_N = (byte)(0) ;
         gxTv_SdtCountry_Countryname = sdt.getgxTv_SdtCountry_Countryname() ;
      }
   }

   public String getgxTv_SdtCountry_Countrycode( )
   {
      return gxTv_SdtCountry_Countrycode ;
   }

   public void setgxTv_SdtCountry_Countrycode( String value )
   {
      gxTv_SdtCountry_N = (byte)(0) ;
      if ( GXutil.strcmp(gxTv_SdtCountry_Countrycode, value) != 0 )
      {
         gxTv_SdtCountry_Mode = "INS" ;
         this.setgxTv_SdtCountry_Countrycode_Z_SetNull( );
         this.setgxTv_SdtCountry_Countryname_Z_SetNull( );
      }
      SetDirty("Countrycode");
      gxTv_SdtCountry_Countrycode = value ;
   }

   public String getgxTv_SdtCountry_Countryname( )
   {
      return gxTv_SdtCountry_Countryname ;
   }

   public void setgxTv_SdtCountry_Countryname( String value )
   {
      gxTv_SdtCountry_N = (byte)(0) ;
      SetDirty("Countryname");
      gxTv_SdtCountry_Countryname = value ;
   }

   public String getgxTv_SdtCountry_Mode( )
   {
      return gxTv_SdtCountry_Mode ;
   }

   public void setgxTv_SdtCountry_Mode( String value )
   {
      gxTv_SdtCountry_N = (byte)(0) ;
      SetDirty("Mode");
      gxTv_SdtCountry_Mode = value ;
   }

   public void setgxTv_SdtCountry_Mode_SetNull( )
   {
      gxTv_SdtCountry_Mode = "" ;
   }

   public boolean getgxTv_SdtCountry_Mode_IsNull( )
   {
      return false ;
   }

   public short getgxTv_SdtCountry_Initialized( )
   {
      return gxTv_SdtCountry_Initialized ;
   }

   public void setgxTv_SdtCountry_Initialized( short value )
   {
      gxTv_SdtCountry_N = (byte)(0) ;
      SetDirty("Initialized");
      gxTv_SdtCountry_Initialized = value ;
   }

   public void setgxTv_SdtCountry_Initialized_SetNull( )
   {
      gxTv_SdtCountry_Initialized = (short)(0) ;
   }

   public boolean getgxTv_SdtCountry_Initialized_IsNull( )
   {
      return false ;
   }

   public String getgxTv_SdtCountry_Countrycode_Z( )
   {
      return gxTv_SdtCountry_Countrycode_Z ;
   }

   public void setgxTv_SdtCountry_Countrycode_Z( String value )
   {
      gxTv_SdtCountry_N = (byte)(0) ;
      SetDirty("Countrycode_Z");
      gxTv_SdtCountry_Countrycode_Z = value ;
   }

   public void setgxTv_SdtCountry_Countrycode_Z_SetNull( )
   {
      gxTv_SdtCountry_Countrycode_Z = "" ;
   }

   public boolean getgxTv_SdtCountry_Countrycode_Z_IsNull( )
   {
      return false ;
   }

   public String getgxTv_SdtCountry_Countryname_Z( )
   {
      return gxTv_SdtCountry_Countryname_Z ;
   }

   public void setgxTv_SdtCountry_Countryname_Z( String value )
   {
      gxTv_SdtCountry_N = (byte)(0) ;
      SetDirty("Countryname_Z");
      gxTv_SdtCountry_Countryname_Z = value ;
   }

   public void setgxTv_SdtCountry_Countryname_Z_SetNull( )
   {
      gxTv_SdtCountry_Countryname_Z = "" ;
   }

   public boolean getgxTv_SdtCountry_Countryname_Z_IsNull( )
   {
      return false ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      com.oea5pruebadeconcepto.country_bc obj;
      obj = new com.oea5pruebadeconcepto.country_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
   }

   public void initialize( )
   {
      gxTv_SdtCountry_Countrycode = "" ;
      gxTv_SdtCountry_N = (byte)(1) ;
      gxTv_SdtCountry_Countryname = "" ;
      gxTv_SdtCountry_Mode = "" ;
      gxTv_SdtCountry_Countrycode_Z = "" ;
      gxTv_SdtCountry_Countryname_Z = "" ;
      sTagName = "" ;
   }

   public byte isNull( )
   {
      return gxTv_SdtCountry_N ;
   }

   public com.oea5pruebadeconcepto.SdtCountry Clone( )
   {
      com.oea5pruebadeconcepto.SdtCountry sdt;
      com.oea5pruebadeconcepto.country_bc obj;
      sdt = (com.oea5pruebadeconcepto.SdtCountry)(clone()) ;
      obj = (com.oea5pruebadeconcepto.country_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( com.oea5pruebadeconcepto.StructSdtCountry struct )
   {
      setgxTv_SdtCountry_Countrycode(struct.getCountrycode());
      setgxTv_SdtCountry_Countryname(struct.getCountryname());
      setgxTv_SdtCountry_Mode(struct.getMode());
      setgxTv_SdtCountry_Initialized(struct.getInitialized());
      setgxTv_SdtCountry_Countrycode_Z(struct.getCountrycode_Z());
      setgxTv_SdtCountry_Countryname_Z(struct.getCountryname_Z());
   }

   @SuppressWarnings("unchecked")
   public com.oea5pruebadeconcepto.StructSdtCountry getStruct( )
   {
      com.oea5pruebadeconcepto.StructSdtCountry struct = new com.oea5pruebadeconcepto.StructSdtCountry ();
      struct.setCountrycode(getgxTv_SdtCountry_Countrycode());
      struct.setCountryname(getgxTv_SdtCountry_Countryname());
      struct.setMode(getgxTv_SdtCountry_Mode());
      struct.setInitialized(getgxTv_SdtCountry_Initialized());
      struct.setCountrycode_Z(getgxTv_SdtCountry_Countrycode_Z());
      struct.setCountryname_Z(getgxTv_SdtCountry_Countryname_Z());
      return struct ;
   }

   private byte gxTv_SdtCountry_N ;
   private short gxTv_SdtCountry_Initialized ;
   private short readOk ;
   private short nOutParmCount ;
   private String gxTv_SdtCountry_Countrycode ;
   private String gxTv_SdtCountry_Mode ;
   private String gxTv_SdtCountry_Countrycode_Z ;
   private String sTagName ;
   private boolean readElement ;
   private boolean formatError ;
   private String gxTv_SdtCountry_Countryname ;
   private String gxTv_SdtCountry_Countryname_Z ;
}

