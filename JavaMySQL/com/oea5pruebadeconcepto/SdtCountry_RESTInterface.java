package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.fasterxml.jackson.annotation.*;
import java.util.*;

@javax.xml.bind.annotation.XmlType(name = "Country", namespace ="OEA5pruebaDeConcepto")
@JsonPropertyOrder(alphabetic=true)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.NONE, getterVisibility=JsonAutoDetect.Visibility.NONE, isGetterVisibility=JsonAutoDetect.Visibility.NONE)
public final  class SdtCountry_RESTInterface extends GxGenericCollectionItem<com.oea5pruebadeconcepto.SdtCountry>
{
   public SdtCountry_RESTInterface( )
   {
      super(new com.oea5pruebadeconcepto.SdtCountry (-1));
   }

   public SdtCountry_RESTInterface( com.oea5pruebadeconcepto.SdtCountry psdt )
   {
      super(psdt);
   }

   @GxSeudo
   @JsonProperty("CountryCode")
   public String getgxTv_SdtCountry_Countrycode( )
   {
      return GXutil.rtrim(((com.oea5pruebadeconcepto.SdtCountry)getSdt()).getgxTv_SdtCountry_Countrycode()) ;
   }

   @JsonProperty("CountryCode")
   public void setgxTv_SdtCountry_Countrycode(  String Value )
   {
      ((com.oea5pruebadeconcepto.SdtCountry)getSdt()).setgxTv_SdtCountry_Countrycode(Value);
   }


   @GxSeudo
   @JsonProperty("CountryName")
   public String getgxTv_SdtCountry_Countryname( )
   {
      return GXutil.rtrim(((com.oea5pruebadeconcepto.SdtCountry)getSdt()).getgxTv_SdtCountry_Countryname()) ;
   }

   @JsonProperty("CountryName")
   public void setgxTv_SdtCountry_Countryname(  String Value )
   {
      ((com.oea5pruebadeconcepto.SdtCountry)getSdt()).setgxTv_SdtCountry_Countryname(Value);
   }


   public void copyFrom( com.oea5pruebadeconcepto.SdtCountry_RESTInterface value )
   {
      ((com.oea5pruebadeconcepto.SdtCountry)getSdt()).setgxTv_SdtCountry_Countrycode(value.getgxTv_SdtCountry_Countrycode());
      ((com.oea5pruebadeconcepto.SdtCountry)getSdt()).setgxTv_SdtCountry_Countryname(value.getgxTv_SdtCountry_Countryname());
   }

   @JsonProperty("gx_md5_hash")
   public String getHash( )
   {
      if ( GXutil.strcmp(md5Hash, null) == 0 )
      {
         md5Hash = super.getHash() ;
      }
      return md5Hash ;
   }

   @JsonProperty("gx_md5_hash")
   public void setHash(  String Value )
   {
      md5Hash = Value ;
   }


   private String md5Hash ;
}

