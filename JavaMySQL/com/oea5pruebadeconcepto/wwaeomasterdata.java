package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(urlPatterns = {"/servlet/com.oea5pruebadeconcepto.wwaeomasterdata", "/com.oea5pruebadeconcepto.wwaeomasterdata"})
public final  class wwaeomasterdata extends GXWebObjectStub
{
   public wwaeomasterdata( )
   {
   }

   public wwaeomasterdata( int remoteHandle )
   {
      super(remoteHandle, new ModelContext( wwaeomasterdata.class ));
   }

   public wwaeomasterdata( int remoteHandle ,
                           ModelContext context )
   {
      super(remoteHandle, context);
   }

   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new wwaeomasterdata_impl(context).doExecute();
   }

   protected void init( com.genexus.internet.HttpContext context )
   {
      new wwaeomasterdata_impl(context).cleanup();
   }

   public String getServletInfo( )
   {
      return "AEO Master Datas";
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

   protected String IntegratedSecurityPermissionPrefix( )
   {
      return "";
   }

   protected String EncryptURLParameters( )
   {
      return "NO";
   }

}

