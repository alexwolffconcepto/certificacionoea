package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.fasterxml.jackson.annotation.*;
import java.util.*;

@javax.xml.bind.annotation.XmlType(name = "AEOMasterData", namespace ="OEA5pruebaDeConcepto")
@JsonPropertyOrder(alphabetic=true)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.NONE, getterVisibility=JsonAutoDetect.Visibility.NONE, isGetterVisibility=JsonAutoDetect.Visibility.NONE)
public final  class SdtAEOMasterData_RESTInterface extends GxGenericCollectionItem<com.oea5pruebadeconcepto.SdtAEOMasterData>
{
   public SdtAEOMasterData_RESTInterface( )
   {
      super(new com.oea5pruebadeconcepto.SdtAEOMasterData (-1));
   }

   public SdtAEOMasterData_RESTInterface( com.oea5pruebadeconcepto.SdtAEOMasterData psdt )
   {
      super(psdt);
   }

   @GxSeudo
   @JsonProperty("IdentificationIssuingCountryCode")
   public String getgxTv_SdtAEOMasterData_Identificationissuingcountrycode( )
   {
      return GXutil.rtrim(((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).getgxTv_SdtAEOMasterData_Identificationissuingcountrycode()) ;
   }

   @JsonProperty("IdentificationIssuingCountryCode")
   public void setgxTv_SdtAEOMasterData_Identificationissuingcountrycode(  String Value )
   {
      ((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).setgxTv_SdtAEOMasterData_Identificationissuingcountrycode(Value);
   }


   @GxSeudo
   @JsonProperty("IdentificationIssuingCountryName")
   public String getgxTv_SdtAEOMasterData_Identificationissuingcountryname( )
   {
      return GXutil.rtrim(((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).getgxTv_SdtAEOMasterData_Identificationissuingcountryname()) ;
   }

   @JsonProperty("IdentificationIssuingCountryName")
   public void setgxTv_SdtAEOMasterData_Identificationissuingcountryname(  String Value )
   {
      ((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).setgxTv_SdtAEOMasterData_Identificationissuingcountryname(Value);
   }


   @GxSeudo
   @JsonProperty("PartieID")
   public String getgxTv_SdtAEOMasterData_Partieid( )
   {
      return GXutil.rtrim(((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).getgxTv_SdtAEOMasterData_Partieid()) ;
   }

   @JsonProperty("PartieID")
   public void setgxTv_SdtAEOMasterData_Partieid(  String Value )
   {
      ((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).setgxTv_SdtAEOMasterData_Partieid(Value);
   }


   @GxSeudo
   @JsonProperty("PartieName")
   public String getgxTv_SdtAEOMasterData_Partiename( )
   {
      return GXutil.rtrim(((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).getgxTv_SdtAEOMasterData_Partiename()) ;
   }

   @JsonProperty("PartieName")
   public void setgxTv_SdtAEOMasterData_Partiename(  String Value )
   {
      ((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).setgxTv_SdtAEOMasterData_Partiename(Value);
   }


   @GxSeudo
   @JsonProperty("PartieShortName")
   public String getgxTv_SdtAEOMasterData_Partieshortname( )
   {
      return GXutil.rtrim(((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).getgxTv_SdtAEOMasterData_Partieshortname()) ;
   }

   @JsonProperty("PartieShortName")
   public void setgxTv_SdtAEOMasterData_Partieshortname(  String Value )
   {
      ((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).setgxTv_SdtAEOMasterData_Partieshortname(Value);
   }


   @GxSeudo
   @JsonProperty("PartieRoleCode")
   public String getgxTv_SdtAEOMasterData_Partierolecode( )
   {
      return GXutil.rtrim(((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).getgxTv_SdtAEOMasterData_Partierolecode()) ;
   }

   @JsonProperty("PartieRoleCode")
   public void setgxTv_SdtAEOMasterData_Partierolecode(  String Value )
   {
      ((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).setgxTv_SdtAEOMasterData_Partierolecode(Value);
   }


   public void copyFrom( com.oea5pruebadeconcepto.SdtAEOMasterData_RESTInterface value )
   {
      ((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).setgxTv_SdtAEOMasterData_Identificationissuingcountrycode(value.getgxTv_SdtAEOMasterData_Identificationissuingcountrycode());
      ((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).setgxTv_SdtAEOMasterData_Identificationissuingcountryname(value.getgxTv_SdtAEOMasterData_Identificationissuingcountryname());
      ((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).setgxTv_SdtAEOMasterData_Partieid(value.getgxTv_SdtAEOMasterData_Partieid());
      ((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).setgxTv_SdtAEOMasterData_Partiename(value.getgxTv_SdtAEOMasterData_Partiename());
      ((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).setgxTv_SdtAEOMasterData_Partieshortname(value.getgxTv_SdtAEOMasterData_Partieshortname());
      ((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).setgxTv_SdtAEOMasterData_Partierolecode(value.getgxTv_SdtAEOMasterData_Partierolecode());
   }

   @JsonProperty("gx_md5_hash")
   public String getHash( )
   {
      if ( GXutil.strcmp(md5Hash, null) == 0 )
      {
         md5Hash = super.getHash() ;
      }
      return md5Hash ;
   }

   @JsonProperty("gx_md5_hash")
   public void setHash(  String Value )
   {
      md5Hash = Value ;
   }


   private String md5Hash ;
}

