package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class aeomasterdata_impl extends GXDataArea
{
   public void initenv( )
   {
      if ( GxWebError != 0 )
      {
         return  ;
      }
   }

   public void inittrn( )
   {
      initialize_properties( ) ;
      entryPointCalled = false ;
      gxfirstwebparm = httpContext.GetFirstPar( "Mode") ;
      gxfirstwebparm_bkp = gxfirstwebparm ;
      gxfirstwebparm = httpContext.DecryptAjaxCall( gxfirstwebparm) ;
      toggleJsOutput = httpContext.isJsOutputEnabled( ) ;
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.disableJsOutput();
      }
      if ( GXutil.strcmp(gxfirstwebparm, "dyncall") == 0 )
      {
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         dyncall( httpContext.GetNextPar( )) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_14") == 0 )
      {
         A1IdentificationIssuingCountryCo = httpContext.GetPar( "IdentificationIssuingCountryCo") ;
         httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxload_14( A1IdentificationIssuingCountryCo) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
      {
         A8PartieRoleCode = httpContext.GetPar( "PartieRoleCode") ;
         httpContext.ajax_rsp_assign_attri("", false, "A8PartieRoleCode", A8PartieRoleCode);
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxload_13( A8PartieRoleCode) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxEvt") == 0 )
      {
         httpContext.setAjaxEventMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxfirstwebparm = httpContext.GetFirstPar( "Mode") ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
      {
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxfirstwebparm = httpContext.GetFirstPar( "Mode") ;
      }
      else
      {
         if ( ! httpContext.IsValidAjaxCall( false) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxfirstwebparm = gxfirstwebparm_bkp ;
      }
      if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
      {
         Gx_mode = gxfirstwebparm ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         if ( GXutil.strcmp(gxfirstwebparm, "viewer") != 0 )
         {
            AV7IdentificationIssuingCountryCode = httpContext.GetPar( "IdentificationIssuingCountryCode") ;
            httpContext.ajax_rsp_assign_attri("", false, "AV7IdentificationIssuingCountryCode", AV7IdentificationIssuingCountryCode);
            com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "gxhash_vIDENTIFICATIONISSUINGCOUNTRYCODE", getSecureSignedToken( "", GXutil.rtrim( localUtil.format( AV7IdentificationIssuingCountryCode, ""))));
            AV8PartieID = httpContext.GetPar( "PartieID") ;
            httpContext.ajax_rsp_assign_attri("", false, "AV8PartieID", AV8PartieID);
            com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "gxhash_vPARTIEID", getSecureSignedToken( "", GXutil.rtrim( localUtil.format( AV8PartieID, ""))));
         }
      }
      if ( toggleJsOutput )
      {
         if ( httpContext.isSpaRequest( ) )
         {
            httpContext.enableJsOutput();
         }
      }
      if ( (GXutil.strcmp("", httpContext.getCookie( "GX_SESSION_ID"))==0) )
      {
         gxcookieaux = httpContext.setCookie( "GX_SESSION_ID", httpContext.encrypt64( com.genexus.util.Encryption.getNewKey( ), context.getServerKey( )), "", GXutil.nullDate(), "", (short)(httpContext.getHttpSecure( ))) ;
      }
      GXKey = httpContext.decrypt64( httpContext.getCookie( "GX_SESSION_ID"), context.getServerKey( )) ;
      toggleJsOutput = httpContext.isJsOutputEnabled( ) ;
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.disableJsOutput();
      }
      init_web_controls( ) ;
      if ( toggleJsOutput )
      {
         if ( httpContext.isSpaRequest( ) )
         {
            httpContext.enableJsOutput();
         }
      }
      if ( ! httpContext.isSpaRequest( ) )
      {
         if ( httpContext.exposeMetadata( ) )
         {
            Form.getMeta().addItem("generator", "GeneXus Java 17_0_8-158023", (short)(0)) ;
         }
         Form.getMeta().addItem("description", "AEO Master Data", (short)(0)) ;
      }
      httpContext.wjLoc = "" ;
      httpContext.nUserReturn = (byte)(0) ;
      httpContext.wbHandled = (byte)(0) ;
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
      }
      if ( ! httpContext.isAjaxRequest( ) )
      {
         GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      wbErr = false ;
      httpContext.setDefaultTheme("Carmine");
      if ( ! httpContext.isLocalStorageSupported( ) )
      {
         httpContext.pushCurrentUrl();
      }
   }

   public aeomasterdata_impl( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public aeomasterdata_impl( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( aeomasterdata_impl.class ));
   }

   public aeomasterdata_impl( int remoteHandle ,
                              ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected void createObjects( )
   {
      dynIdentificationIssuingCountryCo = new HTMLChoice();
      dynPartieRoleCode = new HTMLChoice();
   }

   public void webExecute( )
   {
      initenv( ) ;
      inittrn( ) ;
      if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
      {
         MasterPageObj= createMasterPage(remoteHandle, "com.oea5pruebadeconcepto.rwdmasterpage");
         MasterPageObj.setDataArea(this,false);
         validateSpaRequest();
         MasterPageObj.webExecute();
         if ( ( GxWebError == 0 ) && httpContext.isAjaxRequest( ) )
         {
            httpContext.enableOutput();
            if ( ! httpContext.isAjaxRequest( ) )
            {
               httpContext.GX_webresponse.addHeader("Cache-Control", "no-store");
            }
            if ( ! httpContext.willRedirect( ) )
            {
               addString( httpContext.getJSONResponse( )) ;
            }
            else
            {
               if ( httpContext.isAjaxRequest( ) )
               {
                  httpContext.disableOutput();
               }
               renderHtmlHeaders( ) ;
               httpContext.redirect( httpContext.wjLoc );
               httpContext.dispatchAjaxCommands();
            }
         }
      }
      if ( isAjaxCallMode( ) )
      {
         cleanup();
      }
   }

   public void fix_multi_value_controls( )
   {
      if ( dynIdentificationIssuingCountryCo.getItemCount() > 0 )
      {
         A1IdentificationIssuingCountryCo = dynIdentificationIssuingCountryCo.getValidValue(A1IdentificationIssuingCountryCo) ;
         httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
      }
      if ( httpContext.isAjaxRequest( ) )
      {
         dynIdentificationIssuingCountryCo.setValue( GXutil.rtrim( A1IdentificationIssuingCountryCo) );
         httpContext.ajax_rsp_assign_prop("", false, dynIdentificationIssuingCountryCo.getInternalname(), "Values", dynIdentificationIssuingCountryCo.ToJavascriptSource(), true);
      }
      if ( dynPartieRoleCode.getItemCount() > 0 )
      {
         A8PartieRoleCode = dynPartieRoleCode.getValidValue(A8PartieRoleCode) ;
         httpContext.ajax_rsp_assign_attri("", false, "A8PartieRoleCode", A8PartieRoleCode);
      }
      if ( httpContext.isAjaxRequest( ) )
      {
         dynPartieRoleCode.setValue( GXutil.rtrim( A8PartieRoleCode) );
         httpContext.ajax_rsp_assign_prop("", false, dynPartieRoleCode.getInternalname(), "Values", dynPartieRoleCode.ToJavascriptSource(), true);
      }
   }

   public void draw( )
   {
      if ( httpContext.isAjaxRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( ! com.oea5pruebadeconcepto.GxWebStd.gx_redirect( httpContext) )
      {
         disable_std_buttons( ) ;
         enableDisable( ) ;
         set_caption( ) ;
         /* Form start */
         drawControls( ) ;
         fix_multi_value_controls( ) ;
      }
      /* Execute Exit event if defined. */
   }

   public void drawControls( )
   {
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
      /* Text block */
      com.oea5pruebadeconcepto.GxWebStd.gx_label_ctrl( httpContext, lblTitle_Internalname, "AEO Master Data", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, (short)(0), "HLP_AEOMasterData.htm");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
      ClassString = "ErrorViewer" ;
      StyleString = "" ;
      com.oea5pruebadeconcepto.GxWebStd.gx_msg_list( httpContext, "", httpContext.GX_msglist.getDisplaymode(), StyleString, ClassString, "", "false");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
      TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"" ;
      ClassString = "BtnFirst" ;
      StyleString = "" ;
      com.oea5pruebadeconcepto.GxWebStd.gx_button_ctrl( httpContext, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", httpContext.getButtonType( ), "HLP_AEOMasterData.htm");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
      TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"" ;
      ClassString = "BtnPrevious" ;
      StyleString = "" ;
      com.oea5pruebadeconcepto.GxWebStd.gx_button_ctrl( httpContext, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", httpContext.getButtonType( ), "HLP_AEOMasterData.htm");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
      TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"" ;
      ClassString = "BtnNext" ;
      StyleString = "" ;
      com.oea5pruebadeconcepto.GxWebStd.gx_button_ctrl( httpContext, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", httpContext.getButtonType( ), "HLP_AEOMasterData.htm");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
      TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"" ;
      ClassString = "BtnLast" ;
      StyleString = "" ;
      com.oea5pruebadeconcepto.GxWebStd.gx_button_ctrl( httpContext, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", httpContext.getButtonType( ), "HLP_AEOMasterData.htm");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
      TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"" ;
      ClassString = "BtnSelect" ;
      StyleString = "" ;
      com.oea5pruebadeconcepto.GxWebStd.gx_button_ctrl( httpContext, bttBtn_select_Internalname, "", "Seleccionar", bttBtn_select_Jsonclick, 4, "Seleccionar", "", StyleString, ClassString, bttBtn_select_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"com.oea5pruebadeconcepto.gx0030"+"',["+"{Ctrl:gx.dom.el('"+"IDENTIFICATIONISSUINGCOUNTRYCO"+"'), id:'"+"IDENTIFICATIONISSUINGCOUNTRYCO"+"'"+",IOType:'out'}"+","+"{Ctrl:gx.dom.el('"+"PARTIEID"+"'), id:'"+"PARTIEID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_AEOMasterData.htm");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+dynIdentificationIssuingCountryCo.getInternalname()+"\"", "", "div");
      /* Attribute/Variable Label */
      com.oea5pruebadeconcepto.GxWebStd.gx_label_element( httpContext, dynIdentificationIssuingCountryCo.getInternalname(), "Country Name", "col-sm-3 AttributeLabel", 1, true, "");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
      TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"" ;
      /* ComboBox */
      com.oea5pruebadeconcepto.GxWebStd.gx_combobox_ctrl1( httpContext, dynIdentificationIssuingCountryCo, dynIdentificationIssuingCountryCo.getInternalname(), GXutil.rtrim( A1IdentificationIssuingCountryCo), 1, dynIdentificationIssuingCountryCo.getJsonclick(), 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "Identification Issuing Country Code", 1, dynIdentificationIssuingCountryCo.getEnabled(), 1, (short)(0), 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, (byte)(1), "HLP_AEOMasterData.htm");
      dynIdentificationIssuingCountryCo.setValue( GXutil.rtrim( A1IdentificationIssuingCountryCo) );
      httpContext.ajax_rsp_assign_prop("", false, dynIdentificationIssuingCountryCo.getInternalname(), "Values", dynIdentificationIssuingCountryCo.ToJavascriptSource(), true);
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPartieID_Internalname+"\"", "", "div");
      /* Attribute/Variable Label */
      com.oea5pruebadeconcepto.GxWebStd.gx_label_element( httpContext, edtPartieID_Internalname, "Partie ID", "col-sm-3 AttributeLabel", 1, true, "");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
      /* Single line edit */
      TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"" ;
      com.oea5pruebadeconcepto.GxWebStd.gx_single_line_edit( httpContext, edtPartieID_Internalname, GXutil.rtrim( A5PartieID), GXutil.rtrim( localUtil.format( A5PartieID, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPartieID_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPartieID_Enabled, 1, "text", "", 30, "chr", 1, "row", 30, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), (byte)(-1), true, "AEOPartieID", "left", true, "", "HLP_AEOMasterData.htm");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPartieName_Internalname+"\"", "", "div");
      /* Attribute/Variable Label */
      com.oea5pruebadeconcepto.GxWebStd.gx_label_element( httpContext, edtPartieName_Internalname, "Partie Name", "col-sm-3 AttributeLabel", 1, true, "");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
      /* Single line edit */
      TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"" ;
      com.oea5pruebadeconcepto.GxWebStd.gx_single_line_edit( httpContext, edtPartieName_Internalname, A6PartieName, GXutil.rtrim( localUtil.format( A6PartieName, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPartieName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPartieName_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), (byte)(-1), true, "AEOPartieName", "left", true, "", "HLP_AEOMasterData.htm");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPartieShortName_Internalname+"\"", "", "div");
      /* Attribute/Variable Label */
      com.oea5pruebadeconcepto.GxWebStd.gx_label_element( httpContext, edtPartieShortName_Internalname, "Partie Short Name", "col-sm-3 AttributeLabel", 1, true, "");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
      /* Single line edit */
      TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"" ;
      com.oea5pruebadeconcepto.GxWebStd.gx_single_line_edit( httpContext, edtPartieShortName_Internalname, A7PartieShortName, GXutil.rtrim( localUtil.format( A7PartieShortName, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPartieShortName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPartieShortName_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), (byte)(-1), true, "AEOPartieShortName", "left", true, "", "HLP_AEOMasterData.htm");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+dynPartieRoleCode.getInternalname()+"\"", "", "div");
      /* Attribute/Variable Label */
      com.oea5pruebadeconcepto.GxWebStd.gx_label_element( httpContext, dynPartieRoleCode.getInternalname(), "Partie Role Name", "col-sm-3 AttributeLabel", 1, true, "");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
      TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"" ;
      /* ComboBox */
      com.oea5pruebadeconcepto.GxWebStd.gx_combobox_ctrl1( httpContext, dynPartieRoleCode, dynPartieRoleCode.getInternalname(), GXutil.rtrim( A8PartieRoleCode), 1, dynPartieRoleCode.getJsonclick(), 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "Partie Role Code", 1, dynPartieRoleCode.getEnabled(), 1, (short)(0), 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, (byte)(1), "HLP_AEOMasterData.htm");
      dynPartieRoleCode.setValue( GXutil.rtrim( A8PartieRoleCode) );
      httpContext.ajax_rsp_assign_prop("", false, dynPartieRoleCode.getInternalname(), "Values", dynPartieRoleCode.ToJavascriptSource(), true);
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
      TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"" ;
      ClassString = "BtnEnter" ;
      StyleString = "" ;
      com.oea5pruebadeconcepto.GxWebStd.gx_button_ctrl( httpContext, bttBtn_enter_Internalname, "", bttBtn_enter_Caption, bttBtn_enter_Jsonclick, 5, bttBtn_enter_Tooltiptext, "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", httpContext.getButtonType( ), "HLP_AEOMasterData.htm");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
      TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"" ;
      ClassString = "BtnCancel" ;
      StyleString = "" ;
      com.oea5pruebadeconcepto.GxWebStd.gx_button_ctrl( httpContext, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", httpContext.getButtonType( ), "HLP_AEOMasterData.htm");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      /* Div Control */
      com.oea5pruebadeconcepto.GxWebStd.gx_div_start( httpContext, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
      TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"" ;
      ClassString = "BtnDelete" ;
      StyleString = "" ;
      com.oea5pruebadeconcepto.GxWebStd.gx_button_ctrl( httpContext, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", httpContext.getButtonType( ), "HLP_AEOMasterData.htm");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "Center", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
      com.oea5pruebadeconcepto.GxWebStd.gx_div_end( httpContext, "left", "top", "div");
   }

   public void userMain( )
   {
      standaloneStartup( ) ;
   }

   public void userMainFullajax( )
   {
      initenv( ) ;
      inittrn( ) ;
      userMain( ) ;
      draw( ) ;
      sendCloseFormHiddens( ) ;
   }

   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
      process( ) ;
   }

   public void standaloneStartupServer( )
   {
      /* Execute Start event if defined. */
      httpContext.wbGlbDoneStart = (byte)(0) ;
      /* Execute user event: Start */
      e11012 ();
      httpContext.wbGlbDoneStart = (byte)(1) ;
      assign_properties_default( ) ;
      if ( AnyError == 0 )
      {
         if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read saved values. */
            Z1IdentificationIssuingCountryCo = httpContext.cgiGet( "Z1IdentificationIssuingCountryCo") ;
            Z5PartieID = httpContext.cgiGet( "Z5PartieID") ;
            Z6PartieName = httpContext.cgiGet( "Z6PartieName") ;
            Z7PartieShortName = httpContext.cgiGet( "Z7PartieShortName") ;
            Z8PartieRoleCode = httpContext.cgiGet( "Z8PartieRoleCode") ;
            IsConfirmed = (short)(localUtil.ctol( httpContext.cgiGet( "IsConfirmed"), ",", ".")) ;
            IsModified = (short)(localUtil.ctol( httpContext.cgiGet( "IsModified"), ",", ".")) ;
            Gx_mode = httpContext.cgiGet( "Mode") ;
            N8PartieRoleCode = httpContext.cgiGet( "N8PartieRoleCode") ;
            AV7IdentificationIssuingCountryCode = httpContext.cgiGet( "vIDENTIFICATIONISSUINGCOUNTRYCODE") ;
            AV8PartieID = httpContext.cgiGet( "vPARTIEID") ;
            AV12Insert_PartieRoleCode = httpContext.cgiGet( "vINSERT_PARTIEROLECODE") ;
            A4IdentificationIssuingCountryNa = httpContext.cgiGet( "IDENTIFICATIONISSUINGCOUNTRYNA") ;
            AV14Pgmname = httpContext.cgiGet( "vPGMNAME") ;
            /* Read variables values. */
            dynIdentificationIssuingCountryCo.setValue( httpContext.cgiGet( dynIdentificationIssuingCountryCo.getInternalname()) );
            A1IdentificationIssuingCountryCo = httpContext.cgiGet( dynIdentificationIssuingCountryCo.getInternalname()) ;
            httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
            A5PartieID = httpContext.cgiGet( edtPartieID_Internalname) ;
            httpContext.ajax_rsp_assign_attri("", false, "A5PartieID", A5PartieID);
            A6PartieName = httpContext.cgiGet( edtPartieName_Internalname) ;
            httpContext.ajax_rsp_assign_attri("", false, "A6PartieName", A6PartieName);
            A7PartieShortName = httpContext.cgiGet( edtPartieShortName_Internalname) ;
            httpContext.ajax_rsp_assign_attri("", false, "A7PartieShortName", A7PartieShortName);
            dynPartieRoleCode.setValue( httpContext.cgiGet( dynPartieRoleCode.getInternalname()) );
            A8PartieRoleCode = httpContext.cgiGet( dynPartieRoleCode.getInternalname()) ;
            httpContext.ajax_rsp_assign_attri("", false, "A8PartieRoleCode", A8PartieRoleCode);
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = httpContext.decrypt64( httpContext.getCookie( "GX_SESSION_ID"), context.getServerKey( )) ;
            forbiddenHiddens = new com.genexus.util.GXProperties() ;
            forbiddenHiddens.add("hshsalt", "hsh"+"AEOMasterData");
            forbiddenHiddens.add("Gx_mode", GXutil.rtrim( localUtil.format( Gx_mode, "@!")));
            hsh = httpContext.cgiGet( "hsh") ;
            if ( ( ! ( ( GXutil.strcmp(A1IdentificationIssuingCountryCo, Z1IdentificationIssuingCountryCo) != 0 ) || ( GXutil.strcmp(A5PartieID, Z5PartieID) != 0 ) ) || ( GXutil.strcmp(Gx_mode, "INS") == 0 ) ) && ! GXutil.checkEncryptedSignature( forbiddenHiddens.toString(), hsh, GXKey) )
            {
               GXutil.writeLogError("aeomasterdata:[ SecurityCheckFailed (403 Forbidden) value for]"+forbiddenHiddens.toJSonString());
               GxWebError = (byte)(1) ;
               httpContext.sendError( 403 );
               GXutil.writeLog("send_http_error_code 403");
               AnyError = (short)(1) ;
               return  ;
            }
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( GXutil.strcmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               A1IdentificationIssuingCountryCo = httpContext.GetPar( "IdentificationIssuingCountryCo") ;
               httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
               A5PartieID = httpContext.GetPar( "PartieID") ;
               httpContext.ajax_rsp_assign_attri("", false, "A5PartieID", A5PartieID);
               getEqualNoModal( ) ;
               Gx_mode = "DSP" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons( ) ;
               standaloneModal( ) ;
            }
            else
            {
               if ( isDsp( ) )
               {
                  sMode3 = Gx_mode ;
                  Gx_mode = "UPD" ;
                  httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  Gx_mode = sMode3 ;
                  httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               }
               standaloneModal( ) ;
               if ( ! isIns( ) )
               {
                  getByPrimaryKey( ) ;
                  if ( RcdFound3 == 1 )
                  {
                     if ( isDlt( ) )
                     {
                        /* Confirm record */
                        confirm_010( ) ;
                        if ( AnyError == 0 )
                        {
                           GX_FocusControl = bttBtn_enter_Internalname ;
                           httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                        }
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noinsert"), 1, "IDENTIFICATIONISSUINGCOUNTRYCO");
                     AnyError = (short)(1) ;
                     GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
                     httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
            }
         }
      }
   }

   public void process( )
   {
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         /* Read Transaction buttons. */
         sEvt = httpContext.cgiGet( "_EventName") ;
         EvtGridId = httpContext.cgiGet( "_EventGridId") ;
         EvtRowId = httpContext.cgiGet( "_EventRowId") ;
         if ( GXutil.len( sEvt) > 0 )
         {
            sEvtType = GXutil.left( sEvt, 1) ;
            sEvt = GXutil.right( sEvt, GXutil.len( sEvt)-1) ;
            if ( GXutil.strcmp(sEvtType, "M") != 0 )
            {
               if ( GXutil.strcmp(sEvtType, "E") == 0 )
               {
                  sEvtType = GXutil.right( sEvt, 1) ;
                  if ( GXutil.strcmp(sEvtType, ".") == 0 )
                  {
                     sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                     if ( GXutil.strcmp(sEvt, "START") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: Start */
                        e11012 ();
                     }
                     else if ( GXutil.strcmp(sEvt, "AFTER TRN") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: After Trn */
                        e12012 ();
                     }
                     else if ( GXutil.strcmp(sEvt, "ENTER") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        if ( ! isDsp( ) )
                        {
                           btn_enter( ) ;
                        }
                        /* No code required for Cancel button. It is implemented as the Reset button. */
                     }
                  }
                  else
                  {
                  }
               }
               httpContext.wbHandled = (byte)(1) ;
            }
         }
      }
   }

   public void afterTrn( )
   {
      if ( trnEnded == 1 )
      {
         if ( ! (GXutil.strcmp("", endTrnMsgTxt)==0) )
         {
            httpContext.GX_msglist.addItem(endTrnMsgTxt, endTrnMsgCod, 0, "", true);
         }
         /* Execute user event: After Trn */
         e12012 ();
         trnEnded = 0 ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( isIns( )  )
         {
            /* Clear variables for new insertion. */
            initAll013( ) ;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
         }
      }
      endTrnMsgTxt = "" ;
   }

   public String toString( )
   {
      return "" ;
   }

   public GXContentInfo getContentInfo( )
   {
      return (GXContentInfo)(null) ;
   }

   public void disable_std_buttons( )
   {
      bttBtn_delete_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", GXutil.ltrimstr( DecimalUtil.doubleToDec(bttBtn_delete_Visible), 5, 0), true);
      bttBtn_first_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", GXutil.ltrimstr( DecimalUtil.doubleToDec(bttBtn_first_Visible), 5, 0), true);
      bttBtn_previous_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", GXutil.ltrimstr( DecimalUtil.doubleToDec(bttBtn_previous_Visible), 5, 0), true);
      bttBtn_next_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", GXutil.ltrimstr( DecimalUtil.doubleToDec(bttBtn_next_Visible), 5, 0), true);
      bttBtn_last_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", GXutil.ltrimstr( DecimalUtil.doubleToDec(bttBtn_last_Visible), 5, 0), true);
      bttBtn_select_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", GXutil.ltrimstr( DecimalUtil.doubleToDec(bttBtn_select_Visible), 5, 0), true);
      if ( isDsp( ) || isDlt( ) )
      {
         bttBtn_delete_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", GXutil.ltrimstr( DecimalUtil.doubleToDec(bttBtn_delete_Visible), 5, 0), true);
         if ( isDsp( ) )
         {
            bttBtn_enter_Visible = 0 ;
            httpContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", GXutil.ltrimstr( DecimalUtil.doubleToDec(bttBtn_enter_Visible), 5, 0), true);
         }
         disableAttributes013( ) ;
      }
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( isDlt( ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_confdelete"), 0, "", true);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_mustconfirm"), 0, "", true);
         }
      }
   }

   public void confirm_010( )
   {
      beforeValidate013( ) ;
      if ( AnyError == 0 )
      {
         if ( isDlt( ) )
         {
            onDeleteControls013( ) ;
         }
         else
         {
            checkExtendedTable013( ) ;
            closeExtendedTableCursors013( ) ;
         }
      }
      if ( AnyError == 0 )
      {
         IsConfirmed = (short)(1) ;
         httpContext.ajax_rsp_assign_attri("", false, "IsConfirmed", GXutil.ltrimstr( DecimalUtil.doubleToDec(IsConfirmed), 4, 0));
      }
   }

   public void resetCaption010( )
   {
   }

   public void e11012( )
   {
      /* Start Routine */
      returnInSub = false ;
      if ( ! new com.oea5pruebadeconcepto.isauthorized(remoteHandle, context).executeUdp( AV14Pgmname) )
      {
         Cond_result = true ;
      }
      else
      {
         Cond_result = false ;
      }
      if ( Cond_result )
      {
         callWebObject(formatLink("com.oea5pruebadeconcepto.notauthorized", new String[] {GXutil.URLEncode(GXutil.rtrim(AV14Pgmname))}, new String[] {"GxObject"}) );
         httpContext.wjLocDisableFrm = (byte)(1) ;
      }
      AV10TrnContext.fromxml(AV11WebSession.getValue("TrnContext"), null, null);
      AV12Insert_PartieRoleCode = "" ;
      httpContext.ajax_rsp_assign_attri("", false, "AV12Insert_PartieRoleCode", AV12Insert_PartieRoleCode);
      if ( ( GXutil.strcmp(AV10TrnContext.getgxTv_SdtTransactionContext_Transactionname(), AV14Pgmname) == 0 ) && ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         AV15GXV1 = 1 ;
         httpContext.ajax_rsp_assign_attri("", false, "AV15GXV1", GXutil.ltrimstr( DecimalUtil.doubleToDec(AV15GXV1), 8, 0));
         while ( AV15GXV1 <= AV10TrnContext.getgxTv_SdtTransactionContext_Attributes().size() )
         {
            AV13TrnContextAtt = (com.oea5pruebadeconcepto.SdtTransactionContext_Attribute)((com.oea5pruebadeconcepto.SdtTransactionContext_Attribute)AV10TrnContext.getgxTv_SdtTransactionContext_Attributes().elementAt(-1+AV15GXV1));
            if ( GXutil.strcmp(AV13TrnContextAtt.getgxTv_SdtTransactionContext_Attribute_Attributename(), "PartieRoleCode") == 0 )
            {
               AV12Insert_PartieRoleCode = AV13TrnContextAtt.getgxTv_SdtTransactionContext_Attribute_Attributevalue() ;
               httpContext.ajax_rsp_assign_attri("", false, "AV12Insert_PartieRoleCode", AV12Insert_PartieRoleCode);
            }
            AV15GXV1 = (int)(AV15GXV1+1) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV15GXV1", GXutil.ltrimstr( DecimalUtil.doubleToDec(AV15GXV1), 8, 0));
         }
      }
      if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
      {
         bttBtn_enter_Caption = "Eliminar" ;
         httpContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Caption", bttBtn_enter_Caption, true);
         bttBtn_enter_Tooltiptext = "Eliminar" ;
         httpContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Tooltiptext", bttBtn_enter_Tooltiptext, true);
      }
   }

   public void e12012( )
   {
      /* After Trn Routine */
      returnInSub = false ;
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) && ! AV10TrnContext.getgxTv_SdtTransactionContext_Callerondelete() )
      {
         callWebObject(formatLink("com.oea5pruebadeconcepto.wwaeomasterdata", new String[] {}, new String[] {}) );
         httpContext.wjLocDisableFrm = (byte)(1) ;
      }
      httpContext.setWebReturnParms(new Object[] {});
      httpContext.setWebReturnParmsMetadata(new Object[] {});
      httpContext.wjLocDisableFrm = (byte)(1) ;
      httpContext.nUserReturn = (byte)(1) ;
      pr_default.close(3);
      pr_default.close(2);
      pr_default.close(1);
      returnInSub = true;
      if (true) return;
   }

   public void zm013( int GX_JID )
   {
      if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
      {
         if ( ! isIns( ) )
         {
            Z6PartieName = T00013_A6PartieName[0] ;
            Z7PartieShortName = T00013_A7PartieShortName[0] ;
            Z8PartieRoleCode = T00013_A8PartieRoleCode[0] ;
         }
         else
         {
            Z6PartieName = A6PartieName ;
            Z7PartieShortName = A7PartieShortName ;
            Z8PartieRoleCode = A8PartieRoleCode ;
         }
      }
      if ( GX_JID == -12 )
      {
         Z5PartieID = A5PartieID ;
         Z6PartieName = A6PartieName ;
         Z7PartieShortName = A7PartieShortName ;
         Z8PartieRoleCode = A8PartieRoleCode ;
         Z1IdentificationIssuingCountryCo = A1IdentificationIssuingCountryCo ;
         Z4IdentificationIssuingCountryNa = A4IdentificationIssuingCountryNa ;
      }
   }

   public void standaloneNotModal( )
   {
      bttBtn_delete_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", GXutil.ltrimstr( DecimalUtil.doubleToDec(bttBtn_delete_Enabled), 5, 0), true);
      if ( ! (GXutil.strcmp("", AV7IdentificationIssuingCountryCode)==0) )
      {
         A1IdentificationIssuingCountryCo = AV7IdentificationIssuingCountryCode ;
         httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
      }
      if ( ! (GXutil.strcmp("", AV7IdentificationIssuingCountryCode)==0) )
      {
         dynIdentificationIssuingCountryCo.setEnabled( 0 );
         httpContext.ajax_rsp_assign_prop("", false, dynIdentificationIssuingCountryCo.getInternalname(), "Enabled", GXutil.ltrimstr( dynIdentificationIssuingCountryCo.getEnabled(), 5, 0), true);
      }
      else
      {
         dynIdentificationIssuingCountryCo.setEnabled( 1 );
         httpContext.ajax_rsp_assign_prop("", false, dynIdentificationIssuingCountryCo.getInternalname(), "Enabled", GXutil.ltrimstr( dynIdentificationIssuingCountryCo.getEnabled(), 5, 0), true);
      }
      if ( ! (GXutil.strcmp("", AV7IdentificationIssuingCountryCode)==0) )
      {
         dynIdentificationIssuingCountryCo.setEnabled( 0 );
         httpContext.ajax_rsp_assign_prop("", false, dynIdentificationIssuingCountryCo.getInternalname(), "Enabled", GXutil.ltrimstr( dynIdentificationIssuingCountryCo.getEnabled(), 5, 0), true);
      }
      if ( ! (GXutil.strcmp("", AV8PartieID)==0) )
      {
         A5PartieID = AV8PartieID ;
         httpContext.ajax_rsp_assign_attri("", false, "A5PartieID", A5PartieID);
      }
      if ( ! (GXutil.strcmp("", AV8PartieID)==0) )
      {
         edtPartieID_Enabled = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, edtPartieID_Internalname, "Enabled", GXutil.ltrimstr( DecimalUtil.doubleToDec(edtPartieID_Enabled), 5, 0), true);
      }
      else
      {
         edtPartieID_Enabled = 1 ;
         httpContext.ajax_rsp_assign_prop("", false, edtPartieID_Internalname, "Enabled", GXutil.ltrimstr( DecimalUtil.doubleToDec(edtPartieID_Enabled), 5, 0), true);
      }
      if ( ! (GXutil.strcmp("", AV8PartieID)==0) )
      {
         edtPartieID_Enabled = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, edtPartieID_Internalname, "Enabled", GXutil.ltrimstr( DecimalUtil.doubleToDec(edtPartieID_Enabled), 5, 0), true);
      }
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ! (GXutil.strcmp("", AV12Insert_PartieRoleCode)==0) )
      {
         dynPartieRoleCode.setEnabled( 0 );
         httpContext.ajax_rsp_assign_prop("", false, dynPartieRoleCode.getInternalname(), "Enabled", GXutil.ltrimstr( dynPartieRoleCode.getEnabled(), 5, 0), true);
      }
      else
      {
         dynPartieRoleCode.setEnabled( 1 );
         httpContext.ajax_rsp_assign_prop("", false, dynPartieRoleCode.getInternalname(), "Enabled", GXutil.ltrimstr( dynPartieRoleCode.getEnabled(), 5, 0), true);
      }
   }

   public void standaloneModal( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ! (GXutil.strcmp("", AV12Insert_PartieRoleCode)==0) )
      {
         A8PartieRoleCode = AV12Insert_PartieRoleCode ;
         httpContext.ajax_rsp_assign_attri("", false, "A8PartieRoleCode", A8PartieRoleCode);
      }
      if ( GXutil.strcmp(Gx_mode, "DSP") == 0 )
      {
         bttBtn_enter_Enabled = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", GXutil.ltrimstr( DecimalUtil.doubleToDec(bttBtn_enter_Enabled), 5, 0), true);
      }
      else
      {
         bttBtn_enter_Enabled = 1 ;
         httpContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", GXutil.ltrimstr( DecimalUtil.doubleToDec(bttBtn_enter_Enabled), 5, 0), true);
      }
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
      {
         /* Using cursor T00015 */
         pr_default.execute(3, new Object[] {A1IdentificationIssuingCountryCo});
         A4IdentificationIssuingCountryNa = T00015_A4IdentificationIssuingCountryNa[0] ;
         pr_default.close(3);
         AV14Pgmname = "AEOMasterData" ;
         httpContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
      }
   }

   public void load013( )
   {
      /* Using cursor T00016 */
      pr_default.execute(4, new Object[] {A1IdentificationIssuingCountryCo, A5PartieID});
      if ( (pr_default.getStatus(4) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A4IdentificationIssuingCountryNa = T00016_A4IdentificationIssuingCountryNa[0] ;
         A6PartieName = T00016_A6PartieName[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A6PartieName", A6PartieName);
         A7PartieShortName = T00016_A7PartieShortName[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A7PartieShortName", A7PartieShortName);
         A8PartieRoleCode = T00016_A8PartieRoleCode[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A8PartieRoleCode", A8PartieRoleCode);
         zm013( -12) ;
      }
      pr_default.close(4);
      onLoadActions013( ) ;
   }

   public void onLoadActions013( )
   {
      AV14Pgmname = "AEOMasterData" ;
      httpContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
   }

   public void checkExtendedTable013( )
   {
      nIsDirty_3 = (short)(0) ;
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      AV14Pgmname = "AEOMasterData" ;
      httpContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
      /* Using cursor T00015 */
      pr_default.execute(3, new Object[] {A1IdentificationIssuingCountryCo});
      if ( (pr_default.getStatus(3) == 101) )
      {
         httpContext.GX_msglist.addItem("No existe 'AEOMaster Data Country'.", "ForeignKeyNotFound", 1, "IDENTIFICATIONISSUINGCOUNTRYCO");
         AnyError = (short)(1) ;
         GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      A4IdentificationIssuingCountryNa = T00015_A4IdentificationIssuingCountryNa[0] ;
      pr_default.close(3);
      /* Using cursor T00014 */
      pr_default.execute(2, new Object[] {A8PartieRoleCode});
      if ( (pr_default.getStatus(2) == 101) )
      {
         httpContext.GX_msglist.addItem("No existe 'Partie Role'.", "ForeignKeyNotFound", 1, "PARTIEROLECODE");
         AnyError = (short)(1) ;
         GX_FocusControl = dynPartieRoleCode.getInternalname() ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      pr_default.close(2);
   }

   public void closeExtendedTableCursors013( )
   {
      pr_default.close(3);
      pr_default.close(2);
   }

   public void enableDisable( )
   {
   }

   public void gxload_14( String A1IdentificationIssuingCountryCo )
   {
      /* Using cursor T00017 */
      pr_default.execute(5, new Object[] {A1IdentificationIssuingCountryCo});
      if ( (pr_default.getStatus(5) == 101) )
      {
         httpContext.GX_msglist.addItem("No existe 'AEOMaster Data Country'.", "ForeignKeyNotFound", 1, "IDENTIFICATIONISSUINGCOUNTRYCO");
         AnyError = (short)(1) ;
         GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      A4IdentificationIssuingCountryNa = T00017_A4IdentificationIssuingCountryNa[0] ;
      com.oea5pruebadeconcepto.GxWebStd.set_html_headers( httpContext, 0, "", "");
      addString( "[[") ;
      addString( "\""+PrivateUtilities.encodeJSConstant( A4IdentificationIssuingCountryNa)+"\"") ;
      addString( "]") ;
      if ( (pr_default.getStatus(5) == 101) )
      {
         addString( ",") ;
         addString( "101") ;
      }
      addString( "]") ;
      pr_default.close(5);
   }

   public void gxload_13( String A8PartieRoleCode )
   {
      /* Using cursor T00018 */
      pr_default.execute(6, new Object[] {A8PartieRoleCode});
      if ( (pr_default.getStatus(6) == 101) )
      {
         httpContext.GX_msglist.addItem("No existe 'Partie Role'.", "ForeignKeyNotFound", 1, "PARTIEROLECODE");
         AnyError = (short)(1) ;
         GX_FocusControl = dynPartieRoleCode.getInternalname() ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      com.oea5pruebadeconcepto.GxWebStd.set_html_headers( httpContext, 0, "", "");
      addString( "[[") ;
      addString( "]") ;
      if ( (pr_default.getStatus(6) == 101) )
      {
         addString( ",") ;
         addString( "101") ;
      }
      addString( "]") ;
      pr_default.close(6);
   }

   public void getKey013( )
   {
      /* Using cursor T00019 */
      pr_default.execute(7, new Object[] {A1IdentificationIssuingCountryCo, A5PartieID});
      if ( (pr_default.getStatus(7) != 101) )
      {
         RcdFound3 = (short)(1) ;
      }
      else
      {
         RcdFound3 = (short)(0) ;
      }
      pr_default.close(7);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T00013 */
      pr_default.execute(1, new Object[] {A1IdentificationIssuingCountryCo, A5PartieID});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm013( 12) ;
         RcdFound3 = (short)(1) ;
         A5PartieID = T00013_A5PartieID[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A5PartieID", A5PartieID);
         A6PartieName = T00013_A6PartieName[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A6PartieName", A6PartieName);
         A7PartieShortName = T00013_A7PartieShortName[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A7PartieShortName", A7PartieShortName);
         A8PartieRoleCode = T00013_A8PartieRoleCode[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A8PartieRoleCode", A8PartieRoleCode);
         A1IdentificationIssuingCountryCo = T00013_A1IdentificationIssuingCountryCo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
         Z1IdentificationIssuingCountryCo = A1IdentificationIssuingCountryCo ;
         Z5PartieID = A5PartieID ;
         sMode3 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         load013( ) ;
         if ( AnyError == 1 )
         {
            RcdFound3 = (short)(0) ;
            initializeNonKey013( ) ;
         }
         Gx_mode = sMode3 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      else
      {
         RcdFound3 = (short)(0) ;
         initializeNonKey013( ) ;
         sMode3 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         standaloneModal( ) ;
         Gx_mode = sMode3 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey013( ) ;
      if ( RcdFound3 == 0 )
      {
      }
      else
      {
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound3 = (short)(0) ;
      /* Using cursor T000110 */
      pr_default.execute(8, new Object[] {A1IdentificationIssuingCountryCo, A1IdentificationIssuingCountryCo, A5PartieID});
      if ( (pr_default.getStatus(8) != 101) )
      {
         while ( (pr_default.getStatus(8) != 101) && ( ( GXutil.strcmp(T000110_A1IdentificationIssuingCountryCo[0], A1IdentificationIssuingCountryCo) < 0 ) || ( GXutil.strcmp(T000110_A1IdentificationIssuingCountryCo[0], A1IdentificationIssuingCountryCo) == 0 ) && ( GXutil.strcmp(T000110_A5PartieID[0], A5PartieID) < 0 ) ) )
         {
            pr_default.readNext(8);
         }
         if ( (pr_default.getStatus(8) != 101) && ( ( GXutil.strcmp(T000110_A1IdentificationIssuingCountryCo[0], A1IdentificationIssuingCountryCo) > 0 ) || ( GXutil.strcmp(T000110_A1IdentificationIssuingCountryCo[0], A1IdentificationIssuingCountryCo) == 0 ) && ( GXutil.strcmp(T000110_A5PartieID[0], A5PartieID) > 0 ) ) )
         {
            A1IdentificationIssuingCountryCo = T000110_A1IdentificationIssuingCountryCo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
            A5PartieID = T000110_A5PartieID[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A5PartieID", A5PartieID);
            RcdFound3 = (short)(1) ;
         }
      }
      pr_default.close(8);
   }

   public void move_previous( )
   {
      RcdFound3 = (short)(0) ;
      /* Using cursor T000111 */
      pr_default.execute(9, new Object[] {A1IdentificationIssuingCountryCo, A1IdentificationIssuingCountryCo, A5PartieID});
      if ( (pr_default.getStatus(9) != 101) )
      {
         while ( (pr_default.getStatus(9) != 101) && ( ( GXutil.strcmp(T000111_A1IdentificationIssuingCountryCo[0], A1IdentificationIssuingCountryCo) > 0 ) || ( GXutil.strcmp(T000111_A1IdentificationIssuingCountryCo[0], A1IdentificationIssuingCountryCo) == 0 ) && ( GXutil.strcmp(T000111_A5PartieID[0], A5PartieID) > 0 ) ) )
         {
            pr_default.readNext(9);
         }
         if ( (pr_default.getStatus(9) != 101) && ( ( GXutil.strcmp(T000111_A1IdentificationIssuingCountryCo[0], A1IdentificationIssuingCountryCo) < 0 ) || ( GXutil.strcmp(T000111_A1IdentificationIssuingCountryCo[0], A1IdentificationIssuingCountryCo) == 0 ) && ( GXutil.strcmp(T000111_A5PartieID[0], A5PartieID) < 0 ) ) )
         {
            A1IdentificationIssuingCountryCo = T000111_A1IdentificationIssuingCountryCo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
            A5PartieID = T000111_A5PartieID[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A5PartieID", A5PartieID);
            RcdFound3 = (short)(1) ;
         }
      }
      pr_default.close(9);
   }

   public void btn_enter( )
   {
      nKeyPressed = (byte)(1) ;
      getKey013( ) ;
      if ( isIns( ) )
      {
         /* Insert record */
         GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         insert013( ) ;
         if ( AnyError == 1 )
         {
            GX_FocusControl = "" ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }
      else
      {
         if ( RcdFound3 == 1 )
         {
            if ( ( GXutil.strcmp(A1IdentificationIssuingCountryCo, Z1IdentificationIssuingCountryCo) != 0 ) || ( GXutil.strcmp(A5PartieID, Z5PartieID) != 0 ) )
            {
               A1IdentificationIssuingCountryCo = Z1IdentificationIssuingCountryCo ;
               httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
               A5PartieID = Z5PartieID ;
               httpContext.ajax_rsp_assign_attri("", false, "A5PartieID", A5PartieID);
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "CandidateKeyNotFound", 1, "IDENTIFICATIONISSUINGCOUNTRYCO");
               AnyError = (short)(1) ;
               GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            else if ( isDlt( ) )
            {
               delete( ) ;
               afterTrn( ) ;
               GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            else
            {
               /* Update record */
               update013( ) ;
               GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( ( GXutil.strcmp(A1IdentificationIssuingCountryCo, Z1IdentificationIssuingCountryCo) != 0 ) || ( GXutil.strcmp(A5PartieID, Z5PartieID) != 0 ) )
            {
               /* Insert record */
               GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               insert013( ) ;
               if ( AnyError == 1 )
               {
                  GX_FocusControl = "" ;
                  httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( GXutil.strcmp(Gx_mode, "UPD") == 0 )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_recdeleted"), 1, "IDENTIFICATIONISSUINGCOUNTRYCO");
                  AnyError = (short)(1) ;
                  GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
                  httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Insert record */
                  GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
                  httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  insert013( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "" ;
                     httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
            }
         }
      }
      afterTrn( ) ;
      if ( isUpd( ) || isDlt( ) )
      {
         if ( AnyError == 0 )
         {
            httpContext.nUserReturn = (byte)(1) ;
         }
      }
   }

   public void btn_delete( )
   {
      if ( ( GXutil.strcmp(A1IdentificationIssuingCountryCo, Z1IdentificationIssuingCountryCo) != 0 ) || ( GXutil.strcmp(A5PartieID, Z5PartieID) != 0 ) )
      {
         A1IdentificationIssuingCountryCo = Z1IdentificationIssuingCountryCo ;
         httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
         A5PartieID = Z5PartieID ;
         httpContext.ajax_rsp_assign_attri("", false, "A5PartieID", A5PartieID);
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforedlt"), 1, "IDENTIFICATIONISSUINGCOUNTRYCO");
         AnyError = (short)(1) ;
         GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      else
      {
         delete( ) ;
         afterTrn( ) ;
         GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError != 0 )
      {
      }
   }

   public void checkOptimisticConcurrency013( )
   {
      if ( ! isIns( ) )
      {
         /* Using cursor T00012 */
         pr_default.execute(0, new Object[] {A1IdentificationIssuingCountryCo, A5PartieID});
         if ( (pr_default.getStatus(0) == 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"AEOMasterData"}), "RecordIsLocked", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z6PartieName, T00012_A6PartieName[0]) != 0 ) || ( GXutil.strcmp(Z7PartieShortName, T00012_A7PartieShortName[0]) != 0 ) || ( GXutil.strcmp(Z8PartieRoleCode, T00012_A8PartieRoleCode[0]) != 0 ) )
         {
            if ( GXutil.strcmp(Z6PartieName, T00012_A6PartieName[0]) != 0 )
            {
               GXutil.writeLogln("aeomasterdata:[seudo value changed for attri]"+"PartieName");
               GXutil.writeLogRaw("Old: ",Z6PartieName);
               GXutil.writeLogRaw("Current: ",T00012_A6PartieName[0]);
            }
            if ( GXutil.strcmp(Z7PartieShortName, T00012_A7PartieShortName[0]) != 0 )
            {
               GXutil.writeLogln("aeomasterdata:[seudo value changed for attri]"+"PartieShortName");
               GXutil.writeLogRaw("Old: ",Z7PartieShortName);
               GXutil.writeLogRaw("Current: ",T00012_A7PartieShortName[0]);
            }
            if ( GXutil.strcmp(Z8PartieRoleCode, T00012_A8PartieRoleCode[0]) != 0 )
            {
               GXutil.writeLogln("aeomasterdata:[seudo value changed for attri]"+"PartieRoleCode");
               GXutil.writeLogRaw("Old: ",Z8PartieRoleCode);
               GXutil.writeLogRaw("Current: ",T00012_A8PartieRoleCode[0]);
            }
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_waschg", new Object[] {"AEOMasterData"}), "RecordWasChanged", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert013( )
   {
      beforeValidate013( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable013( ) ;
      }
      if ( AnyError == 0 )
      {
         zm013( 0) ;
         checkOptimisticConcurrency013( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm013( ) ;
            if ( AnyError == 0 )
            {
               beforeInsert013( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000112 */
                  pr_default.execute(10, new Object[] {A5PartieID, A6PartieName, A7PartieShortName, A8PartieRoleCode, A1IdentificationIssuingCountryCo});
                  Application.getSmartCacheProvider(remoteHandle).setUpdated("AEOMasterData");
                  if ( (pr_default.getStatus(10) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "");
                     AnyError = (short)(1) ;
                  }
                  if ( AnyError == 0 )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( AnyError == 0 )
                     {
                        /* Save values for previous() function. */
                        endTrnMsgTxt = localUtil.getMessages().getMessage("GXM_sucadded") ;
                        endTrnMsgCod = "SuccessfullyAdded" ;
                        resetCaption010( ) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load013( ) ;
         }
         endLevel013( ) ;
      }
      closeExtendedTableCursors013( ) ;
   }

   public void update013( )
   {
      beforeValidate013( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable013( ) ;
      }
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency013( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm013( ) ;
            if ( AnyError == 0 )
            {
               beforeUpdate013( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000113 */
                  pr_default.execute(11, new Object[] {A6PartieName, A7PartieShortName, A8PartieRoleCode, A1IdentificationIssuingCountryCo, A5PartieID});
                  Application.getSmartCacheProvider(remoteHandle).setUpdated("AEOMasterData");
                  if ( (pr_default.getStatus(11) == 103) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"AEOMasterData"}), "RecordIsLocked", 1, "");
                     AnyError = (short)(1) ;
                  }
                  deferredUpdate013( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( AnyError == 0 )
                     {
                        if ( isUpd( ) || isDlt( ) )
                        {
                           if ( AnyError == 0 )
                           {
                              httpContext.nUserReturn = (byte)(1) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel013( ) ;
      }
      closeExtendedTableCursors013( ) ;
   }

   public void deferredUpdate013( )
   {
   }

   public void delete( )
   {
      beforeValidate013( ) ;
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency013( ) ;
      }
      if ( AnyError == 0 )
      {
         onDeleteControls013( ) ;
         afterConfirm013( ) ;
         if ( AnyError == 0 )
         {
            beforeDelete013( ) ;
            if ( AnyError == 0 )
            {
               /* No cascading delete specified. */
               /* Using cursor T000114 */
               pr_default.execute(12, new Object[] {A1IdentificationIssuingCountryCo, A5PartieID});
               Application.getSmartCacheProvider(remoteHandle).setUpdated("AEOMasterData");
               if ( AnyError == 0 )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( AnyError == 0 )
                  {
                     if ( isUpd( ) || isDlt( ) )
                     {
                        if ( AnyError == 0 )
                        {
                           httpContext.nUserReturn = (byte)(1) ;
                        }
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode3 = Gx_mode ;
      Gx_mode = "DLT" ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      endLevel013( ) ;
      Gx_mode = sMode3 ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
   }

   public void onDeleteControls013( )
   {
      standaloneModal( ) ;
      if ( AnyError == 0 )
      {
         /* Delete mode formulas */
         AV14Pgmname = "AEOMasterData" ;
         httpContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         /* Using cursor T000115 */
         pr_default.execute(13, new Object[] {A1IdentificationIssuingCountryCo});
         A4IdentificationIssuingCountryNa = T000115_A4IdentificationIssuingCountryNa[0] ;
         pr_default.close(13);
      }
   }

   public void endLevel013( )
   {
      if ( ! isIns( ) )
      {
         pr_default.close(0);
      }
      if ( AnyError == 0 )
      {
         beforeComplete013( ) ;
      }
      if ( AnyError == 0 )
      {
         Application.commitDataStores(context, remoteHandle, pr_default, "aeomasterdata");
         if ( AnyError == 0 )
         {
            confirmValues010( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         Application.rollbackDataStores(context, remoteHandle, pr_default, "aeomasterdata");
      }
      IsModified = (short)(0) ;
      if ( AnyError != 0 )
      {
         httpContext.wjLoc = "" ;
         httpContext.nUserReturn = (byte)(0) ;
      }
   }

   public void scanStart013( )
   {
      /* Scan By routine */
      /* Using cursor T000116 */
      pr_default.execute(14);
      RcdFound3 = (short)(0) ;
      if ( (pr_default.getStatus(14) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A1IdentificationIssuingCountryCo = T000116_A1IdentificationIssuingCountryCo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
         A5PartieID = T000116_A5PartieID[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A5PartieID", A5PartieID);
      }
      /* Load Subordinate Levels */
   }

   public void scanNext013( )
   {
      /* Scan next routine */
      pr_default.readNext(14);
      RcdFound3 = (short)(0) ;
      if ( (pr_default.getStatus(14) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A1IdentificationIssuingCountryCo = T000116_A1IdentificationIssuingCountryCo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
         A5PartieID = T000116_A5PartieID[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A5PartieID", A5PartieID);
      }
   }

   public void scanEnd013( )
   {
      pr_default.close(14);
   }

   public void afterConfirm013( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert013( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate013( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete013( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete013( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate013( )
   {
      /* Before Validate Rules */
   }

   public void disableAttributes013( )
   {
      dynIdentificationIssuingCountryCo.setEnabled( 0 );
      httpContext.ajax_rsp_assign_prop("", false, dynIdentificationIssuingCountryCo.getInternalname(), "Enabled", GXutil.ltrimstr( dynIdentificationIssuingCountryCo.getEnabled(), 5, 0), true);
      edtPartieID_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtPartieID_Internalname, "Enabled", GXutil.ltrimstr( DecimalUtil.doubleToDec(edtPartieID_Enabled), 5, 0), true);
      edtPartieName_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtPartieName_Internalname, "Enabled", GXutil.ltrimstr( DecimalUtil.doubleToDec(edtPartieName_Enabled), 5, 0), true);
      edtPartieShortName_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtPartieShortName_Internalname, "Enabled", GXutil.ltrimstr( DecimalUtil.doubleToDec(edtPartieShortName_Enabled), 5, 0), true);
      dynPartieRoleCode.setEnabled( 0 );
      httpContext.ajax_rsp_assign_prop("", false, dynPartieRoleCode.getInternalname(), "Enabled", GXutil.ltrimstr( dynPartieRoleCode.getEnabled(), 5, 0), true);
   }

   public void send_integrity_lvl_hashes013( )
   {
   }

   public void assign_properties_default( )
   {
   }

   public void confirmValues010( )
   {
   }

   public void renderHtmlHeaders( )
   {
      com.oea5pruebadeconcepto.GxWebStd.gx_html_headers( httpContext, 0, "", "", Form.getMeta(), Form.getMetaequiv(), true);
   }

   public void renderHtmlOpenForm( )
   {
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.enableOutput();
      }
      httpContext.writeText( "<title>") ;
      httpContext.writeValue( Form.getCaption()) ;
      httpContext.writeTextNL( "</title>") ;
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( GXutil.len( sDynURL) > 0 )
      {
         httpContext.writeText( "<BASE href=\""+sDynURL+"\" />") ;
      }
      define_styles( ) ;
      MasterPageObj.master_styles();
      if ( ( ( httpContext.getBrowserType( ) == 1 ) || ( httpContext.getBrowserType( ) == 5 ) ) && ( GXutil.strcmp(httpContext.getBrowserVersion( ), "7.0") == 0 ) )
      {
         httpContext.AddJavascriptSource("json2.js", "?"+httpContext.getBuildNumber( 2240200), false, true);
      }
      httpContext.AddJavascriptSource("jquery.js", "?"+httpContext.getBuildNumber( 2240200), false, true);
      httpContext.AddJavascriptSource("gxgral.js", "?"+httpContext.getBuildNumber( 2240200), false, true);
      httpContext.AddJavascriptSource("gxcfg.js", "?202232411201846", false, true);
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.enableOutput();
      }
      httpContext.writeText( Form.getHeaderrawhtml()) ;
      httpContext.closeHtmlHeader();
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.disableOutput();
      }
      FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"" ;
      httpContext.writeText( "<body ") ;
      bodyStyle = "" + "background-color:" + WebUtils.getHTMLColor( Form.getIBackground()) + ";color:" + WebUtils.getHTMLColor( Form.getTextcolor()) + ";" ;
      bodyStyle += "-moz-opacity:0;opacity:0;" ;
      if ( ! ( (GXutil.strcmp("", Form.getBackground())==0) ) )
      {
         bodyStyle += " background-image:url(" + httpContext.convertURL( Form.getBackground()) + ")" ;
      }
      httpContext.writeText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
      httpContext.writeText( FormProcess+">") ;
      httpContext.skipLines( 1 );
      httpContext.writeTextNL( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("com.oea5pruebadeconcepto.aeomasterdata", new String[] {GXutil.URLEncode(GXutil.rtrim(Gx_mode)),GXutil.URLEncode(GXutil.rtrim(AV7IdentificationIssuingCountryCode)),GXutil.URLEncode(GXutil.rtrim(AV8PartieID))}, new String[] {"Gx_mode","IdentificationIssuingCountryCode","PartieID"}) +"\">") ;
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "_EventName", "");
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "_EventGridId", "");
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "_EventRowId", "");
      httpContext.writeText( "<input type=\"submit\" title=\"submit\" style=\"display:block;height:0;border:0;padding:0\" disabled>") ;
      httpContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
      toggleJsOutput = httpContext.isJsOutputEnabled( ) ;
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.disableJsOutput();
      }
   }

   public void send_integrity_footer_hashes( )
   {
      GXKey = httpContext.decrypt64( httpContext.getCookie( "GX_SESSION_ID"), context.getServerKey( )) ;
      forbiddenHiddens = new com.genexus.util.GXProperties() ;
      forbiddenHiddens.add("hshsalt", "hsh"+"AEOMasterData");
      forbiddenHiddens.add("Gx_mode", GXutil.rtrim( localUtil.format( Gx_mode, "@!")));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "hsh", httpContext.getEncryptedSignature( forbiddenHiddens.toString(), GXKey));
      GXutil.writeLogInfo("aeomasterdata:[ SendSecurityCheck value for]"+forbiddenHiddens.toJSonString());
   }

   public void sendCloseFormHiddens( )
   {
      /* Send hidden variables. */
      /* Send saved values. */
      send_integrity_footer_hashes( ) ;
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "Z1IdentificationIssuingCountryCo", GXutil.rtrim( Z1IdentificationIssuingCountryCo));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "Z5PartieID", GXutil.rtrim( Z5PartieID));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "Z6PartieName", Z6PartieName);
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "Z7PartieShortName", Z7PartieShortName);
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "Z8PartieRoleCode", GXutil.rtrim( Z8PartieRoleCode));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "IsConfirmed", GXutil.ltrim( localUtil.ntoc( IsConfirmed, (byte)(4), (byte)(0), ",", "")));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "IsModified", GXutil.ltrim( localUtil.ntoc( IsModified, (byte)(4), (byte)(0), ",", "")));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "Mode", GXutil.rtrim( Gx_mode));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "gxhash_Mode", getSecureSignedToken( "", GXutil.rtrim( localUtil.format( Gx_mode, "@!"))));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "N8PartieRoleCode", GXutil.rtrim( A8PartieRoleCode));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "vMODE", GXutil.rtrim( Gx_mode));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "gxhash_vMODE", getSecureSignedToken( "", GXutil.rtrim( localUtil.format( Gx_mode, "@!"))));
      if ( httpContext.isAjaxRequest( ) )
      {
         httpContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV10TrnContext);
      }
      else
      {
         httpContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV10TrnContext);
      }
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "gxhash_vTRNCONTEXT", getSecureSignedToken( "", AV10TrnContext));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "vIDENTIFICATIONISSUINGCOUNTRYCODE", GXutil.rtrim( AV7IdentificationIssuingCountryCode));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "gxhash_vIDENTIFICATIONISSUINGCOUNTRYCODE", getSecureSignedToken( "", GXutil.rtrim( localUtil.format( AV7IdentificationIssuingCountryCode, ""))));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "vPARTIEID", GXutil.rtrim( AV8PartieID));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "gxhash_vPARTIEID", getSecureSignedToken( "", GXutil.rtrim( localUtil.format( AV8PartieID, ""))));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "vINSERT_PARTIEROLECODE", GXutil.rtrim( AV12Insert_PartieRoleCode));
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "IDENTIFICATIONISSUINGCOUNTRYNA", A4IdentificationIssuingCountryNa);
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "vPGMNAME", GXutil.rtrim( AV14Pgmname));
   }

   public void renderHtmlCloseForm( )
   {
      sendCloseFormHiddens( ) ;
      com.oea5pruebadeconcepto.GxWebStd.gx_hidden_field( httpContext, "GX_FocusControl", GX_FocusControl);
      httpContext.SendAjaxEncryptionKey();
      sendSecurityToken(sPrefix);
      httpContext.SendComponentObjects();
      httpContext.SendServerCommands();
      httpContext.SendState();
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.disableOutput();
      }
      httpContext.writeTextNL( "</form>") ;
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.enableOutput();
      }
      include_jscripts( ) ;
   }

   public byte executeStartEvent( )
   {
      standaloneStartup( ) ;
      gxajaxcallmode = (byte)((isAjaxCallMode( ) ? 1 : 0)) ;
      return gxajaxcallmode ;
   }

   public void renderHtmlContent( )
   {
      httpContext.writeText( "<div") ;
      com.oea5pruebadeconcepto.GxWebStd.classAttribute( httpContext, "gx-ct-body"+" "+((GXutil.strcmp("", Form.getThemeClass())==0) ? "form-horizontal Form" : Form.getThemeClass())+"-fx");
      httpContext.writeText( ">") ;
      draw( ) ;
      httpContext.writeText( "</div>") ;
   }

   public void dispatchEvents( )
   {
      process( ) ;
   }

   public boolean hasEnterEvent( )
   {
      return true ;
   }

   public com.genexus.webpanels.GXWebForm getForm( )
   {
      return Form ;
   }

   public String getSelfLink( )
   {
      return formatLink("com.oea5pruebadeconcepto.aeomasterdata", new String[] {GXutil.URLEncode(GXutil.rtrim(Gx_mode)),GXutil.URLEncode(GXutil.rtrim(AV7IdentificationIssuingCountryCode)),GXutil.URLEncode(GXutil.rtrim(AV8PartieID))}, new String[] {"Gx_mode","IdentificationIssuingCountryCode","PartieID"})  ;
   }

   public String getPgmname( )
   {
      return "AEOMasterData" ;
   }

   public String getPgmdesc( )
   {
      return "AEO Master Data" ;
   }

   public void initializeNonKey013( )
   {
      A8PartieRoleCode = "" ;
      httpContext.ajax_rsp_assign_attri("", false, "A8PartieRoleCode", A8PartieRoleCode);
      A4IdentificationIssuingCountryNa = "" ;
      httpContext.ajax_rsp_assign_attri("", false, "A4IdentificationIssuingCountryNa", A4IdentificationIssuingCountryNa);
      A6PartieName = "" ;
      httpContext.ajax_rsp_assign_attri("", false, "A6PartieName", A6PartieName);
      A7PartieShortName = "" ;
      httpContext.ajax_rsp_assign_attri("", false, "A7PartieShortName", A7PartieShortName);
      Z6PartieName = "" ;
      Z7PartieShortName = "" ;
      Z8PartieRoleCode = "" ;
   }

   public void initAll013( )
   {
      A1IdentificationIssuingCountryCo = "" ;
      httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
      A5PartieID = "" ;
      httpContext.ajax_rsp_assign_attri("", false, "A5PartieID", A5PartieID);
      initializeNonKey013( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void define_styles( )
   {
      httpContext.AddThemeStyleSheetFile("", context.getHttpContext().getTheme( )+".css", "?"+httpContext.getCacheInvalidationToken( ));
      boolean outputEnabled = httpContext.isOutputEnabled( );
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.enableOutput();
      }
      idxLst = 1 ;
      while ( idxLst <= Form.getJscriptsrc().getCount() )
      {
         httpContext.AddJavascriptSource(GXutil.rtrim( Form.getJscriptsrc().item(idxLst)), "?202232411201859", true, true);
         idxLst = (int)(idxLst+1) ;
      }
      if ( ! outputEnabled )
      {
         if ( httpContext.isSpaRequest( ) )
         {
            httpContext.disableOutput();
         }
      }
      /* End function define_styles */
   }

   public void include_jscripts( )
   {
      httpContext.AddJavascriptSource("messages.spa.js", "?"+httpContext.getCacheInvalidationToken( ), false, true);
      httpContext.AddJavascriptSource("aeomasterdata.js", "?202232411201860", false, true);
      /* End function include_jscripts */
   }

   public void init_default_properties( )
   {
      lblTitle_Internalname = "TITLE" ;
      divTitlecontainer_Internalname = "TITLECONTAINER" ;
      bttBtn_first_Internalname = "BTN_FIRST" ;
      bttBtn_previous_Internalname = "BTN_PREVIOUS" ;
      bttBtn_next_Internalname = "BTN_NEXT" ;
      bttBtn_last_Internalname = "BTN_LAST" ;
      bttBtn_select_Internalname = "BTN_SELECT" ;
      divToolbarcell_Internalname = "TOOLBARCELL" ;
      dynIdentificationIssuingCountryCo.setInternalname( "IDENTIFICATIONISSUINGCOUNTRYCO" );
      edtPartieID_Internalname = "PARTIEID" ;
      edtPartieName_Internalname = "PARTIENAME" ;
      edtPartieShortName_Internalname = "PARTIESHORTNAME" ;
      dynPartieRoleCode.setInternalname( "PARTIEROLECODE" );
      divFormcontainer_Internalname = "FORMCONTAINER" ;
      bttBtn_enter_Internalname = "BTN_ENTER" ;
      bttBtn_cancel_Internalname = "BTN_CANCEL" ;
      bttBtn_delete_Internalname = "BTN_DELETE" ;
      divMaintable_Internalname = "MAINTABLE" ;
      Form.setInternalname( "FORM" );
   }

   public void initialize_properties( )
   {
      httpContext.setAjaxOnSessionTimeout(ajaxOnSessionTimeout());
      httpContext.setDefaultTheme("Carmine");
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.disableJsOutput();
      }
      init_default_properties( ) ;
      Form.setHeaderrawhtml( "" );
      Form.setBackground( "" );
      Form.setTextcolor( 0 );
      Form.setIBackground( (int)(0xFFFFFF) );
      Form.setCaption( "AEO Master Data" );
      bttBtn_delete_Enabled = 0 ;
      bttBtn_delete_Visible = 1 ;
      bttBtn_cancel_Visible = 1 ;
      bttBtn_enter_Tooltiptext = "Confirmar" ;
      bttBtn_enter_Caption = "Confirmar" ;
      bttBtn_enter_Enabled = 1 ;
      bttBtn_enter_Visible = 1 ;
      dynPartieRoleCode.setJsonclick( "" );
      dynPartieRoleCode.setEnabled( 1 );
      edtPartieShortName_Jsonclick = "" ;
      edtPartieShortName_Enabled = 1 ;
      edtPartieName_Jsonclick = "" ;
      edtPartieName_Enabled = 1 ;
      edtPartieID_Jsonclick = "" ;
      edtPartieID_Enabled = 1 ;
      dynIdentificationIssuingCountryCo.setJsonclick( "" );
      dynIdentificationIssuingCountryCo.setEnabled( 1 );
      bttBtn_select_Visible = 1 ;
      bttBtn_last_Visible = 1 ;
      bttBtn_next_Visible = 1 ;
      bttBtn_previous_Visible = 1 ;
      bttBtn_first_Visible = 1 ;
      httpContext.GX_msglist.setDisplaymode( (short)(1) );
      if ( httpContext.isSpaRequest( ) )
      {
         httpContext.enableJsOutput();
      }
   }

   public void dynload_actions( )
   {
      /* End function dynload_actions */
   }

   public void gxdlapartierolecode011( )
   {
      if ( ! httpContext.isAjaxRequest( ) )
      {
         httpContext.GX_webresponse.addHeader("Cache-Control", "no-store");
      }
      addString( "[[") ;
      gxdlapartierolecode_data011( ) ;
      gxdynajaxindex = 1 ;
      while ( gxdynajaxindex <= gxdynajaxctrlcodr.getCount() )
      {
         addString( gxwrpcisep+"{\"c\":\""+PrivateUtilities.encodeJSConstant( gxdynajaxctrlcodr.item(gxdynajaxindex))+"\",\"d\":\""+PrivateUtilities.encodeJSConstant( gxdynajaxctrldescr.item(gxdynajaxindex))+"\"}") ;
         gxdynajaxindex = (int)(gxdynajaxindex+1) ;
         gxwrpcisep = "," ;
      }
      addString( "]") ;
      if ( gxdynajaxctrlcodr.getCount() == 0 )
      {
         addString( ",101") ;
      }
      addString( "]") ;
   }

   public void gxapartierolecode_html011( )
   {
      String gxdynajaxvalue;
      gxdlapartierolecode_data011( ) ;
      gxdynajaxindex = 1 ;
      if ( ! ( gxdyncontrolsrefreshing && httpContext.isAjaxRequest( ) ) )
      {
         dynPartieRoleCode.removeAllItems();
      }
      while ( gxdynajaxindex <= gxdynajaxctrlcodr.getCount() )
      {
         gxdynajaxvalue = gxdynajaxctrlcodr.item(gxdynajaxindex) ;
         dynPartieRoleCode.addItem(gxdynajaxvalue, gxdynajaxctrldescr.item(gxdynajaxindex), (short)(0));
         gxdynajaxindex = (int)(gxdynajaxindex+1) ;
      }
   }

   protected void gxdlapartierolecode_data011( )
   {
      gxdynajaxctrlcodr.removeAllItems();
      gxdynajaxctrldescr.removeAllItems();
      /* Using cursor T000117 */
      pr_default.execute(15);
      while ( (pr_default.getStatus(15) != 101) )
      {
         gxdynajaxctrlcodr.add(GXutil.rtrim( T000117_A8PartieRoleCode[0]));
         gxdynajaxctrldescr.add(T000117_A9PartieRoleName[0]);
         pr_default.readNext(15);
      }
      pr_default.close(15);
   }

   public String rest_partierolecode( com.genexus.internet.StringCollection colParms )
   {
      String jsonString;
      gxdlapartierolecode_data011( ) ;
      jsonString = GXutil.stringCollectionsToJson( gxdynajaxctrlcodr, gxdynajaxctrldescr) ;
      /* End function rest_PARTIEROLECODE */
      return jsonString ;
   }

   public void gxdlaidentificationissuingcountryco011( )
   {
      if ( ! httpContext.isAjaxRequest( ) )
      {
         httpContext.GX_webresponse.addHeader("Cache-Control", "no-store");
      }
      addString( "[[") ;
      gxdlaidentificationissuingcountryco_data011( ) ;
      gxdynajaxindex = 1 ;
      while ( gxdynajaxindex <= gxdynajaxctrlcodr.getCount() )
      {
         addString( gxwrpcisep+"{\"c\":\""+PrivateUtilities.encodeJSConstant( gxdynajaxctrlcodr.item(gxdynajaxindex))+"\",\"d\":\""+PrivateUtilities.encodeJSConstant( gxdynajaxctrldescr.item(gxdynajaxindex))+"\"}") ;
         gxdynajaxindex = (int)(gxdynajaxindex+1) ;
         gxwrpcisep = "," ;
      }
      addString( "]") ;
      if ( gxdynajaxctrlcodr.getCount() == 0 )
      {
         addString( ",101") ;
      }
      addString( "]") ;
   }

   public void gxaidentificationissuingcountryco_html011( )
   {
      String gxdynajaxvalue;
      gxdlaidentificationissuingcountryco_data011( ) ;
      gxdynajaxindex = 1 ;
      if ( ! ( gxdyncontrolsrefreshing && httpContext.isAjaxRequest( ) ) )
      {
         dynIdentificationIssuingCountryCo.removeAllItems();
      }
      while ( gxdynajaxindex <= gxdynajaxctrlcodr.getCount() )
      {
         gxdynajaxvalue = gxdynajaxctrlcodr.item(gxdynajaxindex) ;
         dynIdentificationIssuingCountryCo.addItem(gxdynajaxvalue, gxdynajaxctrldescr.item(gxdynajaxindex), (short)(0));
         gxdynajaxindex = (int)(gxdynajaxindex+1) ;
      }
   }

   protected void gxdlaidentificationissuingcountryco_data011( )
   {
      gxdynajaxctrlcodr.removeAllItems();
      gxdynajaxctrldescr.removeAllItems();
      /* Using cursor T000118 */
      pr_default.execute(16);
      while ( (pr_default.getStatus(16) != 101) )
      {
         gxdynajaxctrlcodr.add(GXutil.rtrim( T000118_A2CountryCode[0]));
         gxdynajaxctrldescr.add(T000118_A3CountryName[0]);
         pr_default.readNext(16);
      }
      pr_default.close(16);
   }

   public String rest_identificationissuingcountryco( com.genexus.internet.StringCollection colParms )
   {
      String jsonString;
      gxdlaidentificationissuingcountryco_data011( ) ;
      jsonString = GXutil.stringCollectionsToJson( gxdynajaxctrlcodr, gxdynajaxctrldescr) ;
      /* End function rest_IDENTIFICATIONISSUINGCOUNTRYCO */
      return jsonString ;
   }

   public void init_web_controls( )
   {
      dynIdentificationIssuingCountryCo.setName( "IDENTIFICATIONISSUINGCOUNTRYCO" );
      dynIdentificationIssuingCountryCo.setWebtags( "" );
      dynIdentificationIssuingCountryCo.removeAllItems();
      /* Using cursor T000119 */
      pr_default.execute(17);
      while ( (pr_default.getStatus(17) != 101) )
      {
         dynIdentificationIssuingCountryCo.addItem(T000119_A2CountryCode[0], T000119_A3CountryName[0], (short)(0));
         pr_default.readNext(17);
      }
      pr_default.close(17);
      if ( dynIdentificationIssuingCountryCo.getItemCount() > 0 )
      {
         A1IdentificationIssuingCountryCo = dynIdentificationIssuingCountryCo.getValidValue(A1IdentificationIssuingCountryCo) ;
         httpContext.ajax_rsp_assign_attri("", false, "A1IdentificationIssuingCountryCo", A1IdentificationIssuingCountryCo);
      }
      dynPartieRoleCode.setName( "PARTIEROLECODE" );
      dynPartieRoleCode.setWebtags( "" );
      dynPartieRoleCode.removeAllItems();
      /* Using cursor T000120 */
      pr_default.execute(18);
      while ( (pr_default.getStatus(18) != 101) )
      {
         dynPartieRoleCode.addItem(T000120_A8PartieRoleCode[0], T000120_A9PartieRoleName[0], (short)(0));
         pr_default.readNext(18);
      }
      pr_default.close(18);
      if ( dynPartieRoleCode.getItemCount() > 0 )
      {
         A8PartieRoleCode = dynPartieRoleCode.getValidValue(A8PartieRoleCode) ;
         httpContext.ajax_rsp_assign_attri("", false, "A8PartieRoleCode", A8PartieRoleCode);
      }
      /* End function init_web_controls */
   }

   public boolean isIns( )
   {
      return ((GXutil.strcmp(Gx_mode, "INS")==0) ? true : false) ;
   }

   public boolean isDlt( )
   {
      return ((GXutil.strcmp(Gx_mode, "DLT")==0) ? true : false) ;
   }

   public boolean isUpd( )
   {
      return ((GXutil.strcmp(Gx_mode, "UPD")==0) ? true : false) ;
   }

   public boolean isDsp( )
   {
      return ((GXutil.strcmp(Gx_mode, "DSP")==0) ? true : false) ;
   }

   public void valid_Identificationissuingcountryco( )
   {
      A8PartieRoleCode = dynPartieRoleCode.getValue() ;
      A1IdentificationIssuingCountryCo = dynIdentificationIssuingCountryCo.getValue() ;
      /* Using cursor T000115 */
      pr_default.execute(13, new Object[] {A1IdentificationIssuingCountryCo});
      if ( (pr_default.getStatus(13) == 101) )
      {
         httpContext.GX_msglist.addItem("No existe 'AEOMaster Data Country'.", "ForeignKeyNotFound", 1, "IDENTIFICATIONISSUINGCOUNTRYCO");
         AnyError = (short)(1) ;
         GX_FocusControl = dynIdentificationIssuingCountryCo.getInternalname() ;
      }
      A4IdentificationIssuingCountryNa = T000115_A4IdentificationIssuingCountryNa[0] ;
      pr_default.close(13);
      dynload_actions( ) ;
      if ( dynIdentificationIssuingCountryCo.getItemCount() > 0 )
      {
         A1IdentificationIssuingCountryCo = dynIdentificationIssuingCountryCo.getValidValue(A1IdentificationIssuingCountryCo) ;
      }
      if ( httpContext.isAjaxRequest( ) )
      {
         dynIdentificationIssuingCountryCo.setValue( GXutil.rtrim( A1IdentificationIssuingCountryCo) );
      }
      /*  Sending validation outputs */
      httpContext.ajax_rsp_assign_attri("", false, "A4IdentificationIssuingCountryNa", A4IdentificationIssuingCountryNa);
   }

   public void valid_Partierolecode( )
   {
      A8PartieRoleCode = dynPartieRoleCode.getValue() ;
      A1IdentificationIssuingCountryCo = dynIdentificationIssuingCountryCo.getValue() ;
      /* Using cursor T000121 */
      pr_default.execute(19, new Object[] {A8PartieRoleCode});
      if ( (pr_default.getStatus(19) == 101) )
      {
         httpContext.GX_msglist.addItem("No existe 'Partie Role'.", "ForeignKeyNotFound", 1, "PARTIEROLECODE");
         AnyError = (short)(1) ;
         GX_FocusControl = dynPartieRoleCode.getInternalname() ;
      }
      pr_default.close(19);
      dynload_actions( ) ;
      if ( dynIdentificationIssuingCountryCo.getItemCount() > 0 )
      {
         A1IdentificationIssuingCountryCo = dynIdentificationIssuingCountryCo.getValidValue(A1IdentificationIssuingCountryCo) ;
      }
      if ( httpContext.isAjaxRequest( ) )
      {
         dynIdentificationIssuingCountryCo.setValue( GXutil.rtrim( A1IdentificationIssuingCountryCo) );
      }
      /*  Sending validation outputs */
   }

   public boolean supportAjaxEvent( )
   {
      return true ;
   }

   public void initializeDynEvents( )
   {
      setEventMetadata("ENTER","{handler:'userMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7IdentificationIssuingCountryCode',fld:'vIDENTIFICATIONISSUINGCOUNTRYCODE',pic:'',hsh:true},{av:'AV8PartieID',fld:'vPARTIEID',pic:'',hsh:true},{av:'dynPartieRoleCode'},{av:'A8PartieRoleCode',fld:'PARTIEROLECODE',pic:''},{av:'dynIdentificationIssuingCountryCo'},{av:'A1IdentificationIssuingCountryCo',fld:'IDENTIFICATIONISSUINGCOUNTRYCO',pic:''}]");
      setEventMetadata("ENTER",",oparms:[{av:'dynPartieRoleCode'},{av:'A8PartieRoleCode',fld:'PARTIEROLECODE',pic:''},{av:'dynIdentificationIssuingCountryCo'},{av:'A1IdentificationIssuingCountryCo',fld:'IDENTIFICATIONISSUINGCOUNTRYCO',pic:''}]}");
      setEventMetadata("REFRESH","{handler:'refresh',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV10TrnContext',fld:'vTRNCONTEXT',pic:'',hsh:true},{av:'AV7IdentificationIssuingCountryCode',fld:'vIDENTIFICATIONISSUINGCOUNTRYCODE',pic:'',hsh:true},{av:'AV8PartieID',fld:'vPARTIEID',pic:'',hsh:true},{av:'dynPartieRoleCode'},{av:'A8PartieRoleCode',fld:'PARTIEROLECODE',pic:''},{av:'dynIdentificationIssuingCountryCo'},{av:'A1IdentificationIssuingCountryCo',fld:'IDENTIFICATIONISSUINGCOUNTRYCO',pic:''}]");
      setEventMetadata("REFRESH",",oparms:[{av:'dynPartieRoleCode'},{av:'A8PartieRoleCode',fld:'PARTIEROLECODE',pic:''},{av:'dynIdentificationIssuingCountryCo'},{av:'A1IdentificationIssuingCountryCo',fld:'IDENTIFICATIONISSUINGCOUNTRYCO',pic:''}]}");
      setEventMetadata("AFTER TRN","{handler:'e12012',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV10TrnContext',fld:'vTRNCONTEXT',pic:'',hsh:true},{av:'dynPartieRoleCode'},{av:'A8PartieRoleCode',fld:'PARTIEROLECODE',pic:''},{av:'dynIdentificationIssuingCountryCo'},{av:'A1IdentificationIssuingCountryCo',fld:'IDENTIFICATIONISSUINGCOUNTRYCO',pic:''}]");
      setEventMetadata("AFTER TRN",",oparms:[{av:'dynPartieRoleCode'},{av:'A8PartieRoleCode',fld:'PARTIEROLECODE',pic:''},{av:'dynIdentificationIssuingCountryCo'},{av:'A1IdentificationIssuingCountryCo',fld:'IDENTIFICATIONISSUINGCOUNTRYCO',pic:''}]}");
      setEventMetadata("VALID_IDENTIFICATIONISSUINGCOUNTRYCO","{handler:'valid_Identificationissuingcountryco',iparms:[{av:'A4IdentificationIssuingCountryNa',fld:'IDENTIFICATIONISSUINGCOUNTRYNA',pic:''},{av:'dynPartieRoleCode'},{av:'A8PartieRoleCode',fld:'PARTIEROLECODE',pic:''},{av:'dynIdentificationIssuingCountryCo'},{av:'A1IdentificationIssuingCountryCo',fld:'IDENTIFICATIONISSUINGCOUNTRYCO',pic:''}]");
      setEventMetadata("VALID_IDENTIFICATIONISSUINGCOUNTRYCO",",oparms:[{av:'A4IdentificationIssuingCountryNa',fld:'IDENTIFICATIONISSUINGCOUNTRYNA',pic:''},{av:'dynPartieRoleCode'},{av:'A8PartieRoleCode',fld:'PARTIEROLECODE',pic:''},{av:'dynIdentificationIssuingCountryCo'},{av:'A1IdentificationIssuingCountryCo',fld:'IDENTIFICATIONISSUINGCOUNTRYCO',pic:''}]}");
      setEventMetadata("VALID_PARTIEID","{handler:'valid_Partieid',iparms:[{av:'dynPartieRoleCode'},{av:'A8PartieRoleCode',fld:'PARTIEROLECODE',pic:''},{av:'dynIdentificationIssuingCountryCo'},{av:'A1IdentificationIssuingCountryCo',fld:'IDENTIFICATIONISSUINGCOUNTRYCO',pic:''}]");
      setEventMetadata("VALID_PARTIEID",",oparms:[{av:'dynPartieRoleCode'},{av:'A8PartieRoleCode',fld:'PARTIEROLECODE',pic:''},{av:'dynIdentificationIssuingCountryCo'},{av:'A1IdentificationIssuingCountryCo',fld:'IDENTIFICATIONISSUINGCOUNTRYCO',pic:''}]}");
      setEventMetadata("VALID_PARTIEROLECODE","{handler:'valid_Partierolecode',iparms:[{av:'dynPartieRoleCode'},{av:'A8PartieRoleCode',fld:'PARTIEROLECODE',pic:''},{av:'dynIdentificationIssuingCountryCo'},{av:'A1IdentificationIssuingCountryCo',fld:'IDENTIFICATIONISSUINGCOUNTRYCO',pic:''}]");
      setEventMetadata("VALID_PARTIEROLECODE",",oparms:[{av:'dynPartieRoleCode'},{av:'A8PartieRoleCode',fld:'PARTIEROLECODE',pic:''},{av:'dynIdentificationIssuingCountryCo'},{av:'A1IdentificationIssuingCountryCo',fld:'IDENTIFICATIONISSUINGCOUNTRYCO',pic:''}]}");
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

   protected String IntegratedSecurityPermissionPrefix( )
   {
      return "";
   }

   protected String EncryptURLParameters( )
   {
      return "NO";
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
      pr_default.close(19);
      pr_default.close(13);
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      sPrefix = "" ;
      wcpOGx_mode = "" ;
      wcpOAV7IdentificationIssuingCountryCode = "" ;
      wcpOAV8PartieID = "" ;
      Z1IdentificationIssuingCountryCo = "" ;
      Z5PartieID = "" ;
      Z6PartieName = "" ;
      Z7PartieShortName = "" ;
      Z8PartieRoleCode = "" ;
      N8PartieRoleCode = "" ;
      scmdbuf = "" ;
      gxfirstwebparm = "" ;
      gxfirstwebparm_bkp = "" ;
      A1IdentificationIssuingCountryCo = "" ;
      A8PartieRoleCode = "" ;
      Gx_mode = "" ;
      AV7IdentificationIssuingCountryCode = "" ;
      AV8PartieID = "" ;
      GXKey = "" ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Form = new com.genexus.webpanels.GXWebForm();
      GX_FocusControl = "" ;
      lblTitle_Jsonclick = "" ;
      ClassString = "" ;
      StyleString = "" ;
      TempTags = "" ;
      bttBtn_first_Jsonclick = "" ;
      bttBtn_previous_Jsonclick = "" ;
      bttBtn_next_Jsonclick = "" ;
      bttBtn_last_Jsonclick = "" ;
      bttBtn_select_Jsonclick = "" ;
      A5PartieID = "" ;
      A6PartieName = "" ;
      A7PartieShortName = "" ;
      bttBtn_enter_Jsonclick = "" ;
      bttBtn_cancel_Jsonclick = "" ;
      bttBtn_delete_Jsonclick = "" ;
      AV12Insert_PartieRoleCode = "" ;
      A4IdentificationIssuingCountryNa = "" ;
      AV14Pgmname = "" ;
      forbiddenHiddens = new com.genexus.util.GXProperties();
      hsh = "" ;
      sMode3 = "" ;
      sEvt = "" ;
      EvtGridId = "" ;
      EvtRowId = "" ;
      sEvtType = "" ;
      endTrnMsgTxt = "" ;
      endTrnMsgCod = "" ;
      AV10TrnContext = new com.oea5pruebadeconcepto.SdtTransactionContext(remoteHandle, context);
      AV11WebSession = httpContext.getWebSession();
      AV13TrnContextAtt = new com.oea5pruebadeconcepto.SdtTransactionContext_Attribute(remoteHandle, context);
      Z4IdentificationIssuingCountryNa = "" ;
      T00015_A4IdentificationIssuingCountryNa = new String[] {""} ;
      T00016_A5PartieID = new String[] {""} ;
      T00016_A4IdentificationIssuingCountryNa = new String[] {""} ;
      T00016_A6PartieName = new String[] {""} ;
      T00016_A7PartieShortName = new String[] {""} ;
      T00016_A8PartieRoleCode = new String[] {""} ;
      T00016_A1IdentificationIssuingCountryCo = new String[] {""} ;
      T00014_A8PartieRoleCode = new String[] {""} ;
      T00017_A4IdentificationIssuingCountryNa = new String[] {""} ;
      T00018_A8PartieRoleCode = new String[] {""} ;
      T00019_A1IdentificationIssuingCountryCo = new String[] {""} ;
      T00019_A5PartieID = new String[] {""} ;
      T00013_A5PartieID = new String[] {""} ;
      T00013_A6PartieName = new String[] {""} ;
      T00013_A7PartieShortName = new String[] {""} ;
      T00013_A8PartieRoleCode = new String[] {""} ;
      T00013_A1IdentificationIssuingCountryCo = new String[] {""} ;
      T000110_A1IdentificationIssuingCountryCo = new String[] {""} ;
      T000110_A5PartieID = new String[] {""} ;
      T000111_A1IdentificationIssuingCountryCo = new String[] {""} ;
      T000111_A5PartieID = new String[] {""} ;
      T00012_A5PartieID = new String[] {""} ;
      T00012_A6PartieName = new String[] {""} ;
      T00012_A7PartieShortName = new String[] {""} ;
      T00012_A8PartieRoleCode = new String[] {""} ;
      T00012_A1IdentificationIssuingCountryCo = new String[] {""} ;
      T000115_A4IdentificationIssuingCountryNa = new String[] {""} ;
      T000116_A1IdentificationIssuingCountryCo = new String[] {""} ;
      T000116_A5PartieID = new String[] {""} ;
      sDynURL = "" ;
      FormProcess = "" ;
      bodyStyle = "" ;
      gxdynajaxctrlcodr = new com.genexus.internet.StringCollection();
      gxdynajaxctrldescr = new com.genexus.internet.StringCollection();
      gxwrpcisep = "" ;
      T000117_A8PartieRoleCode = new String[] {""} ;
      T000117_A9PartieRoleName = new String[] {""} ;
      T000118_A2CountryCode = new String[] {""} ;
      T000118_A3CountryName = new String[] {""} ;
      T000119_A2CountryCode = new String[] {""} ;
      T000119_A3CountryName = new String[] {""} ;
      T000120_A8PartieRoleCode = new String[] {""} ;
      T000120_A9PartieRoleName = new String[] {""} ;
      T000121_A8PartieRoleCode = new String[] {""} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new com.oea5pruebadeconcepto.aeomasterdata__default(),
         new Object[] {
             new Object[] {
            T00012_A5PartieID, T00012_A6PartieName, T00012_A7PartieShortName, T00012_A8PartieRoleCode, T00012_A1IdentificationIssuingCountryCo
            }
            , new Object[] {
            T00013_A5PartieID, T00013_A6PartieName, T00013_A7PartieShortName, T00013_A8PartieRoleCode, T00013_A1IdentificationIssuingCountryCo
            }
            , new Object[] {
            T00014_A8PartieRoleCode
            }
            , new Object[] {
            T00015_A4IdentificationIssuingCountryNa
            }
            , new Object[] {
            T00016_A5PartieID, T00016_A4IdentificationIssuingCountryNa, T00016_A6PartieName, T00016_A7PartieShortName, T00016_A8PartieRoleCode, T00016_A1IdentificationIssuingCountryCo
            }
            , new Object[] {
            T00017_A4IdentificationIssuingCountryNa
            }
            , new Object[] {
            T00018_A8PartieRoleCode
            }
            , new Object[] {
            T00019_A1IdentificationIssuingCountryCo, T00019_A5PartieID
            }
            , new Object[] {
            T000110_A1IdentificationIssuingCountryCo, T000110_A5PartieID
            }
            , new Object[] {
            T000111_A1IdentificationIssuingCountryCo, T000111_A5PartieID
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T000115_A4IdentificationIssuingCountryNa
            }
            , new Object[] {
            T000116_A1IdentificationIssuingCountryCo, T000116_A5PartieID
            }
            , new Object[] {
            T000117_A8PartieRoleCode, T000117_A9PartieRoleName
            }
            , new Object[] {
            T000118_A2CountryCode, T000118_A3CountryName
            }
            , new Object[] {
            T000119_A2CountryCode, T000119_A3CountryName
            }
            , new Object[] {
            T000120_A8PartieRoleCode, T000120_A9PartieRoleName
            }
            , new Object[] {
            T000121_A8PartieRoleCode
            }
         }
      );
      AV14Pgmname = "AEOMasterData" ;
   }

   private byte GxWebError ;
   private byte nKeyPressed ;
   private byte Gx_BScreen ;
   private byte gxajaxcallmode ;
   private short gxcookieaux ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short RcdFound3 ;
   private short nIsDirty_3 ;
   private int trnEnded ;
   private int bttBtn_first_Visible ;
   private int bttBtn_previous_Visible ;
   private int bttBtn_next_Visible ;
   private int bttBtn_last_Visible ;
   private int bttBtn_select_Visible ;
   private int edtPartieID_Enabled ;
   private int edtPartieName_Enabled ;
   private int edtPartieShortName_Enabled ;
   private int bttBtn_enter_Visible ;
   private int bttBtn_enter_Enabled ;
   private int bttBtn_cancel_Visible ;
   private int bttBtn_delete_Visible ;
   private int bttBtn_delete_Enabled ;
   private int AV15GXV1 ;
   private int GX_JID ;
   private int idxLst ;
   private int gxdynajaxindex ;
   private String sPrefix ;
   private String wcpOGx_mode ;
   private String wcpOAV7IdentificationIssuingCountryCode ;
   private String wcpOAV8PartieID ;
   private String Z1IdentificationIssuingCountryCo ;
   private String Z5PartieID ;
   private String Z8PartieRoleCode ;
   private String N8PartieRoleCode ;
   private String scmdbuf ;
   private String gxfirstwebparm ;
   private String gxfirstwebparm_bkp ;
   private String A1IdentificationIssuingCountryCo ;
   private String A8PartieRoleCode ;
   private String Gx_mode ;
   private String AV7IdentificationIssuingCountryCode ;
   private String AV8PartieID ;
   private String GXKey ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String GX_FocusControl ;
   private String divMaintable_Internalname ;
   private String divTitlecontainer_Internalname ;
   private String lblTitle_Internalname ;
   private String lblTitle_Jsonclick ;
   private String ClassString ;
   private String StyleString ;
   private String divFormcontainer_Internalname ;
   private String divToolbarcell_Internalname ;
   private String TempTags ;
   private String bttBtn_first_Internalname ;
   private String bttBtn_first_Jsonclick ;
   private String bttBtn_previous_Internalname ;
   private String bttBtn_previous_Jsonclick ;
   private String bttBtn_next_Internalname ;
   private String bttBtn_next_Jsonclick ;
   private String bttBtn_last_Internalname ;
   private String bttBtn_last_Jsonclick ;
   private String bttBtn_select_Internalname ;
   private String bttBtn_select_Jsonclick ;
   private String edtPartieID_Internalname ;
   private String A5PartieID ;
   private String edtPartieID_Jsonclick ;
   private String edtPartieName_Internalname ;
   private String edtPartieName_Jsonclick ;
   private String edtPartieShortName_Internalname ;
   private String edtPartieShortName_Jsonclick ;
   private String bttBtn_enter_Internalname ;
   private String bttBtn_enter_Caption ;
   private String bttBtn_enter_Jsonclick ;
   private String bttBtn_enter_Tooltiptext ;
   private String bttBtn_cancel_Internalname ;
   private String bttBtn_cancel_Jsonclick ;
   private String bttBtn_delete_Internalname ;
   private String bttBtn_delete_Jsonclick ;
   private String AV12Insert_PartieRoleCode ;
   private String AV14Pgmname ;
   private String hsh ;
   private String sMode3 ;
   private String sEvt ;
   private String EvtGridId ;
   private String EvtRowId ;
   private String sEvtType ;
   private String endTrnMsgTxt ;
   private String endTrnMsgCod ;
   private String sDynURL ;
   private String FormProcess ;
   private String bodyStyle ;
   private String gxwrpcisep ;
   private boolean entryPointCalled ;
   private boolean toggleJsOutput ;
   private boolean wbErr ;
   private boolean returnInSub ;
   private boolean Cond_result ;
   private boolean gxdyncontrolsrefreshing ;
   private String Z6PartieName ;
   private String Z7PartieShortName ;
   private String A6PartieName ;
   private String A7PartieShortName ;
   private String A4IdentificationIssuingCountryNa ;
   private String Z4IdentificationIssuingCountryNa ;
   private com.genexus.internet.StringCollection gxdynajaxctrlcodr ;
   private com.genexus.internet.StringCollection gxdynajaxctrldescr ;
   private com.genexus.webpanels.WebSession AV11WebSession ;
   private com.genexus.util.GXProperties forbiddenHiddens ;
   private HTMLChoice dynIdentificationIssuingCountryCo ;
   private HTMLChoice dynPartieRoleCode ;
   private IDataStoreProvider pr_default ;
   private String[] T00015_A4IdentificationIssuingCountryNa ;
   private String[] T00016_A5PartieID ;
   private String[] T00016_A4IdentificationIssuingCountryNa ;
   private String[] T00016_A6PartieName ;
   private String[] T00016_A7PartieShortName ;
   private String[] T00016_A8PartieRoleCode ;
   private String[] T00016_A1IdentificationIssuingCountryCo ;
   private String[] T00014_A8PartieRoleCode ;
   private String[] T00017_A4IdentificationIssuingCountryNa ;
   private String[] T00018_A8PartieRoleCode ;
   private String[] T00019_A1IdentificationIssuingCountryCo ;
   private String[] T00019_A5PartieID ;
   private String[] T00013_A5PartieID ;
   private String[] T00013_A6PartieName ;
   private String[] T00013_A7PartieShortName ;
   private String[] T00013_A8PartieRoleCode ;
   private String[] T00013_A1IdentificationIssuingCountryCo ;
   private String[] T000110_A1IdentificationIssuingCountryCo ;
   private String[] T000110_A5PartieID ;
   private String[] T000111_A1IdentificationIssuingCountryCo ;
   private String[] T000111_A5PartieID ;
   private String[] T00012_A5PartieID ;
   private String[] T00012_A6PartieName ;
   private String[] T00012_A7PartieShortName ;
   private String[] T00012_A8PartieRoleCode ;
   private String[] T00012_A1IdentificationIssuingCountryCo ;
   private String[] T000115_A4IdentificationIssuingCountryNa ;
   private String[] T000116_A1IdentificationIssuingCountryCo ;
   private String[] T000116_A5PartieID ;
   private String[] T000117_A8PartieRoleCode ;
   private String[] T000117_A9PartieRoleName ;
   private String[] T000118_A2CountryCode ;
   private String[] T000118_A3CountryName ;
   private String[] T000119_A2CountryCode ;
   private String[] T000119_A3CountryName ;
   private String[] T000120_A8PartieRoleCode ;
   private String[] T000120_A9PartieRoleName ;
   private String[] T000121_A8PartieRoleCode ;
   private com.genexus.webpanels.GXWebForm Form ;
   private com.oea5pruebadeconcepto.SdtTransactionContext AV10TrnContext ;
   private com.oea5pruebadeconcepto.SdtTransactionContext_Attribute AV13TrnContextAtt ;
}

final  class aeomasterdata__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T00012", "SELECT `PartieID`, `PartieName`, `PartieShortName`, `PartieRoleCode`, `IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo FROM `AEOMasterData` WHERE `IdentificationIssuingCountryCo` = ? AND `PartieID` = ?  FOR UPDATE ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T00013", "SELECT `PartieID`, `PartieName`, `PartieShortName`, `PartieRoleCode`, `IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo FROM `AEOMasterData` WHERE `IdentificationIssuingCountryCo` = ? AND `PartieID` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T00014", "SELECT `PartieRoleCode` FROM `PartieRole` WHERE `PartieRoleCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T00015", "SELECT `CountryName` AS IdentificationIssuingCountryNa FROM `Country` WHERE `CountryCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T00016", "SELECT TM1.`PartieID`, T2.`CountryName` AS IdentificationIssuingCountryNa, TM1.`PartieName`, TM1.`PartieShortName`, TM1.`PartieRoleCode`, TM1.`IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo FROM (`AEOMasterData` TM1 INNER JOIN `Country` T2 ON T2.`CountryCode` = TM1.`IdentificationIssuingCountryCo`) WHERE TM1.`IdentificationIssuingCountryCo` = ? and TM1.`PartieID` = ? ORDER BY TM1.`IdentificationIssuingCountryCo`, TM1.`PartieID` ",true, GX_NOMASK, false, this,100, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T00017", "SELECT `CountryName` AS IdentificationIssuingCountryNa FROM `Country` WHERE `CountryCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T00018", "SELECT `PartieRoleCode` FROM `PartieRole` WHERE `PartieRoleCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T00019", "SELECT `IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo, `PartieID` FROM `AEOMasterData` WHERE `IdentificationIssuingCountryCo` = ? AND `PartieID` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T000110", "SELECT `IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo, `PartieID` FROM `AEOMasterData` WHERE ( `IdentificationIssuingCountryCo` > ? or `IdentificationIssuingCountryCo` = ? and `PartieID` > ?) ORDER BY `IdentificationIssuingCountryCo`, `PartieID`  LIMIT 1",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,true )
         ,new ForEachCursor("T000111", "SELECT `IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo, `PartieID` FROM `AEOMasterData` WHERE ( `IdentificationIssuingCountryCo` < ? or `IdentificationIssuingCountryCo` = ? and `PartieID` < ?) ORDER BY `IdentificationIssuingCountryCo` DESC, `PartieID` DESC  LIMIT 1",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,true )
         ,new UpdateCursor("T000112", "INSERT INTO `AEOMasterData`(`PartieID`, `PartieName`, `PartieShortName`, `PartieRoleCode`, `IdentificationIssuingCountryCo`) VALUES(?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T000113", "UPDATE `AEOMasterData` SET `PartieName`=?, `PartieShortName`=?, `PartieRoleCode`=?  WHERE `IdentificationIssuingCountryCo` = ? AND `PartieID` = ?", GX_NOMASK)
         ,new UpdateCursor("T000114", "DELETE FROM `AEOMasterData`  WHERE `IdentificationIssuingCountryCo` = ? AND `PartieID` = ?", GX_NOMASK)
         ,new ForEachCursor("T000115", "SELECT `CountryName` AS IdentificationIssuingCountryNa FROM `Country` WHERE `CountryCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T000116", "SELECT `IdentificationIssuingCountryCo` AS IdentificationIssuingCountryCo, `PartieID` FROM `AEOMasterData` ORDER BY `IdentificationIssuingCountryCo`, `PartieID` ",true, GX_NOMASK, false, this,100, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T000117", "SELECT `PartieRoleCode`, `PartieRoleName` FROM `PartieRole` ORDER BY `PartieRoleName` ",true, GX_NOMASK, false, this,0, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T000118", "SELECT `CountryCode`, `CountryName` FROM `Country` ORDER BY `CountryName` ",true, GX_NOMASK, false, this,0, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T000119", "SELECT `CountryCode`, `CountryName` FROM `Country` ORDER BY `CountryName` ",true, GX_NOMASK, false, this,0, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T000120", "SELECT `PartieRoleCode`, `PartieRoleName` FROM `PartieRole` ORDER BY `PartieRoleName` ",true, GX_NOMASK, false, this,0, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("T000121", "SELECT `PartieRoleCode` FROM `PartieRole` WHERE `PartieRoleCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 30);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               ((String[]) buf[2])[0] = rslt.getVarchar(3);
               ((String[]) buf[3])[0] = rslt.getString(4, 2);
               ((String[]) buf[4])[0] = rslt.getString(5, 2);
               return;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 30);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               ((String[]) buf[2])[0] = rslt.getVarchar(3);
               ((String[]) buf[3])[0] = rslt.getString(4, 2);
               ((String[]) buf[4])[0] = rslt.getString(5, 2);
               return;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               return;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1);
               return;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getString(1, 30);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               ((String[]) buf[2])[0] = rslt.getVarchar(3);
               ((String[]) buf[3])[0] = rslt.getVarchar(4);
               ((String[]) buf[4])[0] = rslt.getString(5, 2);
               ((String[]) buf[5])[0] = rslt.getString(6, 2);
               return;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1);
               return;
            case 6 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               return;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getString(2, 30);
               return;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getString(2, 30);
               return;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getString(2, 30);
               return;
            case 13 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1);
               return;
            case 14 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getString(2, 30);
               return;
            case 15 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
            case 16 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
            case 17 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
            case 18 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
            case 19 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               return;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               return;
            case 1 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               return;
            case 2 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 3 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 4 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               return;
            case 5 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 6 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 7 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               return;
            case 8 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 2);
               stmt.setString(3, (String)parms[2], 30);
               return;
            case 9 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 2);
               stmt.setString(3, (String)parms[2], 30);
               return;
            case 10 :
               stmt.setString(1, (String)parms[0], 30);
               stmt.setVarchar(2, (String)parms[1], 50, false);
               stmt.setVarchar(3, (String)parms[2], 50, false);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 2);
               return;
            case 11 :
               stmt.setVarchar(1, (String)parms[0], 50, false);
               stmt.setVarchar(2, (String)parms[1], 50, false);
               stmt.setString(3, (String)parms[2], 2);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 30);
               return;
            case 12 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               return;
            case 13 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 19 :
               stmt.setString(1, (String)parms[0], 2);
               return;
      }
   }

}

