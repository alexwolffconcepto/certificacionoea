package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(urlPatterns = {"/servlet/com.oea5pruebadeconcepto.viewpartierole", "/com.oea5pruebadeconcepto.viewpartierole"})
public final  class viewpartierole extends GXWebObjectStub
{
   public viewpartierole( )
   {
   }

   public viewpartierole( int remoteHandle )
   {
      super(remoteHandle, new ModelContext( viewpartierole.class ));
   }

   public viewpartierole( int remoteHandle ,
                          ModelContext context )
   {
      super(remoteHandle, context);
   }

   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new viewpartierole_impl(context).doExecute();
   }

   protected void init( com.genexus.internet.HttpContext context )
   {
      new viewpartierole_impl(context).cleanup();
   }

   public String getServletInfo( )
   {
      return "View Partie Role";
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

   protected String IntegratedSecurityPermissionPrefix( )
   {
      return "";
   }

   protected String EncryptURLParameters( )
   {
      return "NO";
   }

}

