package com.oea5pruebadeconcepto ;
import com.genexus.*;

@javax.xml.bind.annotation.XmlType(name = "ColCountry", namespace ="OEA5pruebaDeConcepto")
public final  class StructSdtColCountry implements Cloneable, java.io.Serializable
{
   public StructSdtColCountry( )
   {
      this( -1, new ModelContext( StructSdtColCountry.class ));
   }

   public StructSdtColCountry( int remoteHandle ,
                               ModelContext context )
   {
   }

   public  StructSdtColCountry( java.util.Vector<StructSdtCountry> value )
   {
      item = value;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   @javax.xml.bind.annotation.XmlElement(name="Country",namespace="OEA5pruebaDeConcepto")
   public java.util.Vector<StructSdtCountry> getItem( )
   {
      return item;
   }

   public void setItem( java.util.Vector<StructSdtCountry> value )
   {
      item = value;
   }

   protected  java.util.Vector<StructSdtCountry> item = new java.util.Vector<>();
}

