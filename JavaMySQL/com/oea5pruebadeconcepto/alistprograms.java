package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(urlPatterns = {"/servlet/com.oea5pruebadeconcepto.alistprograms", "/com.oea5pruebadeconcepto.alistprograms"})
public final  class alistprograms extends GXWebObjectStub
{
   public alistprograms( )
   {
   }

   public alistprograms( int remoteHandle )
   {
      super(remoteHandle, new ModelContext( alistprograms.class ));
   }

   public alistprograms( int remoteHandle ,
                         ModelContext context )
   {
      super(remoteHandle, context);
   }

   public void executeCmdLine( String args[] )
   {
      @SuppressWarnings("unchecked")
      GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>[] aP0 = new GXBaseCollection[] {new GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>()};

      try
      {
      }
      catch ( ArrayIndexOutOfBoundsException e )
      {
      }

      execute(aP0);
   }

   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new alistprograms_impl(context).doExecute();
   }

   protected void init( com.genexus.internet.HttpContext context )
   {
      new alistprograms_impl(context).cleanup();
   }

   public void execute( GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>[] aP0 )
   {
      new alistprograms_impl(remoteHandle, context).execute_int(aP0);
   }

   @SuppressWarnings("unchecked")
   public GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName> executeUdp( )
   {
      GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>[] aP0 = new GXBaseCollection[] {new GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>()};
      execute(aP0);
      return aP0[0];
   }

   public String getServletInfo( )
   {
      return "List Programs";
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

   protected String IntegratedSecurityPermissionPrefix( )
   {
      return "";
   }

   protected String EncryptURLParameters( )
   {
      return "NO";
   }

}

