package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.fasterxml.jackson.annotation.*;
import java.util.*;

@javax.xml.bind.annotation.XmlType(name = "AEOMasterData", namespace ="OEA5pruebaDeConcepto")
@JsonPropertyOrder(alphabetic=true)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.NONE, getterVisibility=JsonAutoDetect.Visibility.NONE, isGetterVisibility=JsonAutoDetect.Visibility.NONE)
public final  class SdtAEOMasterData_RESTLInterface extends GxGenericCollectionItem<com.oea5pruebadeconcepto.SdtAEOMasterData>
{
   public SdtAEOMasterData_RESTLInterface( )
   {
      super(new com.oea5pruebadeconcepto.SdtAEOMasterData (-1));
   }

   public SdtAEOMasterData_RESTLInterface( com.oea5pruebadeconcepto.SdtAEOMasterData psdt )
   {
      super(psdt);
   }

   @GxSeudo
   @JsonProperty("IdentificationIssuingCountryName")
   public String getgxTv_SdtAEOMasterData_Identificationissuingcountryname( )
   {
      return GXutil.rtrim(((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).getgxTv_SdtAEOMasterData_Identificationissuingcountryname()) ;
   }

   @JsonProperty("IdentificationIssuingCountryName")
   public void setgxTv_SdtAEOMasterData_Identificationissuingcountryname(  String Value )
   {
      ((com.oea5pruebadeconcepto.SdtAEOMasterData)getSdt()).setgxTv_SdtAEOMasterData_Identificationissuingcountryname(Value);
   }


   @JsonProperty("uri")
   public String URI( )
   {
      return "" ;
   }

   private int startRow ;
   private int maxRows ;
}

