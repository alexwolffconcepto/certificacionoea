package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(urlPatterns = {"/servlet/com.oea5pruebadeconcepto.viewaeomasterdata", "/com.oea5pruebadeconcepto.viewaeomasterdata"})
public final  class viewaeomasterdata extends GXWebObjectStub
{
   public viewaeomasterdata( )
   {
   }

   public viewaeomasterdata( int remoteHandle )
   {
      super(remoteHandle, new ModelContext( viewaeomasterdata.class ));
   }

   public viewaeomasterdata( int remoteHandle ,
                             ModelContext context )
   {
      super(remoteHandle, context);
   }

   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new viewaeomasterdata_impl(context).doExecute();
   }

   protected void init( com.genexus.internet.HttpContext context )
   {
      new viewaeomasterdata_impl(context).cleanup();
   }

   public String getServletInfo( )
   {
      return "View AEOMaster Data";
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

   protected String IntegratedSecurityPermissionPrefix( )
   {
      return "";
   }

   protected String EncryptURLParameters( )
   {
      return "NO";
   }

}

