package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.fasterxml.jackson.annotation.*;
import java.util.*;

@javax.xml.bind.annotation.XmlType(name = "Country", namespace ="OEA5pruebaDeConcepto")
@JsonPropertyOrder(alphabetic=true)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.NONE, getterVisibility=JsonAutoDetect.Visibility.NONE, isGetterVisibility=JsonAutoDetect.Visibility.NONE)
public final  class SdtCountry_RESTLInterface extends GxGenericCollectionItem<com.oea5pruebadeconcepto.SdtCountry>
{
   public SdtCountry_RESTLInterface( )
   {
      super(new com.oea5pruebadeconcepto.SdtCountry (-1));
   }

   public SdtCountry_RESTLInterface( com.oea5pruebadeconcepto.SdtCountry psdt )
   {
      super(psdt);
   }

   @GxSeudo
   @JsonProperty("CountryName")
   public String getgxTv_SdtCountry_Countryname( )
   {
      return GXutil.rtrim(((com.oea5pruebadeconcepto.SdtCountry)getSdt()).getgxTv_SdtCountry_Countryname()) ;
   }

   @JsonProperty("CountryName")
   public void setgxTv_SdtCountry_Countryname(  String Value )
   {
      ((com.oea5pruebadeconcepto.SdtCountry)getSdt()).setgxTv_SdtCountry_Countryname(Value);
   }


   @JsonProperty("uri")
   public String URI( )
   {
      return "" ;
   }

   private int startRow ;
   private int maxRows ;
}

