package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.fasterxml.jackson.annotation.*;
import java.util.*;

@javax.xml.bind.annotation.XmlType(name = "ProgramNames.ProgramName", namespace ="OEA5pruebaDeConcepto")
@JsonPropertyOrder(alphabetic=true)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.NONE, getterVisibility=JsonAutoDetect.Visibility.NONE, isGetterVisibility=JsonAutoDetect.Visibility.NONE)
public final  class SdtProgramNames_ProgramName_RESTInterface extends GxGenericCollectionItem<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>
{
   public SdtProgramNames_ProgramName_RESTInterface( )
   {
      super(new com.oea5pruebadeconcepto.SdtProgramNames_ProgramName ());
   }

   public SdtProgramNames_ProgramName_RESTInterface( com.oea5pruebadeconcepto.SdtProgramNames_ProgramName psdt )
   {
      super(psdt);
   }

   @JsonProperty("Name")
   public String getgxTv_SdtProgramNames_ProgramName_Name( )
   {
      return GXutil.rtrim(((com.oea5pruebadeconcepto.SdtProgramNames_ProgramName)getSdt()).getgxTv_SdtProgramNames_ProgramName_Name()) ;
   }

   @JsonProperty("Name")
   public void setgxTv_SdtProgramNames_ProgramName_Name(  String Value )
   {
      ((com.oea5pruebadeconcepto.SdtProgramNames_ProgramName)getSdt()).setgxTv_SdtProgramNames_ProgramName_Name(Value);
   }


   @JsonProperty("Description")
   public String getgxTv_SdtProgramNames_ProgramName_Description( )
   {
      return GXutil.rtrim(((com.oea5pruebadeconcepto.SdtProgramNames_ProgramName)getSdt()).getgxTv_SdtProgramNames_ProgramName_Description()) ;
   }

   @JsonProperty("Description")
   public void setgxTv_SdtProgramNames_ProgramName_Description(  String Value )
   {
      ((com.oea5pruebadeconcepto.SdtProgramNames_ProgramName)getSdt()).setgxTv_SdtProgramNames_ProgramName_Description(Value);
   }


   @JsonProperty("Link")
   public String getgxTv_SdtProgramNames_ProgramName_Link( )
   {
      return GXutil.rtrim(((com.oea5pruebadeconcepto.SdtProgramNames_ProgramName)getSdt()).getgxTv_SdtProgramNames_ProgramName_Link()) ;
   }

   @JsonProperty("Link")
   public void setgxTv_SdtProgramNames_ProgramName_Link(  String Value )
   {
      ((com.oea5pruebadeconcepto.SdtProgramNames_ProgramName)getSdt()).setgxTv_SdtProgramNames_ProgramName_Link(Value);
   }


   int remoteHandle = -1;
}

