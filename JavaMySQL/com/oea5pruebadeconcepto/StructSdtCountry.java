package com.oea5pruebadeconcepto ;
import com.genexus.*;

public final  class StructSdtCountry implements Cloneable, java.io.Serializable
{
   public StructSdtCountry( )
   {
      this( -1, new ModelContext( StructSdtCountry.class ));
   }

   public StructSdtCountry( int remoteHandle ,
                            ModelContext context )
   {
      gxTv_SdtCountry_Countrycode = "" ;
      gxTv_SdtCountry_Countryname = "" ;
      gxTv_SdtCountry_Mode = "" ;
      gxTv_SdtCountry_Countrycode_Z = "" ;
      gxTv_SdtCountry_Countryname_Z = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getCountrycode( )
   {
      return gxTv_SdtCountry_Countrycode ;
   }

   public void setCountrycode( String value )
   {
      gxTv_SdtCountry_N = (byte)(0) ;
      gxTv_SdtCountry_Countrycode = value ;
   }

   public String getCountryname( )
   {
      return gxTv_SdtCountry_Countryname ;
   }

   public void setCountryname( String value )
   {
      gxTv_SdtCountry_N = (byte)(0) ;
      gxTv_SdtCountry_Countryname = value ;
   }

   public String getMode( )
   {
      return gxTv_SdtCountry_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtCountry_N = (byte)(0) ;
      gxTv_SdtCountry_Mode = value ;
   }

   public short getInitialized( )
   {
      return gxTv_SdtCountry_Initialized ;
   }

   public void setInitialized( short value )
   {
      gxTv_SdtCountry_N = (byte)(0) ;
      gxTv_SdtCountry_Initialized = value ;
   }

   public String getCountrycode_Z( )
   {
      return gxTv_SdtCountry_Countrycode_Z ;
   }

   public void setCountrycode_Z( String value )
   {
      gxTv_SdtCountry_N = (byte)(0) ;
      gxTv_SdtCountry_Countrycode_Z = value ;
   }

   public String getCountryname_Z( )
   {
      return gxTv_SdtCountry_Countryname_Z ;
   }

   public void setCountryname_Z( String value )
   {
      gxTv_SdtCountry_N = (byte)(0) ;
      gxTv_SdtCountry_Countryname_Z = value ;
   }

   private byte gxTv_SdtCountry_N ;
   protected short gxTv_SdtCountry_Initialized ;
   protected String gxTv_SdtCountry_Countrycode ;
   protected String gxTv_SdtCountry_Mode ;
   protected String gxTv_SdtCountry_Countrycode_Z ;
   protected String gxTv_SdtCountry_Countryname ;
   protected String gxTv_SdtCountry_Countryname_Z ;
}

