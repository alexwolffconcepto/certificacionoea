package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class country_bc extends GXWebPanel implements IGxSilentTrn
{
   public country_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public country_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( country_bc.class ));
   }

   public country_bc( int remoteHandle ,
                      ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public GXBCCollection<com.oea5pruebadeconcepto.SdtCountry> GetAll( int Start ,
                                                                      int Count )
   {
      GXPagingFrom2 = Start ;
      GXPagingTo2 = ((Count>0) ? Count : 100000000) ;
      /* Using cursor BC00024 */
      pr_default.execute(2, new Object[] {Integer.valueOf(GXPagingFrom2), Integer.valueOf(GXPagingTo2)});
      RcdFound2 = (short)(0) ;
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound2 = (short)(1) ;
         A2CountryCode = BC00024_A2CountryCode[0] ;
         A3CountryName = BC00024_A3CountryName[0] ;
      }
      bcCountry = new com.oea5pruebadeconcepto.SdtCountry(remoteHandle);
      gx_restcollection.clear();
      while ( RcdFound2 != 0 )
      {
         onLoadActions022( ) ;
         addRow022( ) ;
         gx_sdt_item = bcCountry.Clone() ;
         gx_restcollection.add(gx_sdt_item, 0);
         pr_default.readNext(2);
         RcdFound2 = (short)(0) ;
         sMode2 = Gx_mode ;
         Gx_mode = "DSP" ;
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound2 = (short)(1) ;
            A2CountryCode = BC00024_A2CountryCode[0] ;
            A3CountryName = BC00024_A3CountryName[0] ;
         }
         Gx_mode = sMode2 ;
      }
      pr_default.close(2);
      /* Load Subordinate Levels */
      return gx_restcollection ;
   }

   public void setseudovars( )
   {
      zm022( 0) ;
   }

   public void getInsDefault( )
   {
      readRow022( ) ;
      standaloneNotModal( ) ;
      initializeNonKey022( ) ;
      standaloneModal( ) ;
      addRow022( ) ;
      Gx_mode = "INS" ;
   }

   public void afterTrn( )
   {
      if ( trnEnded == 1 )
      {
         if ( ! (GXutil.strcmp("", endTrnMsgTxt)==0) )
         {
            httpContext.GX_msglist.addItem(endTrnMsgTxt, endTrnMsgCod, 0, "", true);
         }
         /* Execute user event: After Trn */
         e11022 ();
         trnEnded = 0 ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( isIns( )  )
         {
            Z2CountryCode = A2CountryCode ;
            SetMode( "UPD") ;
         }
      }
      endTrnMsgTxt = "" ;
   }

   public String toString( )
   {
      return "" ;
   }

   public GXContentInfo getContentInfo( )
   {
      return (GXContentInfo)(null) ;
   }

   public boolean Reindex( )
   {
      return true ;
   }

   public void confirm_020( )
   {
      beforeValidate022( ) ;
      if ( AnyError == 0 )
      {
         if ( isDlt( ) )
         {
            onDeleteControls022( ) ;
         }
         else
         {
            checkExtendedTable022( ) ;
            if ( AnyError == 0 )
            {
            }
            closeExtendedTableCursors022( ) ;
         }
      }
      if ( AnyError == 0 )
      {
         IsConfirmed = (short)(1) ;
      }
   }

   public void e12022( )
   {
      /* Start Routine */
      returnInSub = false ;
   }

   public void e11022( )
   {
      /* After Trn Routine */
      returnInSub = false ;
   }

   public void zm022( int GX_JID )
   {
      if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
      {
         Z3CountryName = A3CountryName ;
      }
      if ( GX_JID == -1 )
      {
         Z2CountryCode = A2CountryCode ;
         Z3CountryName = A3CountryName ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
   }

   public void load022( )
   {
      /* Using cursor BC00025 */
      pr_default.execute(3, new Object[] {A2CountryCode});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound2 = (short)(1) ;
         A3CountryName = BC00025_A3CountryName[0] ;
         zm022( -1) ;
      }
      pr_default.close(3);
      onLoadActions022( ) ;
   }

   public void onLoadActions022( )
   {
   }

   public void checkExtendedTable022( )
   {
      nIsDirty_2 = (short)(0) ;
      standaloneModal( ) ;
   }

   public void closeExtendedTableCursors022( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey022( )
   {
      /* Using cursor BC00026 */
      pr_default.execute(4, new Object[] {A2CountryCode});
      if ( (pr_default.getStatus(4) != 101) )
      {
         RcdFound2 = (short)(1) ;
      }
      else
      {
         RcdFound2 = (short)(0) ;
      }
      pr_default.close(4);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC00027 */
      pr_default.execute(5, new Object[] {A2CountryCode});
      if ( (pr_default.getStatus(5) != 101) )
      {
         zm022( 1) ;
         RcdFound2 = (short)(1) ;
         A2CountryCode = BC00027_A2CountryCode[0] ;
         A3CountryName = BC00027_A3CountryName[0] ;
         Z2CountryCode = A2CountryCode ;
         sMode2 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load022( ) ;
         if ( AnyError == 1 )
         {
            RcdFound2 = (short)(0) ;
            initializeNonKey022( ) ;
         }
         Gx_mode = sMode2 ;
      }
      else
      {
         RcdFound2 = (short)(0) ;
         initializeNonKey022( ) ;
         sMode2 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode2 ;
      }
      pr_default.close(5);
   }

   public void getEqualNoModal( )
   {
      getKey022( ) ;
      if ( RcdFound2 == 0 )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_020( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency022( )
   {
      if ( ! isIns( ) )
      {
         /* Using cursor BC00028 */
         pr_default.execute(6, new Object[] {A2CountryCode});
         if ( (pr_default.getStatus(6) == 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"Country"}), "RecordIsLocked", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(6) == 101) || ( GXutil.strcmp(Z3CountryName, BC00028_A3CountryName[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_waschg", new Object[] {"Country"}), "RecordWasChanged", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert022( )
   {
      beforeValidate022( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable022( ) ;
      }
      if ( AnyError == 0 )
      {
         zm022( 0) ;
         checkOptimisticConcurrency022( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm022( ) ;
            if ( AnyError == 0 )
            {
               beforeInsert022( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor BC00029 */
                  pr_default.execute(7, new Object[] {A2CountryCode, A3CountryName});
                  Application.getSmartCacheProvider(remoteHandle).setUpdated("Country");
                  if ( (pr_default.getStatus(7) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "");
                     AnyError = (short)(1) ;
                  }
                  if ( AnyError == 0 )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( AnyError == 0 )
                     {
                        /* Save values for previous() function. */
                        endTrnMsgTxt = localUtil.getMessages().getMessage("GXM_sucadded") ;
                        endTrnMsgCod = "SuccessfullyAdded" ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load022( ) ;
         }
         endLevel022( ) ;
      }
      closeExtendedTableCursors022( ) ;
   }

   public void update022( )
   {
      beforeValidate022( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable022( ) ;
      }
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency022( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm022( ) ;
            if ( AnyError == 0 )
            {
               beforeUpdate022( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor BC000210 */
                  pr_default.execute(8, new Object[] {A3CountryName, A2CountryCode});
                  Application.getSmartCacheProvider(remoteHandle).setUpdated("Country");
                  if ( (pr_default.getStatus(8) == 103) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"Country"}), "RecordIsLocked", 1, "");
                     AnyError = (short)(1) ;
                  }
                  deferredUpdate022( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( AnyError == 0 )
                     {
                        getByPrimaryKey( ) ;
                        endTrnMsgTxt = localUtil.getMessages().getMessage("GXM_sucupdated") ;
                        endTrnMsgCod = "SuccessfullyUpdated" ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel022( ) ;
      }
      closeExtendedTableCursors022( ) ;
   }

   public void deferredUpdate022( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate022( ) ;
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency022( ) ;
      }
      if ( AnyError == 0 )
      {
         onDeleteControls022( ) ;
         afterConfirm022( ) ;
         if ( AnyError == 0 )
         {
            beforeDelete022( ) ;
            if ( AnyError == 0 )
            {
               /* No cascading delete specified. */
               /* Using cursor BC000211 */
               pr_default.execute(9, new Object[] {A2CountryCode});
               Application.getSmartCacheProvider(remoteHandle).setUpdated("Country");
               if ( AnyError == 0 )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( AnyError == 0 )
                  {
                     endTrnMsgTxt = localUtil.getMessages().getMessage("GXM_sucdeleted") ;
                     endTrnMsgCod = "SuccessfullyDeleted" ;
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode2 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel022( ) ;
      Gx_mode = sMode2 ;
   }

   public void onDeleteControls022( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( AnyError == 0 )
      {
         /* Using cursor BC000212 */
         pr_default.execute(10, new Object[] {A2CountryCode});
         if ( (pr_default.getStatus(10) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_del", new Object[] {"AEO Master Data"}), "CannotDeleteReferencedRecord", 1, "");
            AnyError = (short)(1) ;
         }
         pr_default.close(10);
      }
   }

   public void endLevel022( )
   {
      if ( ! isIns( ) )
      {
         pr_default.close(6);
      }
      if ( AnyError == 0 )
      {
         beforeComplete022( ) ;
      }
      if ( AnyError == 0 )
      {
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( AnyError != 0 )
      {
         httpContext.wjLoc = "" ;
         httpContext.nUserReturn = (byte)(0) ;
      }
   }

   public void scanKeyStart022( )
   {
      /* Scan By routine */
      /* Using cursor BC000213 */
      pr_default.execute(11, new Object[] {A2CountryCode});
      RcdFound2 = (short)(0) ;
      if ( (pr_default.getStatus(11) != 101) )
      {
         RcdFound2 = (short)(1) ;
         A2CountryCode = BC000213_A2CountryCode[0] ;
         A3CountryName = BC000213_A3CountryName[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanKeyNext022( )
   {
      /* Scan next routine */
      pr_default.readNext(11);
      RcdFound2 = (short)(0) ;
      scanKeyLoad022( ) ;
   }

   public void scanKeyLoad022( )
   {
      sMode2 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(11) != 101) )
      {
         RcdFound2 = (short)(1) ;
         A2CountryCode = BC000213_A2CountryCode[0] ;
         A3CountryName = BC000213_A3CountryName[0] ;
      }
      Gx_mode = sMode2 ;
   }

   public void scanKeyEnd022( )
   {
      pr_default.close(11);
   }

   public void afterConfirm022( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert022( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate022( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete022( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete022( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate022( )
   {
      /* Before Validate Rules */
   }

   public void disableAttributes022( )
   {
   }

   public void send_integrity_lvl_hashes022( )
   {
   }

   public void addRow022( )
   {
      VarsToRow2( bcCountry) ;
   }

   public void readRow022( )
   {
      RowToVars2( bcCountry, 1) ;
   }

   public void initializeNonKey022( )
   {
      A3CountryName = "" ;
      Z3CountryName = "" ;
   }

   public void initAll022( )
   {
      A2CountryCode = "" ;
      initializeNonKey022( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public boolean isIns( )
   {
      return ((GXutil.strcmp(Gx_mode, "INS")==0) ? true : false) ;
   }

   public boolean isDlt( )
   {
      return ((GXutil.strcmp(Gx_mode, "DLT")==0) ? true : false) ;
   }

   public boolean isUpd( )
   {
      return ((GXutil.strcmp(Gx_mode, "UPD")==0) ? true : false) ;
   }

   public boolean isDsp( )
   {
      return ((GXutil.strcmp(Gx_mode, "DSP")==0) ? true : false) ;
   }

   public void VarsToRow2( com.oea5pruebadeconcepto.SdtCountry obj2 )
   {
      obj2.setgxTv_SdtCountry_Mode( Gx_mode );
      obj2.setgxTv_SdtCountry_Countryname( A3CountryName );
      obj2.setgxTv_SdtCountry_Countrycode( A2CountryCode );
      obj2.setgxTv_SdtCountry_Countrycode_Z( Z2CountryCode );
      obj2.setgxTv_SdtCountry_Countryname_Z( Z3CountryName );
      obj2.setgxTv_SdtCountry_Mode( Gx_mode );
   }

   public void KeyVarsToRow2( com.oea5pruebadeconcepto.SdtCountry obj2 )
   {
      obj2.setgxTv_SdtCountry_Countrycode( A2CountryCode );
   }

   public void RowToVars2( com.oea5pruebadeconcepto.SdtCountry obj2 ,
                           int forceLoad )
   {
      Gx_mode = obj2.getgxTv_SdtCountry_Mode() ;
      A3CountryName = obj2.getgxTv_SdtCountry_Countryname() ;
      A2CountryCode = obj2.getgxTv_SdtCountry_Countrycode() ;
      Z2CountryCode = obj2.getgxTv_SdtCountry_Countrycode_Z() ;
      Z3CountryName = obj2.getgxTv_SdtCountry_Countryname_Z() ;
      Gx_mode = obj2.getgxTv_SdtCountry_Mode() ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A2CountryCode = (String)getParm(obj,0) ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey022( ) ;
      scanKeyStart022( ) ;
      if ( RcdFound2 == 0 )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z2CountryCode = A2CountryCode ;
      }
      zm022( -1) ;
      onLoadActions022( ) ;
      addRow022( ) ;
      scanKeyEnd022( ) ;
      if ( RcdFound2 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_keynfound"), "PrimaryKeyNotFound", 1, "");
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Load( )
   {
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      RowToVars2( bcCountry, 0) ;
      scanKeyStart022( ) ;
      if ( RcdFound2 == 0 )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z2CountryCode = A2CountryCode ;
      }
      zm022( -1) ;
      onLoadActions022( ) ;
      addRow022( ) ;
      scanKeyEnd022( ) ;
      if ( RcdFound2 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_keynfound"), "PrimaryKeyNotFound", 1, "");
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void saveImpl( )
   {
      nKeyPressed = (byte)(1) ;
      getKey022( ) ;
      if ( isIns( ) )
      {
         /* Insert record */
         insert022( ) ;
      }
      else
      {
         if ( RcdFound2 == 1 )
         {
            if ( GXutil.strcmp(A2CountryCode, Z2CountryCode) != 0 )
            {
               A2CountryCode = Z2CountryCode ;
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "CandidateKeyNotFound", 1, "");
               AnyError = (short)(1) ;
            }
            else if ( isDlt( ) )
            {
               delete( ) ;
               afterTrn( ) ;
            }
            else
            {
               Gx_mode = "UPD" ;
               /* Update record */
               update022( ) ;
            }
         }
         else
         {
            if ( isDlt( ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "CandidateKeyNotFound", 1, "");
               AnyError = (short)(1) ;
            }
            else
            {
               if ( GXutil.strcmp(A2CountryCode, Z2CountryCode) != 0 )
               {
                  if ( isUpd( ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "DuplicatePrimaryKey", 1, "");
                     AnyError = (short)(1) ;
                  }
                  else
                  {
                     Gx_mode = "INS" ;
                     /* Insert record */
                     insert022( ) ;
                  }
               }
               else
               {
                  if ( GXutil.strcmp(Gx_mode, "UPD") == 0 )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_recdeleted"), 1, "");
                     AnyError = (short)(1) ;
                  }
                  else
                  {
                     Gx_mode = "INS" ;
                     /* Insert record */
                     insert022( ) ;
                  }
               }
            }
         }
      }
      afterTrn( ) ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars2( bcCountry, 1) ;
      saveImpl( ) ;
      VarsToRow2( bcCountry) ;
      httpContext.GX_msglist = BackMsgLst ;
   }

   public boolean Insert( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars2( bcCountry, 1) ;
      Gx_mode = "INS" ;
      /* Insert record */
      insert022( ) ;
      afterTrn( ) ;
      VarsToRow2( bcCountry) ;
      httpContext.GX_msglist = BackMsgLst ;
      return (AnyError==0) ;
   }

   public void updateImpl( )
   {
      if ( isUpd( ) )
      {
         saveImpl( ) ;
      }
      else
      {
         com.oea5pruebadeconcepto.SdtCountry auxBC = new com.oea5pruebadeconcepto.SdtCountry( remoteHandle, context);
         IGxSilentTrn auxTrn = auxBC.getTransaction();
         auxBC.Load(A2CountryCode);
         if ( auxTrn.Errors() == 0 )
         {
            auxBC.updateDirties(bcCountry);
            auxBC.Save();
         }
         LclMsgLst = auxTrn.GetMessages() ;
         AnyError = (short)(auxTrn.Errors()) ;
         httpContext.GX_msglist = LclMsgLst ;
         if ( auxTrn.Errors() == 0 )
         {
            Gx_mode = auxTrn.GetMode() ;
            afterTrn( ) ;
         }
      }
   }

   public boolean Update( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars2( bcCountry, 1) ;
      updateImpl( ) ;
      VarsToRow2( bcCountry) ;
      httpContext.GX_msglist = BackMsgLst ;
      return (AnyError==0) ;
   }

   public boolean InsertOrUpdate( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars2( bcCountry, 1) ;
      Gx_mode = "INS" ;
      /* Insert record */
      insert022( ) ;
      if ( AnyError == 1 )
      {
         if ( GXutil.strcmp(httpContext.GX_msglist.getItemValue((short)(1)), "DuplicatePrimaryKey") == 0 )
         {
            AnyError = (short)(0) ;
            httpContext.GX_msglist.removeAllItems();
            updateImpl( ) ;
         }
      }
      else
      {
         afterTrn( ) ;
      }
      VarsToRow2( bcCountry) ;
      httpContext.GX_msglist = BackMsgLst ;
      return (AnyError==0) ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars2( bcCountry, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey022( ) ;
      if ( RcdFound2 == 1 )
      {
         if ( isIns( ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "");
            AnyError = (short)(1) ;
         }
         else if ( GXutil.strcmp(A2CountryCode, Z2CountryCode) != 0 )
         {
            A2CountryCode = Z2CountryCode ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "DuplicatePrimaryKey", 1, "");
            AnyError = (short)(1) ;
         }
         else if ( isDlt( ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( GXutil.strcmp(A2CountryCode, Z2CountryCode) != 0 )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( isUpd( ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_recdeleted"), 1, "");
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      Application.rollbackDataStores(context, remoteHandle, pr_default, "country_bc");
      VarsToRow2( bcCountry) ;
      httpContext.GX_msglist = BackMsgLst ;
   }

   public int Errors( )
   {
      if ( AnyError == 0 )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcCountry.getgxTv_SdtCountry_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcCountry.setgxTv_SdtCountry_Mode( Gx_mode );
   }

   public void SetSDT( com.oea5pruebadeconcepto.SdtCountry sdt ,
                       byte sdtToBc )
   {
      if ( sdt != bcCountry )
      {
         bcCountry = sdt ;
         if ( GXutil.strcmp(bcCountry.getgxTv_SdtCountry_Mode(), "") == 0 )
         {
            bcCountry.setgxTv_SdtCountry_Mode( "INS" );
         }
         if ( sdtToBc == 1 )
         {
            VarsToRow2( bcCountry) ;
         }
         else
         {
            RowToVars2( bcCountry, 1) ;
         }
      }
      else
      {
         if ( GXutil.strcmp(bcCountry.getgxTv_SdtCountry_Mode(), "") == 0 )
         {
            bcCountry.setgxTv_SdtCountry_Mode( "INS" );
         }
      }
   }

   public void ReloadFromSDT( )
   {
      RowToVars2( bcCountry, 1) ;
   }

   public void ForceCommitOnExit( )
   {
      mustCommit = true ;
   }

   public SdtCountry getCountry_BC( )
   {
      return bcCountry ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      BC00024_A2CountryCode = new String[] {""} ;
      BC00024_A3CountryName = new String[] {""} ;
      A2CountryCode = "" ;
      A3CountryName = "" ;
      gx_restcollection = new GXBCCollection<com.oea5pruebadeconcepto.SdtCountry>(com.oea5pruebadeconcepto.SdtCountry.class, "Country", "OEA5pruebaDeConcepto", remoteHandle);
      sMode2 = "" ;
      Gx_mode = "" ;
      endTrnMsgTxt = "" ;
      endTrnMsgCod = "" ;
      Z2CountryCode = "" ;
      Z3CountryName = "" ;
      BC00025_A2CountryCode = new String[] {""} ;
      BC00025_A3CountryName = new String[] {""} ;
      BC00026_A2CountryCode = new String[] {""} ;
      BC00027_A2CountryCode = new String[] {""} ;
      BC00027_A3CountryName = new String[] {""} ;
      BC00028_A2CountryCode = new String[] {""} ;
      BC00028_A3CountryName = new String[] {""} ;
      BC000212_A1IdentificationIssuingCountryCo = new String[] {""} ;
      BC000212_A5PartieID = new String[] {""} ;
      BC000213_A2CountryCode = new String[] {""} ;
      BC000213_A3CountryName = new String[] {""} ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new com.oea5pruebadeconcepto.country_bc__default(),
         new Object[] {
             new Object[] {
            BC00022_A2CountryCode, BC00022_A3CountryName
            }
            , new Object[] {
            BC00023_A2CountryCode, BC00023_A3CountryName
            }
            , new Object[] {
            BC00024_A2CountryCode, BC00024_A3CountryName
            }
            , new Object[] {
            BC00025_A2CountryCode, BC00025_A3CountryName
            }
            , new Object[] {
            BC00026_A2CountryCode
            }
            , new Object[] {
            BC00027_A2CountryCode, BC00027_A3CountryName
            }
            , new Object[] {
            BC00028_A2CountryCode, BC00028_A3CountryName
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC000212_A1IdentificationIssuingCountryCo, BC000212_A5PartieID
            }
            , new Object[] {
            BC000213_A2CountryCode, BC000213_A3CountryName
            }
         }
      );
      /* Execute Start event if defined. */
      /* Execute user event: Start */
      e12022 ();
      standaloneNotModal( ) ;
   }

   private byte nKeyPressed ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short RcdFound2 ;
   private short nIsDirty_2 ;
   private int trnEnded ;
   private int Start ;
   private int Count ;
   private int GXPagingFrom2 ;
   private int GXPagingTo2 ;
   private int GX_JID ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String A2CountryCode ;
   private String sMode2 ;
   private String Gx_mode ;
   private String endTrnMsgTxt ;
   private String endTrnMsgCod ;
   private String Z2CountryCode ;
   private boolean returnInSub ;
   private boolean mustCommit ;
   private String A3CountryName ;
   private String Z3CountryName ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private GXBCCollection<com.oea5pruebadeconcepto.SdtCountry> gx_restcollection ;
   private com.oea5pruebadeconcepto.SdtCountry bcCountry ;
   private com.oea5pruebadeconcepto.SdtCountry gx_sdt_item ;
   private IDataStoreProvider pr_default ;
   private String[] BC00024_A2CountryCode ;
   private String[] BC00024_A3CountryName ;
   private String[] BC00025_A2CountryCode ;
   private String[] BC00025_A3CountryName ;
   private String[] BC00026_A2CountryCode ;
   private String[] BC00027_A2CountryCode ;
   private String[] BC00027_A3CountryName ;
   private String[] BC00028_A2CountryCode ;
   private String[] BC00028_A3CountryName ;
   private String[] BC000212_A1IdentificationIssuingCountryCo ;
   private String[] BC000212_A5PartieID ;
   private String[] BC000213_A2CountryCode ;
   private String[] BC000213_A3CountryName ;
   private String[] BC00022_A2CountryCode ;
   private String[] BC00022_A3CountryName ;
   private String[] BC00023_A2CountryCode ;
   private String[] BC00023_A3CountryName ;
}

final  class country_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC00022", "SELECT `CountryCode`, `CountryName` FROM `Country` WHERE `CountryCode` = ?  FOR UPDATE ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC00023", "SELECT `CountryCode`, `CountryName` FROM `Country` WHERE `CountryCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC00024", "SELECT TM1.`CountryCode`, TM1.`CountryName` FROM `Country` TM1 ORDER BY TM1.`CountryCode`  LIMIT ?, ?",true, GX_NOMASK, false, this,100, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC00025", "SELECT TM1.`CountryCode`, TM1.`CountryName` FROM `Country` TM1 WHERE TM1.`CountryCode` = ? ORDER BY TM1.`CountryCode` ",true, GX_NOMASK, false, this,100, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC00026", "SELECT `CountryCode` FROM `Country` WHERE `CountryCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC00027", "SELECT `CountryCode`, `CountryName` FROM `Country` WHERE `CountryCode` = ? ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new ForEachCursor("BC00028", "SELECT `CountryCode`, `CountryName` FROM `Country` WHERE `CountryCode` = ?  FOR UPDATE ",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,false )
         ,new UpdateCursor("BC00029", "INSERT INTO `Country`(`CountryCode`, `CountryName`) VALUES(?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC000210", "UPDATE `Country` SET `CountryName`=?  WHERE `CountryCode` = ?", GX_NOMASK)
         ,new UpdateCursor("BC000211", "DELETE FROM `Country`  WHERE `CountryCode` = ?", GX_NOMASK)
         ,new ForEachCursor("BC000212", "SELECT `IdentificationIssuingCountryCo`, `PartieID` FROM `AEOMasterData` WHERE `IdentificationIssuingCountryCo` = ?  LIMIT 1",true, GX_NOMASK, false, this,1, GxCacheFrequency.OFF,true )
         ,new ForEachCursor("BC000213", "SELECT TM1.`CountryCode`, TM1.`CountryName` FROM `Country` TM1 WHERE TM1.`CountryCode` = ? ORDER BY TM1.`CountryCode` ",true, GX_NOMASK, false, this,100, GxCacheFrequency.OFF,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               return;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
            case 6 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getString(2, 30);
               return;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2);
               ((String[]) buf[1])[0] = rslt.getVarchar(2);
               return;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 1 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 2 :
               stmt.setInt(1, ((Number) parms[0]).intValue());
               stmt.setInt(2, ((Number) parms[1]).intValue());
               return;
            case 3 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 4 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 5 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 6 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 7 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setVarchar(2, (String)parms[1], 50, false);
               return;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 50, false);
               stmt.setString(2, (String)parms[1], 2);
               return;
            case 9 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 10 :
               stmt.setString(1, (String)parms[0], 2);
               return;
            case 11 :
               stmt.setString(1, (String)parms[0], 2);
               return;
      }
   }

}

