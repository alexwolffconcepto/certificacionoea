package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.servlet.*;
import com.genexus.servlet.http.*;
import java.util.*;
import com.genexus.*;
import com.genexus.Application;
import com.genexus.ws.rs.core.*;

@javax.ws.rs.Path("/AEOMasterData")
public final  class aeomasterdata_bc_services_rest extends GxRestService
{
   @javax.ws.rs.GET
   @javax.ws.rs.Produces({javax.ws.rs.core.MediaType.APPLICATION_JSON + ";charset=UTF-8"})
   public javax.ws.rs.core.Response GetBy( @javax.ws.rs.QueryParam("start") @javax.ws.rs.DefaultValue("0") String sstartRow ,
                                           @javax.ws.rs.QueryParam("count") @javax.ws.rs.DefaultValue("0") String smaxRows ,
                                           @javax.ws.rs.QueryParam("IdentificationIssuingCountryCo") String sA1IdentificationIssuingCountryCo ,
                                           @javax.ws.rs.QueryParam("PartieRoleCode") String sA8PartieRoleCode ,
                                           @javax.ws.rs.QueryParam("IdentificationIssuingCountryNa") String sA4IdentificationIssuingCountryNa )
   {
      Response.ResponseBuilder builder = null;
      builder = Response.notFound();
      return (javax.ws.rs.core.Response) builder.build() ;
   }

   @javax.ws.rs.GET
   @javax.ws.rs.Path("{sA1IdentificationIssuingCountryCo:.*}")
   @javax.ws.rs.Produces({javax.ws.rs.core.MediaType.APPLICATION_JSON + ";charset=UTF-8"})
   public javax.ws.rs.core.Response LoadDefault( @javax.ws.rs.PathParam("sA1IdentificationIssuingCountryCo") String sA1IdentificationIssuingCountryCo )
   {
      super.init( "GET" );
      com.oea5pruebadeconcepto.SdtAEOMasterData worker = new com.oea5pruebadeconcepto.SdtAEOMasterData(remoteHandle, context);
      if ( GXutil.strcmp(sA1IdentificationIssuingCountryCo, "_default") == 0 )
      {
         IGxSilentTrn workertrn = worker.getTransaction();
         workertrn.getInsDefault();
      }
      builder = Response.okWrapped(new com.oea5pruebadeconcepto.SdtAEOMasterData_RESTInterface(worker));
      cleanup();
      return (javax.ws.rs.core.Response) builder.build() ;
   }

   @javax.ws.rs.GET
   @javax.ws.rs.Path("{sA1IdentificationIssuingCountryCo:.*},{sA5PartieID:.*}")
   @javax.ws.rs.Produces({javax.ws.rs.core.MediaType.APPLICATION_JSON + ";charset=UTF-8"})
   public javax.ws.rs.core.Response Load( @javax.ws.rs.PathParam("sA1IdentificationIssuingCountryCo") String sA1IdentificationIssuingCountryCo ,
                                          @javax.ws.rs.PathParam("sA5PartieID") String sA5PartieID )
   {
      super.init( "GET" );
      String A1IdentificationIssuingCountryCo;
      A1IdentificationIssuingCountryCo = sA1IdentificationIssuingCountryCo ;
      String A5PartieID;
      A5PartieID = sA5PartieID ;
      com.oea5pruebadeconcepto.SdtAEOMasterData worker = new com.oea5pruebadeconcepto.SdtAEOMasterData(remoteHandle, context);
      if ( GXutil.strcmp(sA1IdentificationIssuingCountryCo, "_default") == 0 )
      {
         IGxSilentTrn workertrn = worker.getTransaction();
         workertrn.getInsDefault();
      }
      else
      {
         worker.Load(A1IdentificationIssuingCountryCo,A5PartieID);
      }
      if ( worker.Success() )
      {
         builder = Response.okWrapped(new com.oea5pruebadeconcepto.SdtAEOMasterData_RESTInterface(worker));
      }
      else
      {
         ErrorCheck(worker.getTransaction());
         if ( error )
         {
            builder = Response.notFound();
         }
         if ( errorJson != null )
         {
            builder.type("application/json");
            builder.entity(errorJson.toString());
         }
      }
      cleanup();
      addWarningHeaders(builder, worker.getTransaction().GetMessages(), myUriInfoWrapper.getAbsolutePath().getHost());
      return (javax.ws.rs.core.Response) builder.build() ;
   }

   @javax.ws.rs.DELETE
   @javax.ws.rs.Path("{sA1IdentificationIssuingCountryCo:.*},{sA5PartieID:.*}")
   @javax.ws.rs.Produces({javax.ws.rs.core.MediaType.APPLICATION_JSON + ";charset=UTF-8"})
   public javax.ws.rs.core.Response Delete( @javax.ws.rs.PathParam("sA1IdentificationIssuingCountryCo") String sA1IdentificationIssuingCountryCo ,
                                            @javax.ws.rs.PathParam("sA5PartieID") String sA5PartieID )
   {
      super.init( "POST" );
      String A1IdentificationIssuingCountryCo;
      A1IdentificationIssuingCountryCo = sA1IdentificationIssuingCountryCo ;
      String A5PartieID;
      A5PartieID = sA5PartieID ;
      com.oea5pruebadeconcepto.SdtAEOMasterData worker = new com.oea5pruebadeconcepto.SdtAEOMasterData(remoteHandle, context);
      worker.Load(A1IdentificationIssuingCountryCo,A5PartieID);
      worker.Delete();
      if ( worker.Success() )
      {
         Application.commitDataStores(context, remoteHandle, null, "aeomasterdata_bc");
         builder = Response.noContentWrapped();
      }
      else
      {
         ErrorCheck(worker.getTransaction());
         if ( error )
         {
            builder = Response.notFound();
         }
         if ( errorJson != null )
         {
            builder.type("application/json");
            builder.entity(errorJson.toString());
         }
      }
      cleanup();
      addWarningHeaders(builder, worker.getTransaction().GetMessages(), myUriInfoWrapper.getAbsolutePath().getHost());
      return (javax.ws.rs.core.Response) builder.build() ;
   }

   @javax.ws.rs.POST
   @javax.ws.rs.Path("{sA1IdentificationIssuingCountryCo:.*},{sA5PartieID:.*}")
   @javax.ws.rs.Consumes({javax.ws.rs.core.MediaType.APPLICATION_JSON})
   @javax.ws.rs.Produces({javax.ws.rs.core.MediaType.APPLICATION_JSON + ";charset=UTF-8"})
   public javax.ws.rs.core.Response Insert( @javax.ws.rs.PathParam("sA1IdentificationIssuingCountryCo") String sA1IdentificationIssuingCountryCo ,
                                            @javax.ws.rs.PathParam("sA5PartieID") String sA5PartieID ,
                                            @javax.ws.rs.QueryParam("check") @javax.ws.rs.DefaultValue("false") boolean gxcheck ,
                                            @javax.ws.rs.QueryParam("insertorupdate") @javax.ws.rs.DefaultValue("false") boolean gxinsertorupdate ,
                                            com.oea5pruebadeconcepto.SdtAEOMasterData_RESTInterface entity ) throws Exception
   {
      super.init( "POST" );
      try
      {
         String A1IdentificationIssuingCountryCo;
         A1IdentificationIssuingCountryCo = sA1IdentificationIssuingCountryCo ;
         String A5PartieID;
         A5PartieID = sA5PartieID ;
         com.oea5pruebadeconcepto.SdtAEOMasterData worker = new com.oea5pruebadeconcepto.SdtAEOMasterData(remoteHandle, context);
         com.oea5pruebadeconcepto.SdtAEOMasterData_RESTInterface worker_interface = new com.oea5pruebadeconcepto.SdtAEOMasterData_RESTInterface (worker);
         worker_interface.copyFrom(entity);
         worker.setgxTv_SdtAEOMasterData_Identificationissuingcountrycode( A1IdentificationIssuingCountryCo );
         worker.setgxTv_SdtAEOMasterData_Partieid( A5PartieID );
         if ( gxcheck )
         {
            worker.Check();
         }
         else
         {
            if ( gxinsertorupdate )
            {
               worker.InsertOrUpdate();
            }
            else
            {
               worker.Save();
            }
         }
         if ( worker.Success() )
         {
            if ( ! gxcheck )
            {
               Application.commitDataStores(context, remoteHandle, null, "aeomasterdata_bc");
            }
            if ( ! gxcheck )
            {
               builder = Response.createdWrapped(new java.net.URI(myUriInfoWrapper.getRequestUri().toString()));
               builder.entity(worker_interface);
            }
            else
            {
               builder = Response.okWrapped(worker_interface);
            }
         }
         else
         {
            ErrorCheck(worker.getTransaction());
            if ( error )
            {
               builder = Response.notFound();
            }
            if ( errorJson != null )
            {
               builder.type("application/json");
               builder.entity(errorJson.toString());
            }
         }
         cleanup();
         addWarningHeaders(builder, worker.getTransaction().GetMessages(), myUriInfoWrapper.getAbsolutePath().getHost());
         return (javax.ws.rs.core.Response) builder.build() ;
      }
      catch ( Exception e )
      {
         cleanup();
         throw e;
      }
   }

   @javax.ws.rs.PUT
   @javax.ws.rs.Path("{sA1IdentificationIssuingCountryCo:.*},{sA5PartieID:.*}")
   @javax.ws.rs.Consumes({javax.ws.rs.core.MediaType.APPLICATION_JSON})
   @javax.ws.rs.Produces({javax.ws.rs.core.MediaType.APPLICATION_JSON + ";charset=UTF-8"})
   public javax.ws.rs.core.Response Update( @javax.ws.rs.PathParam("sA1IdentificationIssuingCountryCo") String sA1IdentificationIssuingCountryCo ,
                                            @javax.ws.rs.PathParam("sA5PartieID") String sA5PartieID ,
                                            @javax.ws.rs.QueryParam("check") @javax.ws.rs.DefaultValue("false") boolean gxcheck ,
                                            com.oea5pruebadeconcepto.SdtAEOMasterData_RESTInterface entity )
   {
      super.init( "PUT" );
      String A1IdentificationIssuingCountryCo;
      A1IdentificationIssuingCountryCo = sA1IdentificationIssuingCountryCo ;
      String A5PartieID;
      A5PartieID = sA5PartieID ;
      com.oea5pruebadeconcepto.SdtAEOMasterData worker = new com.oea5pruebadeconcepto.SdtAEOMasterData(remoteHandle, context);
      com.oea5pruebadeconcepto.SdtAEOMasterData_RESTInterface worker_interface = new com.oea5pruebadeconcepto.SdtAEOMasterData_RESTInterface (worker);
      worker.setgxTv_SdtAEOMasterData_Identificationissuingcountrycode( A1IdentificationIssuingCountryCo );
      worker.setgxTv_SdtAEOMasterData_Partieid( A5PartieID );
      worker.Load(A1IdentificationIssuingCountryCo,A5PartieID);
      if (entity.getHash().equals(worker_interface.getHash())) {
         worker_interface.copyFrom(entity);
         if ( gxcheck )
         {
            worker.Check();
         }
         else
         {
            worker.Save();
         }
         if ( worker.Success() )
         {
            if ( ! gxcheck )
            {
               Application.commitDataStores(context, remoteHandle, null, "aeomasterdata_bc");
            }
            worker_interface.setHash(null);
            builder = Response.okWrapped(worker_interface);
         }
         else
         {
            ErrorCheck(worker.getTransaction());
            if ( error )
            {
               builder = Response.notFound();
            }
            if ( errorJson != null )
            {
               builder.type("application/json");
               builder.entity(errorJson.toString());
            }
         }
      }
      else
      {
         SetError("409",  localUtil.getMessages().getMessage("GXM_waschg", new Object[] {"AEOMasterData"}) );
         builder = Response.conflict();
         builder.type("application/json");
         builder.entity(errorJson.toString());
      }
      cleanup();
      addWarningHeaders(builder, worker.getTransaction().GetMessages(), myUriInfoWrapper.getAbsolutePath().getHost());
      return (javax.ws.rs.core.Response) builder.build() ;
   }

   private Vector<com.oea5pruebadeconcepto.SdtAEOMasterData_RESTLInterface> SdtAEOMasterData_RESTLInterfacefromGXObjectCollection( GXBCCollection<com.oea5pruebadeconcepto.SdtAEOMasterData> collection )
   {
      Vector<com.oea5pruebadeconcepto.SdtAEOMasterData_RESTLInterface> result = new Vector<com.oea5pruebadeconcepto.SdtAEOMasterData_RESTLInterface>();
      for (int i = 0; i < collection.size(); i++)
      {
         result.addElement(new com.oea5pruebadeconcepto.SdtAEOMasterData_RESTLInterface((com.oea5pruebadeconcepto.SdtAEOMasterData)collection.elementAt(i)));
      }
      return result ;
   }

   private void addWarningHeaders( com.genexus.ws.rs.core.IResponseBuilder builder ,
                                   com.genexus.internet.MsgList collection ,
                                   String host )
   {
      for (int i = 1; i < collection.size() +1; i++)
      {
         String message = collection.getItemText(i);
         short messagetype = collection.getItemType(i);
         if (messagetype == 0)
         {
            String encodedFlag = "";
            if (PrivateUtilities.containsNoAsciiCharacter(message))
            {
               message = PrivateUtilities.encodeURL(message);
               encodedFlag = "Encoded:";
            }
            if (collection.getItemGXMessage(i))
            {
               message = "System:" + message;
            }
            else
            {
               message = "User:" + message;
            }
            builder.header("Warning" , "299 " + host + " \"" + encodedFlag + message + "\"");
         }
      }
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

}

