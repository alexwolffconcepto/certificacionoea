package com.oea5pruebadeconcepto ;
import com.genexus.*;

public final  class StructSdtAEOMasterData implements Cloneable, java.io.Serializable
{
   public StructSdtAEOMasterData( )
   {
      this( -1, new ModelContext( StructSdtAEOMasterData.class ));
   }

   public StructSdtAEOMasterData( int remoteHandle ,
                                  ModelContext context )
   {
      gxTv_SdtAEOMasterData_Identificationissuingcountrycode = "" ;
      gxTv_SdtAEOMasterData_Identificationissuingcountryname = "" ;
      gxTv_SdtAEOMasterData_Partieid = "" ;
      gxTv_SdtAEOMasterData_Partiename = "" ;
      gxTv_SdtAEOMasterData_Partieshortname = "" ;
      gxTv_SdtAEOMasterData_Partierolecode = "" ;
      gxTv_SdtAEOMasterData_Mode = "" ;
      gxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z = "" ;
      gxTv_SdtAEOMasterData_Identificationissuingcountryname_Z = "" ;
      gxTv_SdtAEOMasterData_Partieid_Z = "" ;
      gxTv_SdtAEOMasterData_Partiename_Z = "" ;
      gxTv_SdtAEOMasterData_Partieshortname_Z = "" ;
      gxTv_SdtAEOMasterData_Partierolecode_Z = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getIdentificationissuingcountrycode( )
   {
      return gxTv_SdtAEOMasterData_Identificationissuingcountrycode ;
   }

   public void setIdentificationissuingcountrycode( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Identificationissuingcountrycode = value ;
   }

   public String getIdentificationissuingcountryname( )
   {
      return gxTv_SdtAEOMasterData_Identificationissuingcountryname ;
   }

   public void setIdentificationissuingcountryname( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Identificationissuingcountryname = value ;
   }

   public String getPartieid( )
   {
      return gxTv_SdtAEOMasterData_Partieid ;
   }

   public void setPartieid( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Partieid = value ;
   }

   public String getPartiename( )
   {
      return gxTv_SdtAEOMasterData_Partiename ;
   }

   public void setPartiename( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Partiename = value ;
   }

   public String getPartieshortname( )
   {
      return gxTv_SdtAEOMasterData_Partieshortname ;
   }

   public void setPartieshortname( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Partieshortname = value ;
   }

   public String getPartierolecode( )
   {
      return gxTv_SdtAEOMasterData_Partierolecode ;
   }

   public void setPartierolecode( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Partierolecode = value ;
   }

   public String getMode( )
   {
      return gxTv_SdtAEOMasterData_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Mode = value ;
   }

   public short getInitialized( )
   {
      return gxTv_SdtAEOMasterData_Initialized ;
   }

   public void setInitialized( short value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Initialized = value ;
   }

   public String getIdentificationissuingcountrycode_Z( )
   {
      return gxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z ;
   }

   public void setIdentificationissuingcountrycode_Z( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z = value ;
   }

   public String getIdentificationissuingcountryname_Z( )
   {
      return gxTv_SdtAEOMasterData_Identificationissuingcountryname_Z ;
   }

   public void setIdentificationissuingcountryname_Z( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Identificationissuingcountryname_Z = value ;
   }

   public String getPartieid_Z( )
   {
      return gxTv_SdtAEOMasterData_Partieid_Z ;
   }

   public void setPartieid_Z( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Partieid_Z = value ;
   }

   public String getPartiename_Z( )
   {
      return gxTv_SdtAEOMasterData_Partiename_Z ;
   }

   public void setPartiename_Z( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Partiename_Z = value ;
   }

   public String getPartieshortname_Z( )
   {
      return gxTv_SdtAEOMasterData_Partieshortname_Z ;
   }

   public void setPartieshortname_Z( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Partieshortname_Z = value ;
   }

   public String getPartierolecode_Z( )
   {
      return gxTv_SdtAEOMasterData_Partierolecode_Z ;
   }

   public void setPartierolecode_Z( String value )
   {
      gxTv_SdtAEOMasterData_N = (byte)(0) ;
      gxTv_SdtAEOMasterData_Partierolecode_Z = value ;
   }

   private byte gxTv_SdtAEOMasterData_N ;
   protected short gxTv_SdtAEOMasterData_Initialized ;
   protected String gxTv_SdtAEOMasterData_Identificationissuingcountrycode ;
   protected String gxTv_SdtAEOMasterData_Partieid ;
   protected String gxTv_SdtAEOMasterData_Partierolecode ;
   protected String gxTv_SdtAEOMasterData_Mode ;
   protected String gxTv_SdtAEOMasterData_Identificationissuingcountrycode_Z ;
   protected String gxTv_SdtAEOMasterData_Partieid_Z ;
   protected String gxTv_SdtAEOMasterData_Partierolecode_Z ;
   protected String gxTv_SdtAEOMasterData_Identificationissuingcountryname ;
   protected String gxTv_SdtAEOMasterData_Partiename ;
   protected String gxTv_SdtAEOMasterData_Partieshortname ;
   protected String gxTv_SdtAEOMasterData_Identificationissuingcountryname_Z ;
   protected String gxTv_SdtAEOMasterData_Partiename_Z ;
   protected String gxTv_SdtAEOMasterData_Partieshortname_Z ;
}

