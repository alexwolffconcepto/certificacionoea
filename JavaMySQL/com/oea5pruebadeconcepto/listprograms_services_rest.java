package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.servlet.*;
import com.genexus.servlet.http.*;
import java.util.*;
import com.genexus.*;
import com.genexus.Application;
import com.genexus.ws.rs.core.*;

@javax.ws.rs.Path("/ListPrograms")
public final  class listprograms_services_rest extends GxRestService
{
   @javax.ws.rs.POST
   @javax.ws.rs.Produces({javax.ws.rs.core.MediaType.APPLICATION_JSON + ";charset=UTF-8"})
   public javax.ws.rs.core.Response execute( ) throws Exception
   {
      super.init( "POST" );
      @SuppressWarnings("unchecked")
      GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName> [] AV9ProgramNames = new GXBaseCollection[] { new GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName>() };
      if ( ! processHeaders("listprograms",myServletRequestWrapper,myServletResponseWrapper) )
      {
         builder = Response.notModifiedWrapped();
         cleanup();
         return (javax.ws.rs.core.Response) builder.build() ;
      }
      try
      {
         com.oea5pruebadeconcepto.listprograms worker = new com.oea5pruebadeconcepto.listprograms(remoteHandle, context);
         worker.execute(AV9ProgramNames );
         com.oea5pruebadeconcepto.listprograms_RESTInterfaceOUT data = new com.oea5pruebadeconcepto.listprograms_RESTInterfaceOUT();
         data.setProgramNames(SdtProgramNames_ProgramName_RESTInterfacefromGXObjectCollection(AV9ProgramNames[0]));
         builder = Response.okWrapped(data);
         cleanup();
         return (javax.ws.rs.core.Response) builder.build() ;
      }
      catch ( Exception e )
      {
         cleanup();
         throw e;
      }
   }

   private Vector<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName_RESTInterface> SdtProgramNames_ProgramName_RESTInterfacefromGXObjectCollection( GXBaseCollection<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName> collection )
   {
      Vector<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName_RESTInterface> result = new Vector<com.oea5pruebadeconcepto.SdtProgramNames_ProgramName_RESTInterface>();
      for (int i = 0; i < collection.size(); i++)
      {
         result.addElement(new com.oea5pruebadeconcepto.SdtProgramNames_ProgramName_RESTInterface((com.oea5pruebadeconcepto.SdtProgramNames_ProgramName)collection.elementAt(i)));
      }
      return result ;
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

}

