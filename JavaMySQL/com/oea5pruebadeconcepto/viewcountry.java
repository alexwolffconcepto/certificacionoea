package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(urlPatterns = {"/servlet/com.oea5pruebadeconcepto.viewcountry", "/com.oea5pruebadeconcepto.viewcountry"})
public final  class viewcountry extends GXWebObjectStub
{
   public viewcountry( )
   {
   }

   public viewcountry( int remoteHandle )
   {
      super(remoteHandle, new ModelContext( viewcountry.class ));
   }

   public viewcountry( int remoteHandle ,
                       ModelContext context )
   {
      super(remoteHandle, context);
   }

   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new viewcountry_impl(context).doExecute();
   }

   protected void init( com.genexus.internet.HttpContext context )
   {
      new viewcountry_impl(context).cleanup();
   }

   public String getServletInfo( )
   {
      return "View Country";
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

   protected String IntegratedSecurityPermissionPrefix( )
   {
      return "";
   }

   protected String EncryptURLParameters( )
   {
      return "NO";
   }

}

