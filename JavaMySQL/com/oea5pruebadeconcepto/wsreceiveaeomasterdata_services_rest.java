package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import com.genexus.servlet.*;
import com.genexus.servlet.http.*;
import java.util.*;
import com.genexus.*;
import com.genexus.Application;
import com.genexus.ws.rs.core.*;

@javax.ws.rs.Path("/wsReceiveAEOMasterData")
public final  class wsreceiveaeomasterdata_services_rest extends GxRestService
{
   @javax.ws.rs.POST
   @javax.ws.rs.Consumes({javax.ws.rs.core.MediaType.APPLICATION_JSON})
   @javax.ws.rs.Produces({javax.ws.rs.core.MediaType.APPLICATION_JSON + ";charset=UTF-8"})
   public javax.ws.rs.core.Response execute( com.oea5pruebadeconcepto.wsreceiveaeomasterdata_RESTInterfaceIN entity ) throws Exception
   {
      super.init( "POST" );
      String AV8IdentificationIssuingCountryCode;
      AV8IdentificationIssuingCountryCode = entity.getIdentificationIssuingCountryCode() ;
      String AV9PartieID;
      AV9PartieID = entity.getPartieID() ;
      String AV10PartieName;
      AV10PartieName = entity.getPartieName() ;
      String AV11PartieShortName;
      AV11PartieShortName = entity.getPartieShortName() ;
      String AV12PartieRoleCode;
      AV12PartieRoleCode = entity.getPartieRoleCode() ;
      String [] AV13ErrorDescription = new String[] { "" };
      if ( ! processHeaders("wsreceiveaeomasterdata",myServletRequestWrapper,myServletResponseWrapper) )
      {
         builder = Response.notModifiedWrapped();
         cleanup();
         return (javax.ws.rs.core.Response) builder.build() ;
      }
      try
      {
         com.oea5pruebadeconcepto.wsreceiveaeomasterdata worker = new com.oea5pruebadeconcepto.wsreceiveaeomasterdata(remoteHandle, context);
         worker.execute(AV8IdentificationIssuingCountryCode,AV9PartieID,AV10PartieName,AV11PartieShortName,AV12PartieRoleCode,AV13ErrorDescription );
         com.oea5pruebadeconcepto.wsreceiveaeomasterdata_RESTInterfaceOUT data = new com.oea5pruebadeconcepto.wsreceiveaeomasterdata_RESTInterfaceOUT();
         data.setErrorDescription(AV13ErrorDescription[0]);
         builder = Response.okWrapped(data);
         cleanup();
         return (javax.ws.rs.core.Response) builder.build() ;
      }
      catch ( Exception e )
      {
         cleanup();
         throw e;
      }
   }

   protected boolean IntegratedSecurityEnabled( )
   {
      return false;
   }

   protected int IntegratedSecurityLevel( )
   {
      return 0;
   }

}

