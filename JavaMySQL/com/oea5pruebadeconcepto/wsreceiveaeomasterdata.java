package com.oea5pruebadeconcepto ;
import com.oea5pruebadeconcepto.*;
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.search.*;

public final  class wsreceiveaeomasterdata extends GXProcedure
{
   public wsreceiveaeomasterdata( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( wsreceiveaeomasterdata.class ), "" );
   }

   public wsreceiveaeomasterdata( int remoteHandle ,
                                  ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   @SuppressWarnings("unchecked")
   public String executeUdp( String aP0 ,
                             String aP1 ,
                             String aP2 ,
                             String aP3 ,
                             String aP4 )
   {
      wsreceiveaeomasterdata.this.aP5 = new String[] {""};
      execute_int(aP0, aP1, aP2, aP3, aP4, aP5);
      return aP5[0];
   }

   public void execute( String aP0 ,
                        String aP1 ,
                        String aP2 ,
                        String aP3 ,
                        String aP4 ,
                        String[] aP5 )
   {
      execute_int(aP0, aP1, aP2, aP3, aP4, aP5);
   }

   private void execute_int( String aP0 ,
                             String aP1 ,
                             String aP2 ,
                             String aP3 ,
                             String aP4 ,
                             String[] aP5 )
   {
      wsreceiveaeomasterdata.this.AV8IdentificationIssuingCountryCode = aP0;
      wsreceiveaeomasterdata.this.AV9PartieID = aP1;
      wsreceiveaeomasterdata.this.AV10PartieName = aP2;
      wsreceiveaeomasterdata.this.AV11PartieShortName = aP3;
      wsreceiveaeomasterdata.this.AV12PartieRoleCode = aP4;
      wsreceiveaeomasterdata.this.aP5 = aP5;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      privateExecute();
   }

   private void privateExecute( )
   {
      /*
         INSERT RECORD ON TABLE AEOMasterData

      */
      A1IdentificationIssuingCountryCo = AV8IdentificationIssuingCountryCode ;
      A5PartieID = AV9PartieID ;
      A6PartieName = AV10PartieName ;
      A7PartieShortName = AV11PartieShortName ;
      A8PartieRoleCode = AV12PartieRoleCode ;
      /* Using cursor P000J2 */
      pr_default.execute(0, new Object[] {A1IdentificationIssuingCountryCo, A5PartieID, A6PartieName, A7PartieShortName, A8PartieRoleCode});
      Application.getSmartCacheProvider(remoteHandle).setUpdated("AEOMasterData");
      if ( (pr_default.getStatus(0) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("GXM_noupdate") ;
         AV13ErrorDescription = "ERROR: AEO already exists" ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      Application.commitDataStores(context, remoteHandle, pr_default, "wsreceiveaeomasterdata");
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP5[0] = wsreceiveaeomasterdata.this.AV13ErrorDescription;
      CloseOpenCursors();
      exitApp();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV13ErrorDescription = "" ;
      A1IdentificationIssuingCountryCo = "" ;
      A5PartieID = "" ;
      A6PartieName = "" ;
      A7PartieShortName = "" ;
      A8PartieRoleCode = "" ;
      Gx_emsg = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new com.oea5pruebadeconcepto.wsreceiveaeomasterdata__default(),
         new Object[] {
             new Object[] {
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private int GX_INS3 ;
   private String AV8IdentificationIssuingCountryCode ;
   private String AV9PartieID ;
   private String AV12PartieRoleCode ;
   private String A1IdentificationIssuingCountryCo ;
   private String A5PartieID ;
   private String A8PartieRoleCode ;
   private String Gx_emsg ;
   private String AV10PartieName ;
   private String AV11PartieShortName ;
   private String AV13ErrorDescription ;
   private String A6PartieName ;
   private String A7PartieShortName ;
   private String[] aP5 ;
   private IDataStoreProvider pr_default ;
}

final  class wsreceiveaeomasterdata__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new UpdateCursor("P000J2", "INSERT INTO `AEOMasterData`(`IdentificationIssuingCountryCo`, `PartieID`, `PartieName`, `PartieShortName`, `PartieRoleCode`) VALUES(?, ?, ?, ?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 30);
               stmt.setVarchar(3, (String)parms[2], 50, false);
               stmt.setVarchar(4, (String)parms[3], 50, false);
               stmt.setString(5, (String)parms[4], 2);
               return;
      }
   }

}

