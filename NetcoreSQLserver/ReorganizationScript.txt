CREATE TABLE [PartieRole] ([PartieRoleCode] nchar(2) NOT NULL , [PartieRoleName] nvarchar(30) NOT NULL , PRIMARY KEY([PartieRoleCode]))
INSERT INTO [PartieRole] ([PartieRoleCode], [PartieRoleName]) SELECT [PartieRoleCode], '' FROM (SELECT [PartieRoleCode], ROW_NUMBER() OVER (PARTITION BY PartieRoleCode ORDER BY PartieRoleCode DESC) As _GX_ROW_NUMBER FROM [AEOMasterData]) T WHERE _GX_ROW_NUMBER = 1

CREATE NONCLUSTERED INDEX [IAEOMASTERDATA2] ON [AEOMasterData] ([PartieRoleCode] )

ALTER TABLE [AEOMasterData] ADD CONSTRAINT [IAEOMASTERDATA2] FOREIGN KEY ([PartieRoleCode]) REFERENCES [PartieRole] ([PartieRoleCode])

